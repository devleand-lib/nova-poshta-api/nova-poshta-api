# Nova Poshta API
The package provides access to the API 2.0 functions of the Nova Poshta delivery service.

## Installation
### Git
```git
git clone git@gitlab.com:devleand-lib/nova-poshta-api/nova-poshta-api.git
```

### Composer
```
composer require devleand/nova-poshta-api
```

## Testing
### Automatically
- Static analysing
```
./vendor/bin/phpstan analyse -l 8 src
./vendor/bin/phpstan analyse tests
```
- Unit tests
```
./vendor/bin/phpunit tests
```
### Manual
- Example for class V2\NovaPoshtaApiClient

```injectablephp
use Doctrine\Common\Annotations\AnnotationReader;
use JMS\Serializer\Accessor\DefaultAccessorStrategy;
use JMS\Serializer\Construction\UnserializeObjectConstructor;
use JMS\Serializer\GraphNavigator\Factory\DeserializationGraphNavigatorFactory;
use JMS\Serializer\GraphNavigator\Factory\SerializationGraphNavigatorFactory;
use JMS\Serializer\Handler\HandlerRegistry;
use JMS\Serializer\Metadata\Driver\AnnotationDriver;
use JMS\Serializer\Naming\CamelCaseNamingStrategy;
use JMS\Serializer\Serializer;
use JMS\Serializer\Visitor\Factory\JsonDeserializationVisitorFactory;
use JMS\Serializer\Visitor\Factory\JsonSerializationVisitorFactory;
use Devleand\NovaPoshta\Api\V2\Contracts\Model\ModelInterface;
use Devleand\NovaPoshta\Api\V2\Dto\SettingsDto;
use Devleand\NovaPoshta\Api\V2\Enum\Request\HttpMethod;
use Devleand\NovaPoshta\Api\V2\NovaPoshtaApiClient;
use Metadata\MetadataFactory;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Nyholm\Psr7\Factory\Psr17Factory;
use Symfony\Component\Validator\ConstraintValidatorFactory;
use Symfony\Component\Validator\Context\ExecutionContextFactory;
use Symfony\Component\Validator\Mapping\Factory\LazyLoadingMetadataFactory;
use Symfony\Component\Validator\Validator\RecursiveValidator;
use Symfony\Contracts\Translation\LocaleAwareInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Contracts\Translation\TranslatorTrait;

require 'vendor/autoload.php';

$logger = new Logger('name');
$logger->pushHandler(new StreamHandler('php://stdout', Logger::INFO));

$client = new NovaPoshtaApiClient(
    new SettingsDto(
        'https://api.novaposhta.ua/v2.0/json/',
        '01ba51b9f9f8d70866b1d988e02fbf94'
    ),
    new GuzzleHttp\Client(),
    $psr17Factory = new Psr17Factory(),
    $psr17Factory,
    new Serializer(
        $metadataFactory = new MetadataFactory(
            new AnnotationDriver(
                new AnnotationReader(),
                new CamelCaseNamingStrategy()
            )
        ),
        [
            1 => new SerializationGraphNavigatorFactory(
                $metadataFactory,
                $handlerRegistry = new HandlerRegistry()
            ),
            2 => new DeserializationGraphNavigatorFactory(
                $metadataFactory,
                $handlerRegistry,
                new UnserializeObjectConstructor(),
                new DefaultAccessorStrategy()
            )
        ],
        ['json' => new JsonSerializationVisitorFactory()],
        ['json' => new JsonDeserializationVisitorFactory()]
    ),
    new RecursiveValidator(
        new ExecutionContextFactory(
            new class () implements TranslatorInterface, LocaleAwareInterface {
                use TranslatorTrait;
            }
        ),
        new LazyLoadingMetadataFactory(),
        new ConstraintValidatorFactory()
    ),
    $logger
);

try {
    $response = $client->send(
        HttpMethod::POST(),
        new class () implements ModelInterface {
            public function getApiModel(): string
            {
                return 'Common'; // "modelName"
            }

            public function isApiMethodSupported(string $apiMethod): bool
            {
                return true;
            }
        },
        'getPaymentForms' // "calledMethod"
    );

    var_dump($response);
} catch (Throwable $exception) {
    echo $exception->getMessage();
}
```
- Example for class Model/AddressModel

```injectablephp
use Doctrine\Common\Annotations\AnnotationReader;
use JMS\Serializer\Accessor\DefaultAccessorStrategy;
use JMS\Serializer\Construction\UnserializeObjectConstructor;
use JMS\Serializer\GraphNavigator\Factory\DeserializationGraphNavigatorFactory;
use JMS\Serializer\GraphNavigator\Factory\SerializationGraphNavigatorFactory;
use JMS\Serializer\Handler\HandlerRegistry;
use JMS\Serializer\Metadata\Driver\AnnotationDriver;
use JMS\Serializer\Naming\CamelCaseNamingStrategy;
use JMS\Serializer\Serializer;
use JMS\Serializer\Visitor\Factory\JsonDeserializationVisitorFactory;
use JMS\Serializer\Visitor\Factory\JsonSerializationVisitorFactory;
use Devleand\NovaPoshta\Api\V2\Dto\SettingsDto;
use Devleand\NovaPoshta\Api\V2\Model\Address\AddressModel;
use Devleand\NovaPoshta\Api\V2\NovaPoshtaApiClient;
use Devleand\NovaPoshta\Api\V2\Transformer\DataToEntityTransformer;
use Metadata\MetadataFactory;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Nyholm\Psr7\Factory\Psr17Factory;
use Symfony\Component\Validator\ConstraintValidatorFactory;
use Symfony\Component\Validator\Context\ExecutionContextFactory;
use Symfony\Component\Validator\Mapping\Factory\LazyLoadingMetadataFactory;
use Symfony\Component\Validator\Validator\RecursiveValidator;
use Symfony\Contracts\Translation\LocaleAwareInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Contracts\Translation\TranslatorTrait;

require 'vendor/autoload.php';

$logger = new Logger('name');
$logger->pushHandler(new StreamHandler('php://stdout', Logger::INFO));

$client = new NovaPoshtaApiClient(
    new SettingsDto(
        'https://api.novaposhta.ua/v2.0/json/',
        '01ba51b9f9f8d70866b1d988e02fbf94'
    ),
    new GuzzleHttp\Client(),
    $psr17Factory = new Psr17Factory(),
    $psr17Factory,
    $serializer = new Serializer(
        $metadataFactory = new MetadataFactory(
            new AnnotationDriver(
                new AnnotationReader(),
                new CamelCaseNamingStrategy()
            )
        ),
        [
            1 => new SerializationGraphNavigatorFactory(
                $metadataFactory,
                $handlerRegistry = new HandlerRegistry()
            ),
            2 => new DeserializationGraphNavigatorFactory(
                $metadataFactory,
                $handlerRegistry,
                new UnserializeObjectConstructor(),
                new DefaultAccessorStrategy()
            )
        ],
        ['json' => new JsonSerializationVisitorFactory()],
        ['json' => new JsonDeserializationVisitorFactory()]
    ),
    $validator = new RecursiveValidator(
        new ExecutionContextFactory(
            new class () implements TranslatorInterface, LocaleAwareInterface {
                use TranslatorTrait;
            }
        ),
        new LazyLoadingMetadataFactory(),
        new ConstraintValidatorFactory()
    ),
    $logger
);

$addressModel = new AddressModel($client, new DataToEntityTransformer($serializer, $validator, $logger), $logger);

try {
    $response = $addressModel->getAreas();

    var_dump(iterator_to_array($response->getData()));
} catch (Throwable $exception) {
    echo $exception->getMessage();
}
```
