<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2;

use Devleand\NovaPoshta\Api\V2\Contracts\NovaPoshtaApiClientFactoryInterface;
use Devleand\NovaPoshta\Api\V2\Dto\SettingsDto;
use Devleand\NovaPoshta\Api\V2\Enum\Language;
use JMS\Serializer\SerializerInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class NovaPoshtaApiClientFactory implements NovaPoshtaApiClientFactoryInterface
{
    /**
     * @var ClientInterface
     */
    private ClientInterface $client;

    /**
     * @var RequestFactoryInterface
     */
    private RequestFactoryInterface $requestFactory;

    /**
     * @var StreamFactoryInterface
     */
    private StreamFactoryInterface $streamFactory;

    /**
     * @var SerializerInterface
     */
    private SerializerInterface $serializer;

    /**
     * @var ValidatorInterface
     */
    private ValidatorInterface $validator;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @var string
     */
    private string $baseUrl;

    /**
     * @var Language
     */
    private Language $defaultLanguage;

    public function __construct(
        ClientInterface $client,
        RequestFactoryInterface $requestFactory,
        StreamFactoryInterface $streamFactory,
        SerializerInterface $serializer,
        ValidatorInterface $validator,
        LoggerInterface $logger,
        string $baseUrl,
        Language $defaultLanguage
    ) {
        $this->client = $client;
        $this->requestFactory = $requestFactory;
        $this->streamFactory = $streamFactory;
        $this->serializer = $serializer;
        $this->validator = $validator;
        $this->logger = $logger;
        $this->baseUrl = $baseUrl;
        $this->defaultLanguage = $defaultLanguage;
    }

    public function make(string $key, ?Language $language = null): NovaPoshtaApiClient
    {
        return new NovaPoshtaApiClient(
            new SettingsDto($this->baseUrl, $key, $language ?? $this->defaultLanguage),
            $this->client,
            $this->requestFactory,
            $this->streamFactory,
            $this->serializer,
            $this->validator,
            $this->logger
        );
    }
}
