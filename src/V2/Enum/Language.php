<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Enum;

use MyCLabs\Enum\Enum;

/**
 * @extends Enum<string>
 *
 * @method static Language RU()
 * @method static Language UK()
 */
final class Language extends Enum
{
    public const RU = 'ru';
    public const UK = 'uk';
}
