<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Enum\Request;

use MyCLabs\Enum\Enum;

/**
 * @extends Enum<string>
 *
 * @method static HttpMethod POST()
 * @method static HttpMethod GET()
 */
final class HttpMethod extends Enum
{
    public const POST = 'POST';
    public const GET = 'GET';
}
