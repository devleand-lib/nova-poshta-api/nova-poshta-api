<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Transformer;

use Devleand\NovaPoshta\Api\Logger\LogValidationErrorsTrait;
use Generator;
use JMS\Serializer\ArrayTransformerInterface;
use Devleand\NovaPoshta\Api\V2\Contracts\Exception\DataToEntityTransformException;
use Devleand\NovaPoshta\Api\V2\Contracts\Exception\UnexpectedEntityException;
use Devleand\NovaPoshta\Api\V2\Contracts\Transformer\DataToEntityTransformer as DataToEntityTransformerContract;
use Devleand\NovaPoshta\Api\V2\Exception\EntityValidationFailedException;
use Devleand\NovaPoshta\Api\Logger\ExceptionLoggerTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Throwable;

class DataToEntityTransformer implements DataToEntityTransformerContract
{
    use ExceptionLoggerTrait;
    use LogValidationErrorsTrait;

    /**
     * @var ArrayTransformerInterface
     */
    private ArrayTransformerInterface $arrayTransformer;

    /**
     * @var ValidatorInterface
     */
    private ValidatorInterface $validator;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    public function __construct(
        ArrayTransformerInterface $arrayTransformer,
        ValidatorInterface $validator,
        LoggerInterface $logger
    ) {
        $this->arrayTransformer = $arrayTransformer;
        $this->validator = $validator;
        $this->logger = $logger;
    }

    /**
     * @template TEntity
     *
     * @param iterable<array<string, mixed>> $data
     * @param class-string<TEntity>          $entityClass
     *
     * @return Generator<int, TEntity, never-return, void>
     *
     * @throws DataToEntityTransformException
     * @throws UnexpectedEntityException
     * @throws EntityValidationFailedException<TEntity>
     */
    public function transform(iterable $data, string $entityClass): Generator
    {
        foreach ($data as $dataRow) {
            try {
                $entity = $this->arrayTransformer->fromArray($dataRow, $entityClass);
            } catch (Throwable $exception) {
                $this->logExceptionWithTrace($exception);

                throw new DataToEntityTransformException($dataRow, $entityClass);
            }

            if (! ($entity instanceof $entityClass)) {
                throw new UnexpectedEntityException($entity, $entityClass);
            }

            try {
                $errors = $this->validator->validate($entity);
                if (count($errors) > 0) {
                    $this->logValidationErrors($errors);

                    throw new ValidationFailedException($entity, $errors);
                }
            } catch (Throwable $exception) {
                $this->logExceptionWithTrace($exception);

                throw new EntityValidationFailedException($entity);
            }

            yield $entity;
        }
    }
}
