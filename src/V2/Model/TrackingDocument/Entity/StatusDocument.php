<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\TrackingDocument\Entity;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

final class StatusDocument
{
    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("StatusCode")
     * @Serializer\Type("string")
     */
    private string $statusCode;

    /**
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("Status")
     * @Serializer\Type("string")
     */
    private string $status;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("Number")
     * @Serializer\Type("string")
     */
    private string $number;

    /**
     * @Serializer\SerializedName("DateCreated")
     * @Serializer\Type("string")
     */
    private ?string $dateCreated = null;

    /**
     * @Serializer\SerializedName("DocumentWeight")
     * @Serializer\Type("string")
     */
    private ?string $weight = null;

    /**
     * @Serializer\SerializedName("Cost")
     * @Serializer\Type("string")
     */
    private ?string $costOnSite = null;

    /**
     * @Serializer\SerializedName("CargoDescriptionString")
     * @Serializer\Type("string")
     */
    private ?string $description = null;

    /**
     * @Serializer\SerializedName("CitySender")
     * @Serializer\Type("string")
     */
    private ?string $citySender = null;

    /**
     * @Serializer\SerializedName("RefCitySender")
     * @Serializer\Type("string")
     */
    private ?string $refCitySender = null;

    /**
     * @Serializer\SerializedName("SenderAddress")
     * @Serializer\Type("string")
     */
    private ?string $senderAddress = null;

    /**
     * @Serializer\SerializedName("WarehouseSenderAddress")
     * @Serializer\Type("string")
     */
    private ?string $warehouseSenderAddress = null;

    /**
     * @Serializer\SerializedName("WarehouseSender")
     * @Serializer\Type("string")
     */
    private ?string $warehouseSender = null;

    /**
     * @Serializer\SerializedName("SenderWarehouseIndex")
     * @Serializer\Type("string")
     */
    private ?string $senderWarehouseIndex = null;

    /**
     * @Serializer\SerializedName("WarehouseSenderInternetAddressRef")
     * @Serializer\Type("string")
     */
    private ?string $warehouseSenderInternetAddressRef = null;

    /**
     * @Serializer\SerializedName("SenderFullNameEW")
     * @Serializer\Type("string")
     */
    private ?string $senderFullName = null;

    /**
     * @Serializer\SerializedName("PhoneSender")
     * @Serializer\Type("string")
     */
    private ?string $phoneSender = null;

    /**
     * @Serializer\SerializedName("CityRecipient")
     * @Serializer\Type("string")
     */
    private ?string $cityRecipient = null;

    /**
     * @Serializer\SerializedName("RefCityRecipient")
     * @Serializer\Type("string")
     */
    private ?string $refCityRecipient = null;

    /**
     * @Serializer\SerializedName("RecipientAddress")
     * @Serializer\Type("string")
     */
    private ?string $recipientAddress = null;

    /**
     * @Serializer\SerializedName("WarehouseRecipient")
     * @Serializer\Type("string")
     */
    private ?string $warehouseRecipient = null;

    /**
     * @Serializer\SerializedName("WarehouseRecipientRef")
     * @Serializer\Type("string")
     */
    private ?string $warehouseRecipientRef = null;

    /**
     * @Serializer\SerializedName("RecipientWarehouseTypeRef")
     * @Serializer\Type("string")
     */
    private ?string $recipientWarehouseTypeRef = null;

    /**
     * @Serializer\SerializedName("RecipientWarehouseIndex")
     * @Serializer\Type("string")
     */
    private ?string $recipientWarehouseIndex = null;

    /**
     * @Serializer\SerializedName("WarehouseRecipientInternetAddressRef")
     * @Serializer\Type("string")
     */
    private ?string $warehouseRecipientInternetAddressRef = null;

    /**
     * @Serializer\SerializedName("RecipientFullNameEW")
     * @Serializer\Type("string")
     */
    private ?string $recipientFullName = null;

    /**
     * @Serializer\SerializedName("PhoneRecipient")
     * @Serializer\Type("string")
     */
    private ?string $phoneRecipient = null;

    /**
     * @Serializer\SerializedName("ServiceType")
     * @Serializer\Type("string")
     */
    private ?string $serviceType = null;

    /**
     * @Serializer\SerializedName("PayerType")
     * @Serializer\Type("string")
     */
    private ?string $payerType = null;

    /**
     * @Serializer\SerializedName("PaymentMethod")
     * @Serializer\Type("string")
     */
    private ?string $paymentMethod = null;

    /**
     * @Serializer\SerializedName("CargoType")
     * @Serializer\Type("string")
     */
    private ?string $cargoType = null;

    /**
     * @Serializer\SerializedName("AfterpaymentOnGoodsCost")
     * @Serializer\Type("string")
     */
    private ?string $afterPaymentOnGoodsCost = null;

    public function getStatusCode(): string
    {
        return $this->statusCode;
    }

    public function setStatusCode(string $statusCode): self
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getNumber(): string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getDateCreated(): ?string
    {
        return $this->dateCreated;
    }

    public function setDateCreated(?string $dateCreated): self
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    public function getWeight(): ?string
    {
        return $this->weight;
    }

    public function setWeight(?string $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getCostOnSite(): ?string
    {
        return $this->costOnSite;
    }

    public function setCostOnSite(?string $costOnSite): self
    {
        $this->costOnSite = $costOnSite;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCitySender(): ?string
    {
        return $this->citySender;
    }

    public function setCitySender(?string $citySender): self
    {
        $this->citySender = $citySender;

        return $this;
    }

    public function getRefCitySender(): ?string
    {
        return $this->refCitySender;
    }

    public function setRefCitySender(?string $refCitySender): self
    {
        $this->refCitySender = $refCitySender;

        return $this;
    }

    public function getSenderAddress(): ?string
    {
        return $this->senderAddress;
    }

    public function setSenderAddress(?string $senderAddress): self
    {
        $this->senderAddress = $senderAddress;

        return $this;
    }

    public function getWarehouseSenderAddress(): ?string
    {
        return $this->warehouseSenderAddress;
    }

    public function setWarehouseSenderAddress(?string $warehouseSenderAddress): self
    {
        $this->warehouseSenderAddress = $warehouseSenderAddress;

        return $this;
    }

    public function getWarehouseSender(): ?string
    {
        return $this->warehouseSender;
    }

    public function setWarehouseSender(?string $warehouseSender): self
    {
        $this->warehouseSender = $warehouseSender;

        return $this;
    }

    public function getSenderWarehouseIndex(): ?string
    {
        return $this->senderWarehouseIndex;
    }

    public function setSenderWarehouseIndex(?string $senderWarehouseIndex): self
    {
        $this->senderWarehouseIndex = $senderWarehouseIndex;

        return $this;
    }

    public function getWarehouseSenderInternetAddressRef(): ?string
    {
        return $this->warehouseSenderInternetAddressRef;
    }

    public function setWarehouseSenderInternetAddressRef(?string $warehouseSenderInternetAddressRef): self
    {
        $this->warehouseSenderInternetAddressRef = $warehouseSenderInternetAddressRef;

        return $this;
    }

    public function getSenderFullName(): ?string
    {
        return $this->senderFullName;
    }

    public function setSenderFullName(?string $senderFullName): self
    {
        $this->senderFullName = $senderFullName;

        return $this;
    }

    public function getPhoneSender(): ?string
    {
        return $this->phoneSender;
    }

    public function setPhoneSender(?string $phoneSender): self
    {
        $this->phoneSender = $phoneSender;

        return $this;
    }

    public function getCityRecipient(): ?string
    {
        return $this->cityRecipient;
    }

    public function setCityRecipient(?string $cityRecipient): self
    {
        $this->cityRecipient = $cityRecipient;

        return $this;
    }

    public function getRefCityRecipient(): ?string
    {
        return $this->refCityRecipient;
    }

    public function setRefCityRecipient(?string $refCityRecipient): self
    {
        $this->refCityRecipient = $refCityRecipient;

        return $this;
    }

    public function getRecipientAddress(): ?string
    {
        return $this->recipientAddress;
    }

    public function setRecipientAddress(?string $recipientAddress): self
    {
        $this->recipientAddress = $recipientAddress;

        return $this;
    }

    public function getWarehouseRecipient(): ?string
    {
        return $this->warehouseRecipient;
    }

    public function setWarehouseRecipient(?string $warehouseRecipient): self
    {
        $this->warehouseRecipient = $warehouseRecipient;

        return $this;
    }

    public function getWarehouseRecipientRef(): ?string
    {
        return $this->warehouseRecipientRef;
    }

    public function setWarehouseRecipientRef(?string $warehouseRecipientRef): self
    {
        $this->warehouseRecipientRef = $warehouseRecipientRef;

        return $this;
    }

    public function getRecipientWarehouseTypeRef(): ?string
    {
        return $this->recipientWarehouseTypeRef;
    }

    public function setRecipientWarehouseTypeRef(?string $recipientWarehouseTypeRef): self
    {
        $this->recipientWarehouseTypeRef = $recipientWarehouseTypeRef;

        return $this;
    }

    public function getRecipientWarehouseIndex(): ?string
    {
        return $this->recipientWarehouseIndex;
    }

    public function setRecipientWarehouseIndex(?string $recipientWarehouseIndex): self
    {
        $this->recipientWarehouseIndex = $recipientWarehouseIndex;

        return $this;
    }

    public function getWarehouseRecipientInternetAddressRef(): ?string
    {
        return $this->warehouseRecipientInternetAddressRef;
    }

    public function setWarehouseRecipientInternetAddressRef(?string $warehouseRecipientInternetAddressRef): self
    {
        $this->warehouseRecipientInternetAddressRef = $warehouseRecipientInternetAddressRef;

        return $this;
    }

    public function getRecipientFullName(): ?string
    {
        return $this->recipientFullName;
    }

    public function setRecipientFullName(?string $recipientFullName): self
    {
        $this->recipientFullName = $recipientFullName;

        return $this;
    }

    public function getPhoneRecipient(): ?string
    {
        return $this->phoneRecipient;
    }

    public function setPhoneRecipient(?string $phoneRecipient): self
    {
        $this->phoneRecipient = $phoneRecipient;

        return $this;
    }

    public function getServiceType(): ?string
    {
        return $this->serviceType;
    }

    public function setServiceType(?string $serviceType): self
    {
        $this->serviceType = $serviceType;

        return $this;
    }

    public function getPayerType(): ?string
    {
        return $this->payerType;
    }

    public function setPayerType(?string $payerType): self
    {
        $this->payerType = $payerType;

        return $this;
    }

    public function getPaymentMethod(): ?string
    {
        return $this->paymentMethod;
    }

    public function setPaymentMethod(?string $paymentMethod): self
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    public function getCargoType(): ?string
    {
        return $this->cargoType;
    }

    public function setCargoType(?string $cargoType): self
    {
        $this->cargoType = $cargoType;

        return $this;
    }

    public function getAfterPaymentOnGoodsCost(): ?string
    {
        return $this->afterPaymentOnGoodsCost;
    }

    public function setAfterPaymentOnGoodsCost(?string $afterPaymentOnGoodsCost): self
    {
        $this->afterPaymentOnGoodsCost = $afterPaymentOnGoodsCost;

        return $this;
    }
}
