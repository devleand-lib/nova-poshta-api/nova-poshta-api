<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\TrackingDocument;

use Devleand\NovaPoshta\Api\V2\Contracts\Response\ApiResponseInterface;
use Devleand\NovaPoshta\Api\V2\Model\Enum\ApiModel;
use Devleand\NovaPoshta\Api\V2\Model\Model;
use Devleand\NovaPoshta\Api\V2\Model\TrackingDocument\Dto\GetStatusDocumentsProperties;
use Devleand\NovaPoshta\Api\V2\Model\TrackingDocument\Dto\DocumentDto;
use Devleand\NovaPoshta\Api\V2\Model\TrackingDocument\Entity\StatusDocument;
use Devleand\NovaPoshta\Api\V2\Model\TrackingDocument\Enum\TrackingDocumentMethod;

class TrackingDocumentModel extends Model
{
    public function getApiModel(): ApiModel
    {
        return ApiModel::TRACKING_DOCUMENT();
    }

    public function isApiMethodSupported(string $apiMethod): bool
    {
        return TrackingDocumentMethod::isValid($apiMethod);
    }

    /**
     * @param DocumentDto[] $documents
     *
     * @return ApiResponseInterface<StatusDocument>
     *
     * @see https://developers.novaposhta.ua/view/model/a99d2f28-8512-11ec-8ced-005056b2dbe1/method/a9ae7bc9-8512-11ec-8ced-005056b2dbe1
     */
    public function getStatusDocuments(array $documents): ApiResponseInterface
    {
        return $this->sendWithTransform(
            TrackingDocumentMethod::GET_STATUS_DOCUMENTS,
            StatusDocument::class,
            [new GetStatusDocumentsProperties($documents)]
        );
    }
}
