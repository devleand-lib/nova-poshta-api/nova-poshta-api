<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\TrackingDocument\Enum;

use MyCLabs\Enum\Enum;

/**
 * @extends Enum<string>
 *
 * @method static TrackingDocumentMethod GET_STATUS_DOCUMENTS()
 */
class TrackingDocumentMethod extends Enum
{
    public const GET_STATUS_DOCUMENTS = 'getStatusDocuments';
}
