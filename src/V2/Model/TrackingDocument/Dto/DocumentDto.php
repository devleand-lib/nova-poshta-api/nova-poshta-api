<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\TrackingDocument\Dto;

use Devleand\NovaPoshta\Api\V2\Contracts\Model\ApiMethodProperties;
use JMS\Serializer\Annotation as Serializer;

class DocumentDto implements ApiMethodProperties
{
    /**
     * @Serializer\SerializedName("DocumentNumber")
     * @Serializer\Type("string")
     */
    private string $number;

    /**
     * @Serializer\SkipWhenEmpty()
     * @Serializer\SerializedName("Phone")
     * @Serializer\Type("string")
     */
    private ?string $phone = null;

    public function __construct(string $number)
    {
        $this->number = $number;
    }

    public function getNumber(): string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }
}
