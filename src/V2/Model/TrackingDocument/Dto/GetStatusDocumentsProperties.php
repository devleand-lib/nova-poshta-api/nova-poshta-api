<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\TrackingDocument\Dto;

use Devleand\NovaPoshta\Api\V2\Contracts\Model\ApiMethodProperties;
use JMS\Serializer\Annotation as Serializer;

class GetStatusDocumentsProperties implements ApiMethodProperties
{
    /**
     * @var DocumentDto[]
     *
     * @Serializer\SerializedName("Documents")
     * @Serializer\Type("array<Devleand\NovaPoshta\Api\V2\Model\TrackingDocument\Dto\DocumentDto>")
     */
    private array $documents;

    /**
     * @param DocumentDto[] $documents
     */
    public function __construct(array $documents)
    {
        $this->documents = $documents;
    }

    /**
     * @return DocumentDto[]
     */
    public function getDocuments(): array
    {
        return $this->documents;
    }
}
