<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model;

use JMS\Serializer\ArrayTransformerInterface;
use Devleand\NovaPoshta\Api\V2\Contracts\ApiClientInterface;
use Devleand\NovaPoshta\Api\V2\Contracts\Model\ApiMethodProperties;
use Devleand\NovaPoshta\Api\V2\Contracts\Model\ModelInterface;
use Devleand\NovaPoshta\Api\V2\Contracts\Response\ApiResponseInterface;
use Devleand\NovaPoshta\Api\V2\Contracts\Transformer\DataToEntityTransformer;
use Devleand\NovaPoshta\Api\V2\Enum\Request\HttpMethod;
use Devleand\NovaPoshta\Api\Logger\ExceptionLoggerTrait;
use Psr\Log\LoggerInterface;

abstract class Model implements ModelInterface
{
    use ExceptionLoggerTrait;

    /**
     * @var ApiClientInterface
     */
    protected ApiClientInterface $apiClient;

    /**
     * @var DataToEntityTransformer
     */
    protected DataToEntityTransformer $dataToEntityTransformer;

    /**
     * @var ArrayTransformerInterface
     */
    protected ArrayTransformerInterface $arrayTransformer;

    /**
     * @var LoggerInterface
     */
    protected LoggerInterface $logger;

    public function __construct(
        ApiClientInterface $apiClient,
        DataToEntityTransformer $dataToEntityTransformer,
        ArrayTransformerInterface $arrayTransformer,
        LoggerInterface $logger
    ) {
        $this->apiClient = $apiClient;
        $this->dataToEntityTransformer = $dataToEntityTransformer;
        $this->arrayTransformer = $arrayTransformer;
        $this->logger = $logger;
    }

    public function getApiClient(): ApiClientInterface
    {
        return $this->apiClient;
    }

    public function setApiClient(ApiClientInterface $apiClient): self
    {
        $this->apiClient = $apiClient;

        return $this;
    }

    /**
     * @param string                     $apiMethod
     * @param ApiMethodProperties[]|null $propertiesList
     * @param HttpMethod|null            $httpMethod
     *
     * @return ApiResponseInterface<array<string, mixed>>
     */
    protected function send(
        string $apiMethod,
        ?array $propertiesList = null,
        HttpMethod $httpMethod = null
    ): ApiResponseInterface {
        $httpMethod ??= HttpMethod::POST();

        $data = null;
        if ($propertiesList !== null) {
            $data = [];
            array_walk_recursive(
                $propertiesList,
                function (ApiMethodProperties $properties) use (&$data): void {
                    $data = array_merge($data, $this->arrayTransformer->toArray($properties));
                }
            );
        }

        return $this->apiClient->send(
            $httpMethod,
            $this,
            $apiMethod,
            $data
        );
    }

    /**
     * @template Entity
     *
     * @param string                     $apiMethod
     * @param class-string<Entity>       $entityClass
     * @param ApiMethodProperties[]|null $propertiesList
     * @param HttpMethod|null            $httpMethod
     *
     * @return ApiResponseInterface<Entity>
     */
    protected function sendWithTransform(
        string $apiMethod,
        string $entityClass,
        ?array $propertiesList = null,
        HttpMethod $httpMethod = null
    ): ApiResponseInterface {
        $response = $this->send($apiMethod, $propertiesList, $httpMethod);
        $responseData = $response->getData();

        $lazyEntities = $this->dataToEntityTransformer->transform($responseData, $entityClass);

        return $response->withData($lazyEntities);
    }
}
