<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\CommonGeneral\Entity;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

final class Message
{
    /**
     * @var numeric-string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("MessageCode")
     * @Serializer\Type("string")
     */
    private string $code;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("MessageText")
     * @Serializer\Type("string")
     */
    private string $descriptionEn;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("MessageDescriptionUA")
     * @Serializer\Type("string")
     */
    private string $descriptionUk;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("MessageDescriptionRU")
     * @Serializer\Type("string")
     */
    private string $descriptionRu;

    /**
     * @param numeric-string $code
     * @param string         $descriptionEn
     * @param string         $descriptionUk
     * @param string         $descriptionRu
     */
    public function __construct(string $code, string $descriptionEn, string $descriptionUk, string $descriptionRu)
    {
        $this->code = $code;
        $this->descriptionEn = $descriptionEn;
        $this->descriptionUk = $descriptionUk;
        $this->descriptionRu = $descriptionRu;
    }

    /**
     * @return numeric-string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    public function getDescriptionEn(): string
    {
        return $this->descriptionEn;
    }

    public function getDescriptionUk(): string
    {
        return $this->descriptionUk;
    }

    public function getDescriptionRu(): string
    {
        return $this->descriptionRu;
    }
}
