<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\CommonGeneral;

use Devleand\NovaPoshta\Api\V2\Contracts\Response\ApiResponseInterface;
use Devleand\NovaPoshta\Api\V2\Model\CommonGeneral\Entity\Message;
use Devleand\NovaPoshta\Api\V2\Model\CommonGeneral\Enum\CommonGeneralMethod;
use Devleand\NovaPoshta\Api\V2\Model\Enum\ApiModel;
use Devleand\NovaPoshta\Api\V2\Model\Model;

class CommonGeneralModel extends Model
{
    public function getApiModel(): ApiModel
    {
        return ApiModel::COMMON_GENERAL();
    }

    public function isApiMethodSupported(string $apiMethod): bool
    {
        return CommonGeneralMethod::isValid($apiMethod);
    }

    /**
     * @return ApiResponseInterface<Message>
     *
     * @see https://devcenter.novaposhta.ua/docs/services/55702570a0fe4f0cf4fc53ed/operations/58f0730deea270153c8be3cd
     */
    public function getMessageCodeText(): ApiResponseInterface
    {
        return $this->sendWithTransform(
            CommonGeneralMethod::GET_MESSAGE_CODE_TEXT,
            Message::class
        );
    }
}
