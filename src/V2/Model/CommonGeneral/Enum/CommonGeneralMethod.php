<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\CommonGeneral\Enum;

use MyCLabs\Enum\Enum;

/**
 * @extends Enum<string>
 *
 * @method static CommonGeneralMethod GET_MESSAGE_CODE_TEXT()
 */
final class CommonGeneralMethod extends Enum
{
    public const GET_MESSAGE_CODE_TEXT = 'getMessageCodeText';
}
