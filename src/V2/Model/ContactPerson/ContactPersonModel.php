<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\ContactPerson;

use Devleand\NovaPoshta\Api\V2\Contracts\Response\ApiResponseInterface;
use Devleand\NovaPoshta\Api\V2\Model\ContactPerson\Dto\ContactPersonDto;
use Devleand\NovaPoshta\Api\V2\Model\ContactPerson\Dto\ContactPersonRefDto;
use Devleand\NovaPoshta\Api\V2\Model\ContactPerson\Entity\ContactPerson;
use Devleand\NovaPoshta\Api\V2\Model\ContactPerson\Entity\ContactPersonRef;
use Devleand\NovaPoshta\Api\V2\Model\ContactPerson\Enum\ContactPersonMethod;
use Devleand\NovaPoshta\Api\V2\Model\Enum\ApiModel;
use Devleand\NovaPoshta\Api\V2\Model\Model;

class ContactPersonModel extends Model
{
    public function getApiModel(): ApiModel
    {
        return ApiModel::CONTACT_PERSON();
    }

    public function isApiMethodSupported(string $apiMethod): bool
    {
        return ContactPersonMethod::isValid($apiMethod);
    }

    /**
     * @param ContactPersonDto $contactPersonDto
     *
     * @return ApiResponseInterface<ContactPerson>
     *
     * @see https://devcenter.novaposhta.ua/docs/services/557eb8c8a0fe4f02fc455b2d/operations/55828c4ca0fe4f0adc08ef27
     */
    public function save(ContactPersonDto $contactPersonDto): ApiResponseInterface
    {
        return $this->sendWithTransform(
            ContactPersonMethod::SAVE,
            ContactPerson::class,
            [$contactPersonDto]
        );
    }

    /**
     * @param string $ref
     * @param ContactPersonDto $contactPersonDto
     *
     * @return ApiResponseInterface<ContactPerson>
     *
     * @see https://devcenter.novaposhta.ua/docs/services/557eb8c8a0fe4f02fc455b2d/operations/558297aca0fe4f0adc08ef28
     */
    public function update(string $ref, ContactPersonDto $contactPersonDto): ApiResponseInterface
    {
        return $this->sendWithTransform(
            ContactPersonMethod::UPDATE,
            ContactPerson::class,
            [new ContactPersonRefDto($ref), $contactPersonDto]
        );
    }

    /**
     * @param string $ref
     *
     * @return ApiResponseInterface<ContactPersonRef>
     *
     * @see https://devcenter.novaposhta.ua/docs/services/557eb8c8a0fe4f02fc455b2d/operations/55829aa2a0fe4f0adc08ef29
     */
    public function delete(string $ref): ApiResponseInterface
    {
        $refDto = new ContactPersonRefDto($ref);

        return $this->sendWithTransform(
            ContactPersonMethod::DELETE,
            ContactPersonRef::class,
            [$refDto]
        );
    }
}
