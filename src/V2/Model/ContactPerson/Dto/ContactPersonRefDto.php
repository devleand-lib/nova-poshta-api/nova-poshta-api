<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\ContactPerson\Dto;

use JMS\Serializer\Annotation as Serializer;
use Devleand\NovaPoshta\Api\V2\Contracts\Model\ApiMethodProperties;

class ContactPersonRefDto implements ApiMethodProperties
{
    /**
     * @var string
     *
     * @Serializer\SerializedName("Ref")
     * @Serializer\Type("string")
     */
    protected string $ref;

    public function __construct(string $ref)
    {
        $this->ref = $ref;
    }

    public function getRef(): string
    {
        return $this->ref;
    }
}
