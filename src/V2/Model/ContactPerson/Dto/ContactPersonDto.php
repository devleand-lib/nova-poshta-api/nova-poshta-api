<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\ContactPerson\Dto;

use JMS\Serializer\Annotation as Serializer;
use Devleand\NovaPoshta\Api\V2\Contracts\Model\ApiMethodProperties;

class ContactPersonDto implements ApiMethodProperties
{
    /**
     * @var string
     *
     * @Serializer\SerializedName("CounterpartyRef")
     * @Serializer\Type("string")
     */
    protected string $counterpartyRef;

    /**
     * @var string
     *
     * @Serializer\SerializedName("FirstName")
     * @Serializer\Type("string")
     */
    protected string $firstName;

    /**
     * @var string
     *
     * @Serializer\SerializedName("LastName")
     * @Serializer\Type("string")
     */
    protected string $lastName;

    /**
     * @var string
     *
     * @Serializer\SerializedName("MiddleName")
     * @Serializer\Type("string")
     */
    protected string $middleName;

    /**
     * @var string
     *
     * @Serializer\SerializedName("Phone")
     * @Serializer\Type("string")
     */
    protected string $phone;

    public function __construct(
        string $counterpartyRef,
        string $firstName,
        string $lastName,
        string $middleName,
        string $phone
    ) {
        $this->counterpartyRef = $counterpartyRef;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->middleName = $middleName;
        $this->phone = $phone;
    }

    public function getCounterpartyRef(): string
    {
        return $this->counterpartyRef;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getMiddleName(): string
    {
        return $this->middleName;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }
}
