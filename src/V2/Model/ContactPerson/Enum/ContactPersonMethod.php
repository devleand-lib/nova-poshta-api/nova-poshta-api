<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\ContactPerson\Enum;

use MyCLabs\Enum\Enum;

/**
 * @extends Enum<string>
 *
 * @method static ContactPersonMethod SAVE()
 * @method static ContactPersonMethod UPDATE()
 * @method static ContactPersonMethod DELETE()
 */
final class ContactPersonMethod extends Enum
{
    public const SAVE = 'save';
    public const UPDATE = 'update';
    public const DELETE = 'delete';
}
