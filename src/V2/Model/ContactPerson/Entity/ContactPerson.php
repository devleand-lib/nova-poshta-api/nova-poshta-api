<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\ContactPerson\Entity;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

final class ContactPerson
{
    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("Ref")
     * @Serializer\Type("string")
     */
    private string $ref;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("Description")
     * @Serializer\Type("string")
     */
    private string $description;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("FirstName")
     * @Serializer\Type("string")
     */
    private string $firstName;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("LastName")
     * @Serializer\Type("string")
     */
    private string $lastName;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("MiddleName")
     * @Serializer\Type("string")
     */
    private string $middleName;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("Phones")
     * @Serializer\Type("string")
     */
    private string $phones;

    /**
     * @var string|null
     *
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("Email")
     * @Serializer\Type("string")
     */
    private ?string $email;

    public function __construct(
        string $ref,
        string $description,
        string $firstName,
        string $lastName,
        string $middleName,
        string $phones,
        ?string $email
    ) {
        $this->ref = $ref;
        $this->description = $description;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->middleName = $middleName;
        $this->phones = $phones;
        $this->email = $email;
    }

    public function getRef(): string
    {
        return $this->ref;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getMiddleName(): string
    {
        return $this->middleName;
    }

    public function getPhones(): string
    {
        return $this->phones;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }
}
