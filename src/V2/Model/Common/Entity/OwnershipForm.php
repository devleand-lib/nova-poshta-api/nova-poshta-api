<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\Common\Entity;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

final class OwnershipForm
{
    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("Ref")
     * @Serializer\Type("string")
     */
    private string $ref;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("Description")
     * @Serializer\Type("string")
     */
    private string $shortDescriptionUk;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("FullName")
     * @Serializer\Type("string")
     */
    private string $descriptionUk;

    public function __construct(string $ref, string $shortDescriptionUk, string $descriptionUk)
    {
        $this->ref = $ref;
        $this->shortDescriptionUk = $shortDescriptionUk;
        $this->descriptionUk = $descriptionUk;
    }

    public function getRef(): string
    {
        return $this->ref;
    }

    public function getShortDescriptionUk(): string
    {
        return $this->shortDescriptionUk;
    }

    public function getDescriptionUk(): string
    {
        return $this->descriptionUk;
    }
}
