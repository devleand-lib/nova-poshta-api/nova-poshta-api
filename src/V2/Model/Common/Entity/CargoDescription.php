<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\Common\Entity;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

final class CargoDescription
{
    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("Ref")
     * @Serializer\Type("string")
     */
    private string $ref;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("Description")
     * @Serializer\Type("string")
     */
    private string $descriptionUk;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("DescriptionRu")
     * @Serializer\Type("string")
     */
    private string $descriptionRu;

    public function __construct(string $ref, string $descriptionUk, string $descriptionRu)
    {
        $this->ref = $ref;
        $this->descriptionUk = $descriptionUk;
        $this->descriptionRu = $descriptionRu;
    }

    public function getRef(): string
    {
        return $this->ref;
    }

    public function getDescriptionUk(): string
    {
        return $this->descriptionUk;
    }

    public function getDescriptionRu(): string
    {
        return $this->descriptionRu;
    }
}
