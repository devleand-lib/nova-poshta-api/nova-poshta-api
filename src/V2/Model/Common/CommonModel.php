<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\Common;

use Devleand\NovaPoshta\Api\V2\Contracts\Response\ApiResponseInterface;
use Devleand\NovaPoshta\Api\V2\Model\Common\Entity\BackwardDeliveryCargoType;
use Devleand\NovaPoshta\Api\V2\Model\Common\Entity\CargoDescription;
use Devleand\NovaPoshta\Api\V2\Model\Common\Entity\CargoType;
use Devleand\NovaPoshta\Api\V2\Model\Common\Entity\OwnershipForm;
use Devleand\NovaPoshta\Api\V2\Model\Common\Entity\PaymentForm;
use Devleand\NovaPoshta\Api\V2\Model\Common\Entity\ServiceType;
use Devleand\NovaPoshta\Api\V2\Model\Common\Entity\TypeOfCounterparties;
use Devleand\NovaPoshta\Api\V2\Model\Common\Entity\TypeOfPayers;
use Devleand\NovaPoshta\Api\V2\Model\Common\Entity\TypeOfPayersForRedelivery;
use Devleand\NovaPoshta\Api\V2\Model\Common\Enum\CommonMethod;
use Devleand\NovaPoshta\Api\V2\Dto\Model\PaginationPropertiesDto;
use Devleand\NovaPoshta\Api\V2\Model\Enum\ApiModel;
use Devleand\NovaPoshta\Api\V2\Model\Model;

class CommonModel extends Model
{
    public function getApiModel(): ApiModel
    {
        return ApiModel::COMMON();
    }

    public function isApiMethodSupported(string $apiMethod): bool
    {
        return CommonMethod::isValid($apiMethod);
    }

    /**
     * @return ApiResponseInterface<CargoType>
     *
     * @see https://devcenter.novaposhta.ua/docs/services/55702570a0fe4f0cf4fc53ed/operations/55702571a0fe4f0b64838909
     */
    public function getCargoTypes(): ApiResponseInterface
    {
        return $this->sendWithTransform(CommonMethod::GET_CARGO_TYPES, CargoType::class);
    }

    /**
     * @return ApiResponseInterface<BackwardDeliveryCargoType>
     *
     * @see https://devcenter.novaposhta.ua/docs/services/55702570a0fe4f0cf4fc53ed/operations/55702571a0fe4f0b64838907
     */
    public function getBackwardDeliveryCargoTypes(): ApiResponseInterface
    {
        return $this->sendWithTransform(
            CommonMethod::GET_BACKWARD_DELIVERY_CARGO_TYPES,
            BackwardDeliveryCargoType::class
        );
    }

    /**
     * @return ApiResponseInterface<TypeOfPayers>
     *
     * @see https://devcenter.novaposhta.ua/docs/services/55702570a0fe4f0cf4fc53ed/operations/55702571a0fe4f0b64838913
     */
    public function getTypesOfPayers(): ApiResponseInterface
    {
        return $this->sendWithTransform(CommonMethod::GET_TYPES_OF_PAYERS, TypeOfPayers::class);
    }

    /**
     * @return ApiResponseInterface<TypeOfPayersForRedelivery>
     *
     * @see https://devcenter.novaposhta.ua/docs/services/55702570a0fe4f0cf4fc53ed/operations/55702571a0fe4f0b64838914
     */
    public function getTypesOfPayersForRedelivery(): ApiResponseInterface
    {
        return $this->sendWithTransform(
            CommonMethod::GET_TYPES_OF_PAYERS_FOR_REDELIVERY,
            TypeOfPayersForRedelivery::class
        );
    }

    /**
     * @param PaginationPropertiesDto|null $paginationProperties
     *
     * @return ApiResponseInterface<CargoDescription>
     *
     * @see https://devcenter.novaposhta.ua/docs/services/55702570a0fe4f0cf4fc53ed/operations/55702571a0fe4f0b64838908
     */
    public function getCargoDescriptionList(PaginationPropertiesDto $paginationProperties = null): ApiResponseInterface
    {
        $propertiesList = $paginationProperties ? [$paginationProperties] : null;

        return $this->sendWithTransform(
            CommonMethod::GET_CARGO_DESCRIPTION_LIST,
            CargoDescription::class,
            $propertiesList
        );
    }

    /**
     * @return ApiResponseInterface<ServiceType>
     *
     * @see https://devcenter.novaposhta.ua/docs/services/55702570a0fe4f0cf4fc53ed/operations/55702571a0fe4f0b6483890e
     */
    public function getServiceTypes(): ApiResponseInterface
    {
        return $this->sendWithTransform(CommonMethod::GET_SERVICE_TYPES, ServiceType::class);
    }

    /**
     * @return ApiResponseInterface<TypeOfCounterparties>
     *
     * @see https://devcenter.novaposhta.ua/docs/services/55702570a0fe4f0cf4fc53ed/operations/55702571a0fe4f0b64838912
     */
    public function getTypesOfCounterparties(): ApiResponseInterface
    {
        return $this->sendWithTransform(
            CommonMethod::GET_TYPES_OF_COUNTERPARTIES,
            TypeOfCounterparties::class
        );
    }

    /**
     * @return ApiResponseInterface<PaymentForm>
     *
     * @see https://devcenter.novaposhta.ua/docs/services/55702570a0fe4f0cf4fc53ed/operations/55702571a0fe4f0b6483890d
     */
    public function getPaymentForms(): ApiResponseInterface
    {
        return $this->sendWithTransform(CommonMethod::GET_PAYMENT_FORMS, PaymentForm::class);
    }

    /**
     * @return ApiResponseInterface<OwnershipForm>
     *
     * @see https://devcenter.novaposhta.ua/docs/services/55702570a0fe4f0cf4fc53ed/operations/55702571a0fe4f0b6483890b
     */
    public function getOwnershipFormsList(): ApiResponseInterface
    {
        return $this->sendWithTransform(
            CommonMethod::GET_OWNERSHIP_FORMS_LIST,
            OwnershipForm::class
        );
    }
}
