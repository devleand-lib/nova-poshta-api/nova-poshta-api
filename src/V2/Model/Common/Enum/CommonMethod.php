<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\Common\Enum;

use MyCLabs\Enum\Enum;

/**
 * @extends Enum<string>
 *
 * @method static CommonMethod GET_CARGO_TYPES()
 * @method static CommonMethod GET_BACKWARD_DELIVERY_CARGO_TYPES()
 * @method static CommonMethod GET_TYPES_OF_PAYERS()
 * @method static CommonMethod GET_TYPES_OF_PAYERS_FOR_REDELIVERY()
 * @method static CommonMethod GET_CARGO_DESCRIPTION_LIST()
 * @method static CommonMethod GET_SERVICE_TYPES()
 * @method static CommonMethod GET_TYPES_OF_COUNTERPARTIES()
 * @method static CommonMethod GET_PAYMENT_FORMS()
 * @method static CommonMethod GET_OWNERSHIP_FORMS_LIST()
 */
final class CommonMethod extends Enum
{
    public const GET_CARGO_TYPES = 'getCargoTypes';
    public const GET_BACKWARD_DELIVERY_CARGO_TYPES = 'getBackwardDeliveryCargoTypes';
    public const GET_TYPES_OF_PAYERS = 'getTypesOfPayers';
    public const GET_TYPES_OF_PAYERS_FOR_REDELIVERY = 'getTypesOfPayersForRedelivery';
    public const GET_CARGO_DESCRIPTION_LIST = 'getCargoDescriptionList';
    public const GET_SERVICE_TYPES = 'getServiceTypes';
    public const GET_TYPES_OF_COUNTERPARTIES = 'getTypesOfCounterparties';
    public const GET_PAYMENT_FORMS = 'getPaymentForms';
    public const GET_OWNERSHIP_FORMS_LIST = 'getOwnershipFormsList';
}
