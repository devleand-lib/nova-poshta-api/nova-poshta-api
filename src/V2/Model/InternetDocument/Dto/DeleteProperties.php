<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\InternetDocument\Dto;

use Devleand\NovaPoshta\Api\V2\Contracts\Model\ApiMethodProperties;
use JMS\Serializer\Annotation as Serializer;

class DeleteProperties implements ApiMethodProperties
{
    /**
     * @var string[]
     *
     * @Serializer\SerializedName("DocumentRefs")
     * @Serializer\Type("array<string>")
     */
    private array $documentRefs;

    /**
     * @param string[] $documentRefs
     */
    public function __construct(array $documentRefs)
    {
        $this->documentRefs = $documentRefs;
    }

    /**
     * @return string[]
     */
    public function getDocumentRefs(): array
    {
        return $this->documentRefs;
    }

    /**
     * @param string[] $documentRefs
     */
    public function setDocumentRefs(array $documentRefs): self
    {
        $this->documentRefs = $documentRefs;

        return $this;
    }
}
