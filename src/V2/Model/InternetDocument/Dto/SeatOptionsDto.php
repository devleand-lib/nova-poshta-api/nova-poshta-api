<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\InternetDocument\Dto;

use JMS\Serializer\Annotation as Serializer;

class SeatOptionsDto
{
    /**
     * @Serializer\SerializedName("weight")
     * @Serializer\Type("string")
     */
    private string $weight;

    /**
     * @Serializer\SerializedName("volumetricVolume")
     * @Serializer\Type("string")
     */
    private string $volumetricVolume;

    /**
     * @Serializer\SerializedName("volumetricWidth")
     * @Serializer\Type("string")
     */
    private string $volumetricWidth;

    /**
     * @Serializer\SerializedName("volumetricHeight")
     * @Serializer\Type("string")
     */
    private string $volumetricHeight;

    /**
     * @Serializer\SerializedName("volumetricLength")
     * @Serializer\Type("string")
     */
    private string $volumetricLength;

    public function __construct(
        string $weight,
        string $volumetricVolume,
        string $volumetricWidth,
        string $volumetricHeight,
        string $volumetricLength
    ) {
        $this->weight = $weight;
        $this->volumetricVolume = $volumetricVolume;
        $this->volumetricWidth = $volumetricWidth;
        $this->volumetricHeight = $volumetricHeight;
        $this->volumetricLength = $volumetricLength;
    }

    public function getWeight(): string
    {
        return $this->weight;
    }

    public function setWeight(string $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getVolumetricVolume(): string
    {
        return $this->volumetricVolume;
    }

    public function setVolumetricVolume(string $volumetricVolume): self
    {
        $this->volumetricVolume = $volumetricVolume;

        return $this;
    }

    public function getVolumetricWidth(): string
    {
        return $this->volumetricWidth;
    }

    public function setVolumetricWidth(string $volumetricWidth): self
    {
        $this->volumetricWidth = $volumetricWidth;

        return $this;
    }

    public function getVolumetricHeight(): string
    {
        return $this->volumetricHeight;
    }

    public function setVolumetricHeight(string $volumetricHeight): self
    {
        $this->volumetricHeight = $volumetricHeight;

        return $this;
    }

    public function getVolumetricLength(): string
    {
        return $this->volumetricLength;
    }

    public function setVolumetricLength(string $volumetricLength): self
    {
        $this->volumetricLength = $volumetricLength;

        return $this;
    }
}
