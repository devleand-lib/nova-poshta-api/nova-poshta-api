<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\InternetDocument\Dto;

use Devleand\NovaPoshta\Api\V2\Support\TypeCaster;
use JMS\Serializer\Annotation as Serializer;
use Devleand\NovaPoshta\Api\V2\Contracts\Model\ApiMethodProperties;

class InternetDocumentDto implements ApiMethodProperties
{
    /**
     * @Serializer\SerializedName("NewAddress")
     * @Serializer\Type("string")
     */
    private string $newAddress;

    /**
     * @Serializer\SerializedName("PayerType")
     * @Serializer\Type("string")
     */
    private string $payerType;

    /**
     * @Serializer\SerializedName("PaymentMethod")
     * @Serializer\Type("string")
     */
    private string $paymentMethod;

    /**
     * @Serializer\SerializedName("CargoType")
     * @Serializer\Type("string")
     */
    private string $cargoType;

    /**
     * @Serializer\SerializedName("VolumeGeneral")
     * @Serializer\Type("string")
     */
    private string $volumeGeneral;

    /**
     * @Serializer\SerializedName("Weight")
     * @Serializer\Type("string")
     */
    private string $weight;

    /**
     * @Serializer\SerializedName("ServiceType")
     * @Serializer\Type("string")
     */
    private string $serviceType;

    /**
     * @Serializer\SerializedName("SeatsAmount")
     * @Serializer\Type("string")
     */
    private string $seatsAmount;

    /**
     * @Serializer\SerializedName("Description")
     * @Serializer\Type("string")
     */
    private string $description;

    /**
     * @Serializer\SerializedName("Cost")
     * @Serializer\Type("string")
     */
    private string $cost;

    /**
     * @Serializer\SerializedName("CitySender")
     * @Serializer\Type("string")
     */
    private string $citySender;

    /**
     * @Serializer\SerializedName("Sender")
     * @Serializer\Type("string")
     */
    private string $sender;

    /**
     * @Serializer\SerializedName("SenderAddress")
     * @Serializer\Type("string")
     */
    private string $senderAddress;

    /**
     * @Serializer\SerializedName("ContactSender")
     * @Serializer\Type("string")
     */
    private string $contactSender;

    /**
     * @Serializer\SerializedName("SendersPhone")
     * @Serializer\Type("string")
     */
    private string $sendersPhone;

    /**
     * @Serializer\SerializedName("RecipientCityName")
     * @Serializer\Type("string")
     */
    private string $recipientCityName;

    /**
     * @Serializer\SerializedName("SettlementType")
     * @Serializer\Type("string")
     */
    private string $settlementType;

    /**
     * @Serializer\SerializedName("RecipientArea")
     * @Serializer\Type("string")
     */
    private string $recipientArea;

    /**
     * @Serializer\SerializedName("RecipientAreaRegions")
     * @Serializer\Type("string")
     */
    private ?string $recipientAreaRegions;

    /**
     * @Serializer\SerializedName("RecipientAddressName")
     * @Serializer\Type("string")
     */
    private string $recipientAddressName;

    /**
     * @Serializer\SerializedName("RecipientHouse")
     * @Serializer\Type("string")
     */
    private ?string $recipientHouse;

    /**
     * @Serializer\SerializedName("RecipientFlat")
     * @Serializer\Type("string")
     */
    private ?string $recipientFlat;

    /**
     * @Serializer\SerializedName("RecipientName")
     * @Serializer\Type("string")
     */
    private string $recipientName;

    /**
     * @Serializer\SerializedName("RecipientType")
     * @Serializer\Type("string")
     */
    private string $recipientType;

    /**
     * @Serializer\SerializedName("RecipientsPhone")
     * @Serializer\Type("string")
     */
    private string $recipientsPhone;

    /**
     * @Serializer\SerializedName("DateTime")
     * @Serializer\Type("string")
     */
    private string $dateTime;

    /**
     * @Serializer\SkipWhenEmpty()
     * @Serializer\SerializedName("RecipientAddressNote")
     * @Serializer\Type("string")
     */
    private ?string $recipientAddressNote = null;

    /**
     * @var SeatOptionsDto[]|null
     *
     * @Serializer\SkipWhenEmpty()
     * @Serializer\SerializedName("OptionsSeat")
     * @Serializer\Type("array<Devleand\NovaPoshta\Api\V2\Model\InternetDocument\Dto\SeatOptionsDto>")
     */
    private ?array $optionsSeat = null;

    /**
     * @Serializer\SkipWhenEmpty()
     * @Serializer\SerializedName("AfterpaymentOnGoodsCost")
     * @Serializer\Type("string")
     */
    private ?string $afterPaymentOnGoodsCost = null;

    /**
     * @var BackwardDeliveryDto[]|null
     *
     * @Serializer\SkipWhenEmpty()
     * @Serializer\SerializedName("BackwardDeliveryData")
     * @Serializer\Type("array<Devleand\NovaPoshta\Api\V2\Model\InternetDocument\Dto\BackwardDeliveryDto>")
     */
    private ?array $backwardDeliveryData = null;

    public function isNewAddress(): bool
    {
        return TypeCaster::stringToBool($this->newAddress);
    }

    public function setNewAddress(bool $newAddress): self
    {
        $this->newAddress = TypeCaster::boolToString($newAddress);

        return $this;
    }

    public function getPayerType(): string
    {
        return $this->payerType;
    }

    public function setPayerType(string $payerType): self
    {
        $this->payerType = $payerType;

        return $this;
    }

    public function getPaymentMethod(): string
    {
        return $this->paymentMethod;
    }

    public function setPaymentMethod(string $paymentMethod): self
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    public function getCargoType(): string
    {
        return $this->cargoType;
    }

    public function setCargoType(string $cargoType): self
    {
        $this->cargoType = $cargoType;

        return $this;
    }

    public function getVolumeGeneral(): string
    {
        return $this->volumeGeneral;
    }

    public function setVolumeGeneral(string $volumeGeneral): self
    {
        $this->volumeGeneral = $volumeGeneral;

        return $this;
    }

    public function getWeight(): string
    {
        return $this->weight;
    }

    public function setWeight(string $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getServiceType(): string
    {
        return $this->serviceType;
    }

    public function setServiceType(string $serviceType): self
    {
        $this->serviceType = $serviceType;

        return $this;
    }

    public function getSeatsAmount(): string
    {
        return $this->seatsAmount;
    }

    public function setSeatsAmount(string $seatsAmount): self
    {
        $this->seatsAmount = $seatsAmount;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCost(): string
    {
        return $this->cost;
    }

    public function setCost(string $cost): self
    {
        $this->cost = $cost;

        return $this;
    }

    public function getCitySender(): string
    {
        return $this->citySender;
    }

    public function setCitySender(string $citySender): self
    {
        $this->citySender = $citySender;

        return $this;
    }

    public function getSender(): string
    {
        return $this->sender;
    }

    public function setSender(string $sender): self
    {
        $this->sender = $sender;

        return $this;
    }

    public function getSenderAddress(): string
    {
        return $this->senderAddress;
    }

    public function setSenderAddress(string $senderAddress): self
    {
        $this->senderAddress = $senderAddress;

        return $this;
    }

    public function getContactSender(): string
    {
        return $this->contactSender;
    }

    public function setContactSender(string $contactSender): self
    {
        $this->contactSender = $contactSender;

        return $this;
    }

    public function getSendersPhone(): string
    {
        return $this->sendersPhone;
    }

    public function setSendersPhone(string $sendersPhone): self
    {
        $this->sendersPhone = $sendersPhone;

        return $this;
    }

    public function getRecipientCityName(): string
    {
        return $this->recipientCityName;
    }

    public function setRecipientCityName(string $recipientCityName): self
    {
        $this->recipientCityName = $recipientCityName;

        return $this;
    }

    public function getSettlementType(): string
    {
        return $this->settlementType;
    }

    public function setSettlementType(string $settlementType): self
    {
        $this->settlementType = $settlementType;

        return $this;
    }

    public function getRecipientArea(): string
    {
        return $this->recipientArea;
    }

    public function setRecipientArea(string $recipientArea): self
    {
        $this->recipientArea = $recipientArea;

        return $this;
    }

    public function getRecipientAreaRegions(): ?string
    {
        return $this->recipientAreaRegions;
    }

    public function setRecipientAreaRegions(?string $recipientAreaRegions): self
    {
        $this->recipientAreaRegions = $recipientAreaRegions;

        return $this;
    }

    public function getRecipientAddressName(): string
    {
        return $this->recipientAddressName;
    }

    public function setRecipientAddressName(string $recipientAddressName): self
    {
        $this->recipientAddressName = $recipientAddressName;

        return $this;
    }

    public function getRecipientHouse(): ?string
    {
        return $this->recipientHouse;
    }

    public function setRecipientHouse(?string $recipientHouse): self
    {
        $this->recipientHouse = $recipientHouse;

        return $this;
    }

    public function getRecipientFlat(): ?string
    {
        return $this->recipientFlat;
    }

    public function setRecipientFlat(?string $recipientFlat): self
    {
        $this->recipientFlat = $recipientFlat;

        return $this;
    }

    public function getRecipientName(): string
    {
        return $this->recipientName;
    }

    public function setRecipientName(string $recipientName): self
    {
        $this->recipientName = $recipientName;

        return $this;
    }

    public function getRecipientType(): string
    {
        return $this->recipientType;
    }

    public function setRecipientType(string $recipientType): self
    {
        $this->recipientType = $recipientType;

        return $this;
    }

    public function getRecipientsPhone(): string
    {
        return $this->recipientsPhone;
    }

    public function setRecipientsPhone(string $recipientsPhone): self
    {
        $this->recipientsPhone = $recipientsPhone;

        return $this;
    }

    public function getDateTime(): string
    {
        return $this->dateTime;
    }

    public function setDateTime(string $dateTime): self
    {
        $this->dateTime = $dateTime;

        return $this;
    }

    public function getRecipientAddressNote(): ?string
    {
        return $this->recipientAddressNote;
    }

    public function setRecipientAddressNote(?string $recipientAddressNote): self
    {
        $this->recipientAddressNote = $recipientAddressNote;

        return $this;
    }

    /**
     * @return SeatOptionsDto[]|null
     */
    public function getOptionsSeat(): ?array
    {
        return $this->optionsSeat;
    }

    /**
     * @param SeatOptionsDto[]|null $optionsSeat
     */
    public function setOptionsSeat(?array $optionsSeat): self
    {
        $this->optionsSeat = $optionsSeat;

        return $this;
    }

    public function getAfterPaymentOnGoodsCost(): ?string
    {
        return $this->afterPaymentOnGoodsCost;
    }

    public function setAfterPaymentOnGoodsCost(?string $afterPaymentOnGoodsCost): self
    {
        $this->afterPaymentOnGoodsCost = $afterPaymentOnGoodsCost;

        return $this;
    }

    /**
     * @return BackwardDeliveryDto[]|null
     */
    public function getBackwardDeliveryData(): ?array
    {
        return $this->backwardDeliveryData;
    }

    /**
     * @param BackwardDeliveryDto[]|null $backwardDeliveryData
     */
    public function setBackwardDeliveryData(?array $backwardDeliveryData): self
    {
        $this->backwardDeliveryData = $backwardDeliveryData;

        return $this;
    }
}
