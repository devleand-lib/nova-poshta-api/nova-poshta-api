<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\InternetDocument\Dto;

use Devleand\NovaPoshta\Api\V2\Contracts\Model\ApiMethodProperties;
use Devleand\NovaPoshta\Api\V2\Support\TypeCaster;
use JMS\Serializer\Annotation as Serializer;

class GetDocumentListPropertiesDto implements ApiMethodProperties
{
    /**
     * @Serializer\SerializedName("DateTimeFrom")
     * @Serializer\Type("string")
     */
    private string $dateTimeFrom;

    /**
     * @Serializer\SerializedName("DateTimeTo")
     * @Serializer\Type("string")
     */
    private string $dateTimeTo;

    /**
     * @Serializer\SerializedName("GetFullList")
     * @Serializer\Type("string")
     */
    private string $getFullList = TypeCaster::FALSE_STRING;

    /**
     * @Serializer\SkipWhenEmpty()
     * @Serializer\SerializedName("Page")
     * @Serializer\Type("string")
     */
    private ?string $page = null;

    /**
     * @Serializer\SkipWhenEmpty()
     * @Serializer\SerializedName("DateTime")
     * @Serializer\Type("string")
     */
    private ?string $dateTime = null;

    public function __construct(string $dateTimeFrom, string $dateTimeTo)
    {
        $this->dateTimeFrom = $dateTimeFrom;
        $this->dateTimeTo = $dateTimeTo;
    }

    public function getDateTimeFrom(): string
    {
        return $this->dateTimeFrom;
    }

    public function setDateTimeFrom(string $dateTimeFrom): self
    {
        $this->dateTimeFrom = $dateTimeFrom;

        return $this;
    }

    public function getDateTimeTo(): string
    {
        return $this->dateTimeTo;
    }

    public function setDateTimeTo(string $dateTimeTo): self
    {
        $this->dateTimeTo = $dateTimeTo;

        return $this;
    }

    public function isGetFullList(): bool
    {
        return TypeCaster::stringToBool($this->getFullList);
    }

    public function setGetFullList(bool $getFullList): self
    {
        $this->getFullList = TypeCaster::boolToString($getFullList);

        return $this;
    }

    public function getDateTime(): ?string
    {
        return $this->dateTime;
    }

    public function setDateTime(?string $dateTime): self
    {
        $this->dateTime = $dateTime;

        return $this;
    }

    public function getPage(): ?string
    {
        return $this->page;
    }

    public function setPage(?string $page): self
    {
        $this->page = $page;

        return $this;
    }
}
