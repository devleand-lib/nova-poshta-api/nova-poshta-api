<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\InternetDocument\Dto;

use JMS\Serializer\Annotation as Serializer;

class BackwardDeliveryDto
{
    /**
     * @Serializer\SerializedName("PayerType")
     * @Serializer\Type("string")
     */
    private string $payerType;

    /**
     * @Serializer\SerializedName("RedeliveryString")
     * @Serializer\Type("string")
     */
    private string $redeliveryString;

    /**
     * @Serializer\SerializedName("CargoType")
     * @Serializer\Type("string")
     */
    private string $cargoType;

    public function __construct(string $payerType, string $redeliveryString, string $cargoType)
    {
        $this->payerType = $payerType;
        $this->redeliveryString = $redeliveryString;
        $this->cargoType = $cargoType;
    }

    public function getPayerType(): string
    {
        return $this->payerType;
    }

    public function setPayerType(string $payerType): self
    {
        $this->payerType = $payerType;

        return $this;
    }

    public function getRedeliveryString(): string
    {
        return $this->redeliveryString;
    }

    public function setRedeliveryString(string $redeliveryString): self
    {
        $this->redeliveryString = $redeliveryString;

        return $this;
    }

    public function getCargoType(): string
    {
        return $this->cargoType;
    }

    public function setCargoType(string $cargoType): self
    {
        $this->cargoType = $cargoType;

        return $this;
    }
}
