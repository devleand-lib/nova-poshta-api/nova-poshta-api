<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\InternetDocument\Dto;

use Devleand\NovaPoshta\Api\V2\Contracts\Model\ApiMethodProperties;
use JMS\Serializer\Annotation as Serializer;

class InternetDocumentRefDto implements ApiMethodProperties
{
    /**
     * @Serializer\SerializedName("Ref")
     * @Serializer\Type("string")
     */
    private string $ref;

    public function __construct(string $ref)
    {
        $this->ref = $ref;
    }

    public function getRef(): string
    {
        return $this->ref;
    }
}
