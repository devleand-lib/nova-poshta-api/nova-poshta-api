<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\InternetDocument\Enum;

use MyCLabs\Enum\Enum;

/**
 * @extends Enum<string>
 *
 * @method static InternetDocumentMethod GET_DOCUMENT_LIST()
 * @method static InternetDocumentMethod SAVE()
 * @method static InternetDocumentMethod UPDATE()
 * @method static InternetDocumentMethod DELETE()
 */
final class InternetDocumentMethod extends Enum
{
    public const GET_DOCUMENT_LIST = 'getDocumentList';
    public const SAVE = 'save';
    public const UPDATE = 'update';
    public const DELETE = 'delete';
}
