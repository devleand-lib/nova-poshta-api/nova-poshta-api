<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\InternetDocument\Entity;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

final class InternetDocument
{
    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("Ref")
     * @Serializer\Type("string")
     */
    private string $ref;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("IntDocNumber")
     * @Serializer\Type("string")
     */
    private string $intDocNumber;

    /**
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("DateTime")
     * @Serializer\Type("string")
     */
    private string $dateTime;

    /**
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("PreferredDeliveryDate")
     * @Serializer\Type("string")
     */
    private ?string $preferredDeliveryDate;

    /**
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("RecipientDateTime")
     * @Serializer\Type("string")
     */
    private ?string $recipientDateTime;

    /**
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("EWDateCreated")
     * @Serializer\Type("string")
     */
    private ?string $expressWaybillDateCreated;

    /**
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("Weight")
     * @Serializer\Type("string")
     */
    private string $weight;

    /**
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("SeatsAmount")
     * @Serializer\Type("string")
     */
    private string $seatsAmount;

    /**
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("Cost")
     * @Serializer\Type("string")
     */
    private string $cost;

    /**
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("CostOnSite")
     * @Serializer\Type("string")
     */
    private string $costOnSite;

    /**
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("CitySender")
     * @Serializer\Type("string")
     */
    private string $citySender;

    /**
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("SenderAddress")
     * @Serializer\Type("string")
     */
    private string $senderAddress;

    /**
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("CityRecipient")
     * @Serializer\Type("string")
     */
    private string $cityRecipient;

    /**
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("RecipientAddress")
     * @Serializer\Type("string")
     */
    private string $recipientAddress;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("PayerType")
     * @Serializer\Type("string")
     */
    private string $payerType;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("PaymentMethod")
     * @Serializer\Type("string")
     */
    private string $paymentMethod;

    /**
     * @Serializer\SerializedName("AfterpaymentOnGoodsCost")
     * @Serializer\Type("string")
     */
    private ?string $afterPaymentOnGoodsCost = null;

    /**
     * @Serializer\SerializedName("PackingNumber")
     * @Serializer\Type("string")
     */
    private ?string $packingNumber = null;

    /**
     * @Serializer\SerializedName("RejectionReason")
     * @Serializer\Type("string")
     */
    private ?string $rejectionReason = null;

    /**
     * @Serializer\SerializedName("StateId")
     * @Serializer\Type("string")
     */
    private ?string $stateId = null;

    /**
     * @Serializer\SerializedName("StateName")
     * @Serializer\Type("string")
     */
    private ?string $stateName = null;

    public function getRef(): string
    {
        return $this->ref;
    }

    public function setRef(string $ref): self
    {
        $this->ref = $ref;

        return $this;
    }

    public function getIntDocNumber(): string
    {
        return $this->intDocNumber;
    }

    public function setIntDocNumber(string $intDocNumber): self
    {
        $this->intDocNumber = $intDocNumber;

        return $this;
    }

    public function getDateTime(): string
    {
        return $this->dateTime;
    }

    public function setDateTime(string $dateTime): self
    {
        $this->dateTime = $dateTime;

        return $this;
    }

    public function getPreferredDeliveryDate(): ?string
    {
        return $this->preferredDeliveryDate;
    }

    public function setPreferredDeliveryDate(?string $preferredDeliveryDate): self
    {
        $this->preferredDeliveryDate = $preferredDeliveryDate;

        return $this;
    }

    public function getRecipientDateTime(): ?string
    {
        return $this->recipientDateTime;
    }

    public function setRecipientDateTime(?string $recipientDateTime): self
    {
        $this->recipientDateTime = $recipientDateTime;

        return $this;
    }

    public function getExpressWaybillDateCreated(): ?string
    {
        return $this->expressWaybillDateCreated;
    }

    public function setExpressWaybillDateCreated(?string $expressWaybillDateCreated): self
    {
        $this->expressWaybillDateCreated = $expressWaybillDateCreated;

        return $this;
    }

    public function getWeight(): string
    {
        return $this->weight;
    }

    public function setWeight(string $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getSeatsAmount(): string
    {
        return $this->seatsAmount;
    }

    public function setSeatsAmount(string $seatsAmount): self
    {
        $this->seatsAmount = $seatsAmount;

        return $this;
    }

    public function getCost(): string
    {
        return $this->cost;
    }

    public function setCost(string $cost): self
    {
        $this->cost = $cost;

        return $this;
    }

    public function getCostOnSite(): string
    {
        return $this->costOnSite;
    }

    public function setCostOnSite(string $costOnSite): self
    {
        $this->costOnSite = $costOnSite;

        return $this;
    }

    public function getCitySender(): string
    {
        return $this->citySender;
    }

    public function setCitySender(string $citySender): self
    {
        $this->citySender = $citySender;

        return $this;
    }

    public function getSenderAddress(): string
    {
        return $this->senderAddress;
    }

    public function setSenderAddress(string $senderAddress): self
    {
        $this->senderAddress = $senderAddress;

        return $this;
    }

    public function getCityRecipient(): string
    {
        return $this->cityRecipient;
    }

    public function setCityRecipient(string $cityRecipient): self
    {
        $this->cityRecipient = $cityRecipient;

        return $this;
    }

    public function getRecipientAddress(): string
    {
        return $this->recipientAddress;
    }

    public function setRecipientAddress(string $recipientAddress): self
    {
        $this->recipientAddress = $recipientAddress;

        return $this;
    }

    public function getPayerType(): string
    {
        return $this->payerType;
    }

    public function setPayerType(string $payerType): self
    {
        $this->payerType = $payerType;

        return $this;
    }

    public function getPaymentMethod(): string
    {
        return $this->paymentMethod;
    }

    public function setPaymentMethod(string $paymentMethod): self
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    public function getAfterPaymentOnGoodsCost(): ?string
    {
        return $this->afterPaymentOnGoodsCost;
    }

    public function setAfterPaymentOnGoodsCost(?string $afterPaymentOnGoodsCost): self
    {
        $this->afterPaymentOnGoodsCost = $afterPaymentOnGoodsCost;

        return $this;
    }

    public function getPackingNumber(): ?string
    {
        return $this->packingNumber;
    }

    public function setPackingNumber(?string $packingNumber): self
    {
        $this->packingNumber = $packingNumber;

        return $this;
    }

    public function getRejectionReason(): ?string
    {
        return $this->rejectionReason;
    }

    public function setRejectionReason(?string $rejectionReason): self
    {
        $this->rejectionReason = $rejectionReason;

        return $this;
    }

    public function getStateId(): ?string
    {
        return $this->stateId;
    }

    public function setStateId(?string $stateId): self
    {
        $this->stateId = $stateId;

        return $this;
    }

    public function getStateName(): ?string
    {
        return $this->stateName;
    }

    public function setStateName(?string $stateName): self
    {
        $this->stateName = $stateName;

        return $this;
    }
}
