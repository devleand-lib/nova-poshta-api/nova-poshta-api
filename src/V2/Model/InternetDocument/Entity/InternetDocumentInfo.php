<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\InternetDocument\Entity;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

final class InternetDocumentInfo
{
    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("Ref")
     * @Serializer\Type("string")
     */
    private string $ref;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("IntDocNumber")
     * @Serializer\Type("string")
     */
    private string $intDocNumber;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("CostOnSite")
     * @Serializer\Type("string")
     */
    private string $costOnSite;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("EstimatedDeliveryDate")
     * @Serializer\Type("string")
     */
    private string $estimatedDeliveryDate;

    public function getRef(): string
    {
        return $this->ref;
    }

    public function setRef(string $ref): self
    {
        $this->ref = $ref;

        return $this;
    }

    public function getIntDocNumber(): string
    {
        return $this->intDocNumber;
    }

    public function setIntDocNumber(string $intDocNumber): self
    {
        $this->intDocNumber = $intDocNumber;

        return $this;
    }

    public function getCostOnSite(): string
    {
        return $this->costOnSite;
    }

    public function setCostOnSite(string $costOnSite): self
    {
        $this->costOnSite = $costOnSite;

        return $this;
    }

    public function getEstimatedDeliveryDate(): string
    {
        return $this->estimatedDeliveryDate;
    }

    public function setEstimatedDeliveryDate(string $estimatedDeliveryDate): self
    {
        $this->estimatedDeliveryDate = $estimatedDeliveryDate;

        return $this;
    }
}
