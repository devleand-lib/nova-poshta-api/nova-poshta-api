<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\InternetDocument;

use Devleand\NovaPoshta\Api\V2\Contracts\Response\ApiResponseInterface;
use Devleand\NovaPoshta\Api\V2\Model\Enum\ApiModel;
use Devleand\NovaPoshta\Api\V2\Model\InternetDocument\Dto\DeleteProperties;
use Devleand\NovaPoshta\Api\V2\Model\InternetDocument\Dto\GetDocumentListPropertiesDto;
use Devleand\NovaPoshta\Api\V2\Model\InternetDocument\Dto\InternetDocumentDto;
use Devleand\NovaPoshta\Api\V2\Model\InternetDocument\Dto\InternetDocumentRefDto;
use Devleand\NovaPoshta\Api\V2\Model\InternetDocument\Entity\InternetDocument;
use Devleand\NovaPoshta\Api\V2\Model\InternetDocument\Entity\InternetDocumentInfo;
use Devleand\NovaPoshta\Api\V2\Model\InternetDocument\Entity\InternetDocumentRef;
use Devleand\NovaPoshta\Api\V2\Model\InternetDocument\Enum\InternetDocumentMethod;
use Devleand\NovaPoshta\Api\V2\Model\Model;

class InternetDocumentModel extends Model
{
    public function getApiModel(): ApiModel
    {
        return ApiModel::INTERNET_DOCUMENT();
    }

    public function isApiMethodSupported(string $apiMethod): bool
    {
        return InternetDocumentMethod::isValid($apiMethod);
    }

    /**
     * @return ApiResponseInterface<InternetDocument>
     *
     * @see https://developers.novaposhta.ua/view/model/a90d323c-8512-11ec-8ced-005056b2dbe1/method/a9d22b34-8512-11ec-8ced-005056b2dbe1
     */
    public function getDocumentList(GetDocumentListPropertiesDto $properties): ApiResponseInterface
    {
        return $this->sendWithTransform(
            InternetDocumentMethod::GET_DOCUMENT_LIST,
            InternetDocument::class,
            [$properties]
        );
    }

    /**
     * @return ApiResponseInterface<InternetDocumentInfo>
     *
     * @see https://developers.novaposhta.ua/view/model/a90d323c-8512-11ec-8ced-005056b2dbe1/method/a965630e-8512-11ec-8ced-005056b2dbe1
     */
    public function save(InternetDocumentDto $internetDocument): ApiResponseInterface
    {
        return $this->sendWithTransform(
            InternetDocumentMethod::SAVE,
            InternetDocumentInfo::class,
            [$internetDocument]
        );
    }

    /**
     * @return ApiResponseInterface<InternetDocumentInfo>
     *
     * @see https://developers.novaposhta.ua/view/model/a90d323c-8512-11ec-8ced-005056b2dbe1/method/a98a4354-8512-11ec-8ced-005056b2dbe1
     */
    public function update(string $ref, InternetDocumentDto $internetDocument): ApiResponseInterface
    {
        return $this->sendWithTransform(
            InternetDocumentMethod::UPDATE,
            InternetDocumentInfo::class,
            [new InternetDocumentRefDto($ref), $internetDocument]
        );
    }

    /**
     * @param string[] $refs
     *
     * @return ApiResponseInterface<InternetDocumentRef>
     *
     * @see https://developers.novaposhta.ua/view/model/a90d323c-8512-11ec-8ced-005056b2dbe1/method/a9f43ff1-8512-11ec-8ced-005056b2dbe1
     */
    public function delete(array $refs): ApiResponseInterface
    {
        return $this->sendWithTransform(
            InternetDocumentMethod::DELETE,
            InternetDocumentRef::class,
            [new DeleteProperties($refs)]
        );
    }
}
