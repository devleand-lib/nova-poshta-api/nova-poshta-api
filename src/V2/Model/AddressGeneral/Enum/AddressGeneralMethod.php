<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Enum;

use MyCLabs\Enum\Enum;

/**
 * @extends Enum<string>
 *
 * @method static AddressGeneralMethod GET_SETTLEMENTS()
 * @method static AddressGeneralMethod GET_WAREHOUSE_TYPES()
 * @method static AddressGeneralMethod GET_WAREHOUSES()
 */
final class AddressGeneralMethod extends Enum
{
    public const GET_SETTLEMENTS = 'getSettlements';
    public const GET_WAREHOUSE_TYPES = 'getWarehouseTypes';
    public const GET_WAREHOUSES = 'getWarehouses';
}
