<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Entity;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

final class ShipmentDimensions
{
    /**
     * @var int
     *
     * @Assert\NotBlank()
     * @Assert\Type("integer")
     *
     * @Serializer\SerializedName("Width")
     * @Serializer\Type("integer")
     */
    private int $weight;

    /**
     * @var int
     *
     * @Assert\NotBlank()
     * @Assert\Type("integer")
     *
     * @Serializer\SerializedName("Height")
     * @Serializer\Type("integer")
     */
    private int $height;

    /**
     * @var int
     *
     * @Assert\NotBlank()
     * @Assert\Type("integer")
     *
     * @Serializer\SerializedName("Length")
     * @Serializer\Type("integer")
     */
    private int $length;

    public function __construct(int $weight, int $height, int $length)
    {
        $this->weight = $weight;
        $this->height = $height;
        $this->length = $length;
    }

    public function getWeight(): int
    {
        return $this->weight;
    }

    public function getHeight(): int
    {
        return $this->height;
    }

    public function getLength(): int
    {
        return $this->length;
    }
}
