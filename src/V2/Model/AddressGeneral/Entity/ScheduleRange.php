<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Entity;

use Carbon\Carbon;
use Carbon\Exceptions\InvalidFormatException;
use DateTimeInterface;

final class ScheduleRange
{
    /**
     * @var \DateTimeInterface
     */
    private DateTimeInterface $startTime;

    /**
     * @var \DateTimeInterface
     */
    private DateTimeInterface $endTime;

    public function __construct(DateTimeInterface $startTime, DateTimeInterface $endTime)
    {
        $this->startTime = $startTime;
        $this->endTime = $endTime;
    }

    public function getStartTime(): DateTimeInterface
    {
        return $this->startTime;
    }

    public function getEndTime(): DateTimeInterface
    {
        return $this->endTime;
    }

    /**
     * @param string $dateRange
     *
     * @return static|null
     *
     * @throws InvalidFormatException
     */
    public static function parseOrNull(string $dateRange): ?self
    {
        $dateRangeList = explode('-', $dateRange);

        if (count($dateRangeList) === 1 || empty($dateRangeList[0]) || empty($dateRangeList[1])) {
            return null;
        }

        $startTime = Carbon::parse($dateRangeList[0]);
        $endTime = Carbon::parse($dateRangeList[1]);

        return new self($startTime, $endTime);
    }
}
