<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Entity;

use DateTimeInterface;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

final class Warehouse
{
    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("Ref")
     * @Serializer\Type("string")
     */
    private string $ref;

    /**
     * Warehouse code.
     *
     * @var numeric-string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("SiteKey")
     * @Serializer\Type("string")
     */
    private string $siteKey;

    /**
     * @var numeric-string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("Number")
     * @Serializer\Type("string")
     */
    private string $number;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("TypeOfWarehouse")
     * @Serializer\Type("string")
     */
    private string $warehouseTypeRef;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("CityRef")
     * @Serializer\Type("string")
     */
    private string $cityRef;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("CityDescription")
     * @Serializer\Type("string")
     */
    private string $cityDescriptionUk;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("CityDescriptionRu")
     * @Serializer\Type("string")
     */
    private string $cityDescriptionRu;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("SettlementRef")
     * @Serializer\Type("string")
     */
    private string $settlementRef;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("SettlementDescription")
     * @Serializer\Type("string")
     */
    private string $settlementDescriptionUk;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("SettlementAreaDescription")
     * @Serializer\Type("string")
     */
    private string $settlementAreaDescriptionUk;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("SettlementRegionsDescription")
     * @Serializer\Type("string")
     */
    private string $settlementRegionDescriptionUk;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("SettlementTypeDescription")
     * @Serializer\Type("string")
     */
    private string $settlementTypeDescriptionUk;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("SettlementTypeDescriptionRu")
     * @Serializer\Type("string")
     */
    private string $settlementTypeDescriptionRu;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("Description")
     * @Serializer\Type("string")
     */
    private string $descriptionUk;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("DescriptionRu")
     * @Serializer\Type("string")
     */
    private string $descriptionRu;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("ShortAddress")
     * @Serializer\Type("string")
     */
    private string $shortAddressUk;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("ShortAddressRu")
     * @Serializer\Type("string")
     */
    private string $shortAddressRu;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("Phone")
     * @Serializer\Type("string")
     */
    private string $phone;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("Latitude")
     * @Serializer\Type("string")
     */
    private string $latitude;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("Longitude")
     * @Serializer\Type("string")
     */
    private string $longitude;

    /**
     * @var numeric-string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("TotalMaxWeightAllowed")
     * @Serializer\Type("string")
     */
    private string $totalMaxWeightAllowed;

    /**
     * @var numeric-string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("PlaceMaxWeightAllowed")
     * @Serializer\Type("string")
     */
    private string $placeMaxWeightAllowed;

    /**
     * Maximum dimensions of a shipment for shipment.
     *
     * @var ShipmentDimensions
     *
     * @Assert\NotBlank()
     * @Assert\Type("Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Entity\ShipmentDimensions")
     *
     * @Serializer\SerializedName("SendingLimitationsOnDimensions")
     * @Serializer\Type("Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Entity\ShipmentDimensions")
     */
    private ShipmentDimensions $sendingLimitationsOnDimensions;

    /**
     * Maximum dimensions of a shipment to receive.
     *
     * @var ShipmentDimensions
     *
     * @Assert\NotBlank()
     * @Assert\Type("Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Entity\ShipmentDimensions")
     *
     * @Serializer\SerializedName("ReceivingLimitationsOnDimensions")
     * @Serializer\Type("Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Entity\ShipmentDimensions")
     */
    private ShipmentDimensions $receivingLimitationsOnDimensions;

    /**
     * Schedule for receiving parcels.
     *
     * @var Schedule
     *
     * @Assert\NotBlank()
     * @Assert\Type("Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Entity\Schedule")
     *
     * @Serializer\SerializedName("Reception")
     * @Serializer\Type("Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Entity\Schedule")
     */
    private Schedule $receptionSchedule;

    /**
     * Dispatch schedule day to day.
     *
     * @var Schedule
     *
     * @Assert\NotBlank()
     * @Assert\Type("Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Entity\Schedule")
     *
     * @Serializer\SerializedName("Delivery")
     * @Serializer\Type("Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Entity\Schedule")
     */
    private Schedule $deliverySchedule;

    /**
     * @var Schedule
     *
     * @Assert\NotBlank()
     * @Assert\Type("Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Entity\Schedule")
     *
     * @Serializer\SerializedName("Schedule")
     * @Serializer\Type("Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Entity\Schedule")
     */
    private Schedule $schedule;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("DistrictCode")
     * @Serializer\Type("string")
     */
    private string $districtCode;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("CategoryOfWarehouse")
     * @Serializer\Type("string")
     */
    private string $warehouseCategory;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("WarehouseStatus")
     * @Serializer\Type("string")
     */
    private string $warehouseStatus;

    /**
     * @var \DateTimeInterface
     *
     * @Assert\NotBlank()
     * @Assert\Type("DateTimeInterface")
     *
     * @Serializer\SerializedName("WarehouseStatusDate")
     * @Serializer\Type("DateTime<'Y-m-d H:i:s'>")
     */
    private DateTimeInterface $warehouseStatusDate;

    /**
     * @var string|null
     *
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("RegionCity")
     * @Serializer\Type("string")
     */
    private ?string $regionCityUk;

    /**
     * @var numeric-string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("MaxDeclaredCost")
     * @Serializer\Type("string")
     */
    private string $maxDeclaredCost;

    /**
     * Availability of Post-Finance cash desk.
     *
     * @var bool
     *
     * @Assert\NotBlank()
     * @Assert\Type("boolean")
     *
     * @Serializer\SerializedName("PostFinance")
     * @Serializer\Type("boolean")
     */
    private bool $isAvailablePostFinance;

    /**
     * @var bool
     *
     * @Assert\NotBlank()
     * @Assert\Type("boolean")
     *
     * @Serializer\SerializedName("BicycleParking")
     * @Serializer\Type("boolean")
     */
    private bool $isAvailableBicycleParking;

    /**
     * @var bool
     *
     * @Assert\NotBlank()
     * @Assert\Type("boolean")
     *
     * @Serializer\SerializedName("PaymentAccess")
     * @Serializer\Type("boolean")
     */
    private bool $isAvailablePaymentAccess;

    /**
     * Pos-terminal at the warehouse.
     *
     * @var bool
     *
     * @Assert\NotBlank()
     * @Assert\Type("boolean")
     *
     * @Serializer\SerializedName("POSTerminal")
     * @Serializer\Type("boolean")
     */
    private bool $isAvailablePOSTerminal;

    /**
     * Possibility of registration of international shipment.
     *
     * @var bool
     *
     * @Assert\NotBlank()
     * @Assert\Type("boolean")
     *
     * @Serializer\SerializedName("InternationalShipping")
     * @Serializer\Type("boolean")
     */
    private bool $isAvailableInternationalShipping;

    /**
     * Self-service workplace.
     *
     * @var bool
     *
     * @Assert\NotBlank()
     * @Assert\Type("boolean")
     *
     * @Serializer\SerializedName("SelfServiceWorkplacesCount")
     * @Serializer\Type("boolean")
     */
    private bool $isAvailableSelfServiceWorkplaces;

    /**
     * Affiliation of the branch to the franchise network.
     *
     * @var bool
     *
     * @Assert\NotBlank()
     * @Assert\Type("boolean")
     *
     * @Serializer\SerializedName("WarehouseForAgent")
     * @Serializer\Type("boolean")
     */
    private bool $isWarehouseForAgent;

    /**
     * @var bool
     *
     * @Assert\NotBlank()
     * @Assert\Type("boolean")
     *
     * @Serializer\SerializedName("WorkInMobileAwis")
     * @Serializer\Type("boolean")
     */
    private bool $isWorkInMobileAwis;

    /**
     * @param string             $ref
     * @param numeric-string     $siteKey
     * @param numeric-string     $number
     * @param string             $warehouseTypeRef
     * @param string             $cityRef
     * @param string             $cityDescriptionUk
     * @param string             $cityDescriptionRu
     * @param string             $settlementRef
     * @param string             $settlementDescriptionUk
     * @param string             $settlementAreaDescriptionUk
     * @param string             $settlementRegionDescriptionUk
     * @param string             $settlementTypeDescriptionUk
     * @param string             $settlementTypeDescriptionRu
     * @param string             $descriptionUk
     * @param string             $descriptionRu
     * @param string             $shortAddressUk
     * @param string             $shortAddressRu
     * @param string             $phone
     * @param string             $latitude
     * @param string             $longitude
     * @param numeric-string     $totalMaxWeightAllowed
     * @param numeric-string     $placeMaxWeightAllowed
     * @param ShipmentDimensions $sendingLimitationsOnDimensions
     * @param ShipmentDimensions $receivingLimitationsOnDimensions
     * @param Schedule           $receptionSchedule
     * @param Schedule           $deliverySchedule
     * @param Schedule           $schedule
     * @param string             $districtCode
     * @param string             $warehouseCategory
     * @param string             $warehouseStatus
     * @param \DateTimeInterface $warehouseStatusDate
     * @param string|null        $regionCityUk
     * @param numeric-string     $maxDeclaredCost
     * @param bool               $isAvailablePostFinance
     * @param bool               $isAvailableBicycleParking
     * @param bool               $isAvailablePaymentAccess
     * @param bool               $isAvailablePOSTerminal
     * @param bool               $isAvailableInternationalShipping
     * @param bool               $isAvailableSelfServiceWorkplaces
     * @param bool               $isWarehouseForAgent
     * @param bool               $isWorkInMobileAwis
     */
    public function __construct(
        string $ref,
        string $siteKey,
        string $number,
        string $warehouseTypeRef,
        string $cityRef,
        string $cityDescriptionUk,
        string $cityDescriptionRu,
        string $settlementRef,
        string $settlementDescriptionUk,
        string $settlementAreaDescriptionUk,
        string $settlementRegionDescriptionUk,
        string $settlementTypeDescriptionUk,
        string $settlementTypeDescriptionRu,
        string $descriptionUk,
        string $descriptionRu,
        string $shortAddressUk,
        string $shortAddressRu,
        string $phone,
        string $latitude,
        string $longitude,
        string $totalMaxWeightAllowed,
        string $placeMaxWeightAllowed,
        ShipmentDimensions $sendingLimitationsOnDimensions,
        ShipmentDimensions $receivingLimitationsOnDimensions,
        Schedule $receptionSchedule,
        Schedule $deliverySchedule,
        Schedule $schedule,
        string $districtCode,
        string $warehouseCategory,
        string $warehouseStatus,
        DateTimeInterface $warehouseStatusDate,
        ?string $regionCityUk,
        string $maxDeclaredCost,
        bool $isAvailablePostFinance,
        bool $isAvailableBicycleParking,
        bool $isAvailablePaymentAccess,
        bool $isAvailablePOSTerminal,
        bool $isAvailableInternationalShipping,
        bool $isAvailableSelfServiceWorkplaces,
        bool $isWarehouseForAgent,
        bool $isWorkInMobileAwis
    ) {
        $this->ref = $ref;
        $this->siteKey = $siteKey;
        $this->number = $number;
        $this->warehouseTypeRef = $warehouseTypeRef;
        $this->cityRef = $cityRef;
        $this->cityDescriptionUk = $cityDescriptionUk;
        $this->cityDescriptionRu = $cityDescriptionRu;
        $this->settlementRef = $settlementRef;
        $this->settlementDescriptionUk = $settlementDescriptionUk;
        $this->settlementAreaDescriptionUk = $settlementAreaDescriptionUk;
        $this->settlementRegionDescriptionUk = $settlementRegionDescriptionUk;
        $this->settlementTypeDescriptionUk = $settlementTypeDescriptionUk;
        $this->settlementTypeDescriptionRu = $settlementTypeDescriptionRu;
        $this->descriptionUk = $descriptionUk;
        $this->descriptionRu = $descriptionRu;
        $this->shortAddressUk = $shortAddressUk;
        $this->shortAddressRu = $shortAddressRu;
        $this->phone = $phone;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->totalMaxWeightAllowed = $totalMaxWeightAllowed;
        $this->placeMaxWeightAllowed = $placeMaxWeightAllowed;
        $this->sendingLimitationsOnDimensions = $sendingLimitationsOnDimensions;
        $this->receivingLimitationsOnDimensions = $receivingLimitationsOnDimensions;
        $this->receptionSchedule = $receptionSchedule;
        $this->deliverySchedule = $deliverySchedule;
        $this->schedule = $schedule;
        $this->districtCode = $districtCode;
        $this->warehouseCategory = $warehouseCategory;
        $this->warehouseStatus = $warehouseStatus;
        $this->warehouseStatusDate = $warehouseStatusDate;
        $this->regionCityUk = $regionCityUk;
        $this->maxDeclaredCost = $maxDeclaredCost;
        $this->isAvailablePostFinance = $isAvailablePostFinance;
        $this->isAvailableBicycleParking = $isAvailableBicycleParking;
        $this->isAvailablePaymentAccess = $isAvailablePaymentAccess;
        $this->isAvailablePOSTerminal = $isAvailablePOSTerminal;
        $this->isAvailableInternationalShipping = $isAvailableInternationalShipping;
        $this->isAvailableSelfServiceWorkplaces = $isAvailableSelfServiceWorkplaces;
        $this->isWarehouseForAgent = $isWarehouseForAgent;
        $this->isWorkInMobileAwis = $isWorkInMobileAwis;
    }

    public function getRef(): string
    {
        return $this->ref;
    }

    /**
     * @return numeric-string
     */
    public function getSiteKey(): string
    {
        return $this->siteKey;
    }

    /**
     * @return numeric-string
     */
    public function getNumber(): string
    {
        return $this->number;
    }

    public function getWarehouseTypeRef(): string
    {
        return $this->warehouseTypeRef;
    }

    public function getCityRef(): string
    {
        return $this->cityRef;
    }

    public function getCityDescriptionUk(): string
    {
        return $this->cityDescriptionUk;
    }

    public function getCityDescriptionRu(): string
    {
        return $this->cityDescriptionRu;
    }

    public function getSettlementRef(): string
    {
        return $this->settlementRef;
    }

    public function getSettlementDescriptionUk(): string
    {
        return $this->settlementDescriptionUk;
    }

    public function getSettlementAreaDescriptionUk(): string
    {
        return $this->settlementAreaDescriptionUk;
    }

    public function getSettlementRegionDescriptionUk(): string
    {
        return $this->settlementRegionDescriptionUk;
    }

    public function getSettlementTypeDescriptionUk(): string
    {
        return $this->settlementTypeDescriptionUk;
    }

    public function getSettlementTypeDescriptionRu(): string
    {
        return $this->settlementTypeDescriptionRu;
    }

    public function getDescriptionUk(): string
    {
        return $this->descriptionUk;
    }

    public function getDescriptionRu(): string
    {
        return $this->descriptionRu;
    }

    public function getShortAddressUk(): string
    {
        return $this->shortAddressUk;
    }

    public function getShortAddressRu(): string
    {
        return $this->shortAddressRu;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function getLatitude(): string
    {
        return $this->latitude;
    }

    public function getLongitude(): string
    {
        return $this->longitude;
    }

    /**
     * @return numeric-string
     */
    public function getTotalMaxWeightAllowed(): string
    {
        return $this->totalMaxWeightAllowed;
    }

    /**
     * @return numeric-string
     */
    public function getPlaceMaxWeightAllowed(): string
    {
        return $this->placeMaxWeightAllowed;
    }

    public function getSendingLimitationsOnDimensions(): ShipmentDimensions
    {
        return $this->sendingLimitationsOnDimensions;
    }

    public function getReceivingLimitationsOnDimensions(): ShipmentDimensions
    {
        return $this->receivingLimitationsOnDimensions;
    }

    public function getReceptionSchedule(): Schedule
    {
        return $this->receptionSchedule;
    }

    public function getDeliverySchedule(): Schedule
    {
        return $this->deliverySchedule;
    }

    public function getSchedule(): Schedule
    {
        return $this->schedule;
    }

    public function getDistrictCode(): string
    {
        return $this->districtCode;
    }

    public function getWarehouseCategory(): string
    {
        return $this->warehouseCategory;
    }

    public function getWarehouseStatus(): string
    {
        return $this->warehouseStatus;
    }

    public function getWarehouseStatusDate(): DateTimeInterface
    {
        return $this->warehouseStatusDate;
    }

    public function getRegionCityUk(): ?string
    {
        return $this->regionCityUk;
    }

    /**
     * @return numeric-string
     */
    public function getMaxDeclaredCost(): string
    {
        return $this->maxDeclaredCost;
    }

    /**
     * @return bool
     */
    public function isAvailablePostFinance(): bool
    {
        return $this->isAvailablePostFinance;
    }

    /**
     * @return bool
     */
    public function isAvailableBicycleParking(): bool
    {
        return $this->isAvailableBicycleParking;
    }

    /**
     * @return bool
     */
    public function isAvailablePaymentAccess(): bool
    {
        return $this->isAvailablePaymentAccess;
    }

    /**
     * @return bool
     */
    public function isAvailablePOSTerminal(): bool
    {
        return $this->isAvailablePOSTerminal;
    }

    /**
     * @return bool
     */
    public function isAvailableInternationalShipping(): bool
    {
        return $this->isAvailableInternationalShipping;
    }

    /**
     * @return bool
     */
    public function isAvailableSelfServiceWorkplaces(): bool
    {
        return $this->isAvailableSelfServiceWorkplaces;
    }

    /**
     * @return bool
     */
    public function isWarehouseForAgent(): bool
    {
        return $this->isWarehouseForAgent;
    }

    /**
     * @return bool
     */
    public function isWorkInMobileAwis(): bool
    {
        return $this->isWorkInMobileAwis;
    }
}
