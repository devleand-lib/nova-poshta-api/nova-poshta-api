<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Entity;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

final class Settlement
{
    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("Ref")
     * @Serializer\Type("string")
     */
    private string $ref;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("SettlementType")
     * @Serializer\Type("string")
     */
    private string $settlementTypeRef;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("SettlementTypeDescription")
     * @Serializer\Type("string")
     */
    private string $settlementTypeDescriptionUk;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("SettlementTypeDescriptionRu")
     * @Serializer\Type("string")
     */
    private string $settlementTypeDescriptionRu;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("Latitude")
     * @Serializer\Type("string")
     */
    private string $latitude;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("Longitude")
     * @Serializer\Type("string")
     */
    private string $longitude;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("Description")
     * @Serializer\Type("string")
     */
    private string $descriptionUk;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("DescriptionRu")
     * @Serializer\Type("string")
     */
    private string $descriptionRu;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("Region")
     * @Serializer\Type("string")
     */
    private string $regionRef;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("RegionsDescription")
     * @Serializer\Type("string")
     */
    private string $regionDescriptionUk;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("RegionsDescriptionRu")
     * @Serializer\Type("string")
     */
    private string $regionDescriptionRu;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("Area")
     * @Serializer\Type("string")
     */
    private string $areaRef;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("AreaDescription")
     * @Serializer\Type("string")
     */
    private string $areaDescriptionUk;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("AreaDescriptionRu")
     * @Serializer\Type("string")
     */
    private string $areaDescriptionRu;

    /**
     * @var numeric-string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("Index1")
     * @Serializer\Type("string")
     */
    private string $index1;

    /**
     * @var numeric-string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("Index2")
     * @Serializer\Type("string")
     */
    private string $index2;

    /**
     * Index of COATSU;
     *
     * @var numeric-string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("IndexCOATSU1")
     * @Serializer\Type("string")
     */
    private string $indexCOATSU1;

    /**
     * @var bool
     *
     * @Assert\NotBlank()
     * @Assert\Type("boolean")
     *
     * @Serializer\SerializedName("Delivery1")
     * @Serializer\Type("boolean")
     */
    private bool $delivery1;

    /**
     * @var bool
     *
     * @Assert\NotBlank()
     * @Assert\Type("boolean")
     *
     * @Serializer\SerializedName("Delivery2")
     * @Serializer\Type("boolean")
     */
    private bool $delivery2;

    /**
     * @var bool
     *
     * @Assert\NotBlank()
     * @Assert\Type("boolean")
     *
     * @Serializer\SerializedName("Delivery3")
     * @Serializer\Type("boolean")
     */
    private bool $delivery3;

    /**
     * @var bool
     *
     * @Assert\NotBlank()
     * @Assert\Type("boolean")
     *
     * @Serializer\SerializedName("Delivery4")
     * @Serializer\Type("boolean")
     */
    private bool $delivery4;

    /**
     * @var bool
     *
     * @Assert\NotBlank()
     * @Assert\Type("boolean")
     *
     * @Serializer\SerializedName("Delivery5")
     * @Serializer\Type("boolean")
     */
    private bool $delivery5;

    /**
     * @var bool
     *
     * @Assert\NotBlank()
     * @Assert\Type("boolean")
     *
     * @Serializer\SerializedName("Delivery6")
     * @Serializer\Type("boolean")
     */
    private bool $delivery6;

    /**
     * @var bool
     *
     * @Assert\NotBlank()
     * @Assert\Type("boolean")
     *
     * @Serializer\SerializedName("Delivery7")
     * @Serializer\Type("boolean")
     */
    private bool $delivery7;

    /**
     * @var bool
     *
     * @Assert\NotBlank()
     * @Assert\Type("boolean")
     *
     * @Serializer\SerializedName("Warehouse")
     * @Serializer\Type("boolean")
     */
    private bool $isAvailableWarehouses;

    /**
     * @var int
     *
     * @Assert\NotBlank()
     * @Assert\Type("integer")
     *
     * @Serializer\SerializedName("SpecialCashCheck")
     * @Serializer\Type("integer")
     */
    private int $specialCashCheck;

    /**
     * @param string         $ref
     * @param string         $settlementTypeRef
     * @param string         $settlementTypeDescriptionUk
     * @param string         $settlementTypeDescriptionRu
     * @param string         $latitude
     * @param string         $longitude
     * @param string         $descriptionUk
     * @param string         $descriptionRu
     * @param string         $regionRef
     * @param string         $regionDescriptionUk
     * @param string         $regionDescriptionRu
     * @param string         $areaRef
     * @param string         $areaDescriptionUk
     * @param string         $areaDescriptionRu
     * @param numeric-string $index1
     * @param numeric-string $index2
     * @param numeric-string $indexCOATSU1
     * @param bool           $delivery1
     * @param bool           $delivery2
     * @param bool           $delivery3
     * @param bool           $delivery4
     * @param bool           $delivery5
     * @param bool           $delivery6
     * @param bool           $delivery7
     * @param bool           $isAvailableWarehouses
     * @param int            $specialCashCheck
     */
    public function __construct(
        string $ref,
        string $settlementTypeRef,
        string $settlementTypeDescriptionUk,
        string $settlementTypeDescriptionRu,
        string $latitude,
        string $longitude,
        string $descriptionUk,
        string $descriptionRu,
        string $regionRef,
        string $regionDescriptionUk,
        string $regionDescriptionRu,
        string $areaRef,
        string $areaDescriptionUk,
        string $areaDescriptionRu,
        string $index1,
        string $index2,
        string $indexCOATSU1,
        bool $delivery1,
        bool $delivery2,
        bool $delivery3,
        bool $delivery4,
        bool $delivery5,
        bool $delivery6,
        bool $delivery7,
        bool $isAvailableWarehouses,
        int $specialCashCheck
    ) {
        $this->ref = $ref;
        $this->settlementTypeRef = $settlementTypeRef;
        $this->settlementTypeDescriptionUk = $settlementTypeDescriptionUk;
        $this->settlementTypeDescriptionRu = $settlementTypeDescriptionRu;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->descriptionUk = $descriptionUk;
        $this->descriptionRu = $descriptionRu;
        $this->regionRef = $regionRef;
        $this->regionDescriptionUk = $regionDescriptionUk;
        $this->regionDescriptionRu = $regionDescriptionRu;
        $this->areaRef = $areaRef;
        $this->areaDescriptionUk = $areaDescriptionUk;
        $this->areaDescriptionRu = $areaDescriptionRu;
        $this->index1 = $index1;
        $this->index2 = $index2;
        $this->indexCOATSU1 = $indexCOATSU1;
        $this->delivery1 = $delivery1;
        $this->delivery2 = $delivery2;
        $this->delivery3 = $delivery3;
        $this->delivery4 = $delivery4;
        $this->delivery5 = $delivery5;
        $this->delivery6 = $delivery6;
        $this->delivery7 = $delivery7;
        $this->isAvailableWarehouses = $isAvailableWarehouses;
        $this->specialCashCheck = $specialCashCheck;
    }


    public function getRef(): string
    {
        return $this->ref;
    }

    public function getSettlementTypeRef(): string
    {
        return $this->settlementTypeRef;
    }

    public function getSettlementTypeDescriptionUk(): string
    {
        return $this->settlementTypeDescriptionUk;
    }

    public function getSettlementTypeDescriptionRu(): string
    {
        return $this->settlementTypeDescriptionRu;
    }

    public function getLatitude(): string
    {
        return $this->latitude;
    }

    public function getLongitude(): string
    {
        return $this->longitude;
    }

    public function getDescriptionUk(): string
    {
        return $this->descriptionUk;
    }

    public function getDescriptionRu(): string
    {
        return $this->descriptionRu;
    }

    public function getRegionRef(): string
    {
        return $this->regionRef;
    }

    public function getRegionDescriptionUk(): string
    {
        return $this->regionDescriptionUk;
    }

    public function getRegionDescriptionRu(): string
    {
        return $this->regionDescriptionRu;
    }

    public function getAreaRef(): string
    {
        return $this->areaRef;
    }

    public function getAreaDescriptionUk(): string
    {
        return $this->areaDescriptionUk;
    }

    public function getAreaDescriptionRu(): string
    {
        return $this->areaDescriptionRu;
    }

    /**
     * @return numeric-string
     */
    public function getIndex1(): string
    {
        return $this->index1;
    }

    /**
     * @return numeric-string
     */
    public function getIndex2(): string
    {
        return $this->index2;
    }

    /**
     * @return numeric-string
     */
    public function getIndexCOATSU1(): string
    {
        return $this->indexCOATSU1;
    }

    /**
     * @return bool
     */
    public function isDelivery1(): bool
    {
        return $this->delivery1;
    }

    /**
     * @return bool
     */
    public function isDelivery2(): bool
    {
        return $this->delivery2;
    }

    /**
     * @return bool
     */
    public function isDelivery3(): bool
    {
        return $this->delivery3;
    }

    /**
     * @return bool
     */
    public function isDelivery4(): bool
    {
        return $this->delivery4;
    }

    /**
     * @return bool
     */
    public function isDelivery5(): bool
    {
        return $this->delivery5;
    }

    /**
     * @return bool
     */
    public function isDelivery6(): bool
    {
        return $this->delivery6;
    }

    /**
     * @return bool
     */
    public function isDelivery7(): bool
    {
        return $this->delivery7;
    }

    public function isAvailableWarehouses(): bool
    {
        return $this->isAvailableWarehouses;
    }

    public function getSpecialCashCheck(): int
    {
        return $this->specialCashCheck;
    }
}
