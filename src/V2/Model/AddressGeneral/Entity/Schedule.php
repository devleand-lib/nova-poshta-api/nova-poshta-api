<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Entity;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

final class Schedule
{
    /**
     * @var ScheduleRange|null
     *
     * @Assert\Type("Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Entity\ScheduleRange")
     *
     * @Serializer\Accessor(setter="setMonday")
     * @Serializer\SerializedName("Monday")
     * @Serializer\Type("string")
     */
    private ?ScheduleRange $monday;

    /**
     * @var ScheduleRange|null
     *
     * @Assert\Type("Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Entity\ScheduleRange")
     *
     * @Serializer\Accessor(setter="setTuesday")
     * @Serializer\SerializedName("Tuesday")
     * @Serializer\Type("string")
     */
    private ?ScheduleRange $tuesday;

    /**
     * @var ScheduleRange|null
     *
     * @Assert\Type("Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Entity\ScheduleRange")
     *
     * @Serializer\Accessor(setter="setWednesday")
     * @Serializer\SerializedName("Wednesday")
     * @Serializer\Type("string")
     */
    private ?ScheduleRange $wednesday;

    /**
     * @var ScheduleRange|null
     *
     * @Assert\Type("Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Entity\ScheduleRange")
     *
     * @Serializer\Accessor(setter="setThursday")
     * @Serializer\SerializedName("Thursday")
     * @Serializer\Type("string")
     */
    private ?ScheduleRange $thursday;

    /**
     * @var ScheduleRange|null
     *
     * @Assert\Type("Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Entity\ScheduleRange")
     *
     * @Serializer\Accessor(setter="setFriday")
     * @Serializer\SerializedName("Friday")
     * @Serializer\Type("string")
     */
    private ?ScheduleRange $friday;

    /**
     * @var ScheduleRange|null
     *
     * @Assert\Type("Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Entity\ScheduleRange")
     *
     * @Serializer\Accessor(setter="setSaturday")
     * @Serializer\SerializedName("Saturday")
     * @Serializer\Type("string")
     */
    private ?ScheduleRange $saturday;

    /**
     * @var ScheduleRange|null
     *
     * @Assert\Type("Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Entity\ScheduleRange")
     *
     * @Serializer\Accessor(setter="setSunday")
     * @Serializer\SerializedName("Sunday")
     * @Serializer\Type("string")
     */
    private ?ScheduleRange $sunday;

    public function __construct(
        ?ScheduleRange $monday,
        ?ScheduleRange $tuesday,
        ?ScheduleRange $wednesday,
        ?ScheduleRange $thursday,
        ?ScheduleRange $friday,
        ?ScheduleRange $saturday,
        ?ScheduleRange $sunday
    ) {
        $this->monday = $monday;
        $this->tuesday = $tuesday;
        $this->wednesday = $wednesday;
        $this->thursday = $thursday;
        $this->friday = $friday;
        $this->saturday = $saturday;
        $this->sunday = $sunday;
    }

    /**
     * @return \Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Entity\ScheduleRange|null
     */
    public function getMonday(): ?ScheduleRange
    {
        return $this->monday;
    }

    /**
     * @param string $monday
     */
    public function setMonday(string $monday): void
    {
        $this->monday = ScheduleRange::parseOrNull($monday);
    }

    /**
     * @return \Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Entity\ScheduleRange|null
     */
    public function getTuesday(): ?ScheduleRange
    {
        return $this->tuesday;
    }

    /**
     * @param string $tuesday
     */
    public function setTuesday(string $tuesday): void
    {
        $this->tuesday = ScheduleRange::parseOrNull($tuesday);
    }

    /**
     * @return \Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Entity\ScheduleRange|null
     */
    public function getWednesday(): ?ScheduleRange
    {
        return $this->wednesday;
    }

    /**
     * @param string $wednesday
     */
    public function setWednesday(string $wednesday): void
    {
        $this->wednesday = ScheduleRange::parseOrNull($wednesday);
    }

    /**
     * @return \Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Entity\ScheduleRange|null
     */
    public function getThursday(): ?ScheduleRange
    {
        return $this->thursday;
    }

    /**
     * @param string $thursday
     */
    public function setThursday(string $thursday): void
    {
        $this->thursday = ScheduleRange::parseOrNull($thursday);
    }

    /**
     * @return \Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Entity\ScheduleRange|null
     */
    public function getFriday(): ?ScheduleRange
    {
        return $this->friday;
    }

    /**
     * @param string $friday
     */
    public function setFriday(string $friday): void
    {
        $this->friday = ScheduleRange::parseOrNull($friday);
    }

    /**
     * @return \Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Entity\ScheduleRange|null
     */
    public function getSaturday(): ?ScheduleRange
    {
        return $this->saturday;
    }

    /**
     * @param string $saturday
     */
    public function setSaturday(string $saturday): void
    {
        $this->saturday = ScheduleRange::parseOrNull($saturday);
    }

    /**
     * @return \Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Entity\ScheduleRange|null
     */
    public function getSunday(): ?ScheduleRange
    {
        return $this->sunday;
    }

    /**
     * @param string $sunday
     */
    public function setSunday(string $sunday): void
    {
        $this->sunday = ScheduleRange::parseOrNull($sunday);
    }
}
