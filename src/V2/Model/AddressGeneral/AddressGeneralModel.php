<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\AddressGeneral;

use Devleand\NovaPoshta\Api\V2\Contracts\Model\ApiMethodProperties;
use Devleand\NovaPoshta\Api\V2\Contracts\Response\ApiResponseInterface;
use Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Dto\GetSettlementsMethod\FindSettlementsPropertiesDto;
use Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Dto\GetWarehousesMethod\FindWarehousesPropertiesDto;
use Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Entity\Settlement;
use Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Entity\Warehouse;
use Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Entity\WarehouseType;
use Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Enum\AddressGeneralMethod;
use Devleand\NovaPoshta\Api\V2\Model\Enum\ApiModel;
use Devleand\NovaPoshta\Api\V2\Model\Model;

class AddressGeneralModel extends Model
{
    public function getApiModel(): ApiModel
    {
        return ApiModel::ADDRESS_GENERAL();
    }

    public function isApiMethodSupported(string $apiMethod): bool
    {
        return AddressGeneralMethod::isValid($apiMethod);
    }

    /**
     * @param ApiMethodProperties[]|null $propertiesList
     *
     * @return ApiResponseInterface<Settlement>
     *
     * @see https://devcenter.novaposhta.ua/docs/services/556d7ccaa0fe4f08e8f7ce43/operations/56248fffa0fe4f0da0550ea8
     */
    public function getSettlements(array $propertiesList = null): ApiResponseInterface
    {
        return $this->sendWithTransform(
            AddressGeneralMethod::GET_SETTLEMENTS,
            Settlement::class,
            $propertiesList
        );
    }

    /**
     * @param string $ref
     *
     * @return ApiResponseInterface<Settlement>
     *
     * @see https://devcenter.novaposhta.ua/docs/services/556d7ccaa0fe4f08e8f7ce43/operations/56248fffa0fe4f0da0550ea8
     */
    public function findSettlementByRef(string $ref): ApiResponseInterface
    {
        return $this->getSettlements([new FindSettlementsPropertiesDto($ref)]);
    }

    /**
     * @param string $settlementStringUk
     *
     * @return ApiResponseInterface<Settlement>
     *
     * @see https://devcenter.novaposhta.ua/docs/services/556d7ccaa0fe4f08e8f7ce43/operations/56248fffa0fe4f0da0550ea8
     */
    public function findSettlementByString(string $settlementStringUk): ApiResponseInterface
    {
        return $this->getSettlements([new FindSettlementsPropertiesDto(null, $settlementStringUk)]);
    }

    /**
     * @return ApiResponseInterface<WarehouseType>
     *
     * @see https://devcenter.novaposhta.ua/docs/services/556d7ccaa0fe4f08e8f7ce43/operations/556d8211a0fe4f08e8f7ce45
     */
    public function getWarehouseTypes(): ApiResponseInterface
    {
        return $this->sendWithTransform(
            AddressGeneralMethod::GET_WAREHOUSE_TYPES,
            WarehouseType::class
        );
    }

    /**
     * @param ApiMethodProperties[]|null $propertiesList
     *
     * @return ApiResponseInterface<Warehouse>
     *
     * @see https://devcenter.novaposhta.ua/docs/services/556d7ccaa0fe4f08e8f7ce43/operations/556d8211a0fe4f08e8f7ce45
     */
    public function getWarehouses(array $propertiesList = null): ApiResponseInterface
    {
        return $this->sendWithTransform(
            AddressGeneralMethod::GET_WAREHOUSES,
            Warehouse::class,
            $propertiesList
        );
    }

    /**
     * @param string $ref
     *
     * @return ApiResponseInterface<Warehouse>
     *
     * @see https://devcenter.novaposhta.ua/docs/services/556d7ccaa0fe4f08e8f7ce43/operations/556d8211a0fe4f08e8f7ce45
     */
    public function findWarehouseByRef(string $ref): ApiResponseInterface
    {
        return $this->getWarehouses([new FindWarehousesPropertiesDto($ref)]);
    }

    /**
     * @param string $warehouseString
     *
     * @return ApiResponseInterface<Warehouse>
     *
     * @see https://devcenter.novaposhta.ua/docs/services/556d7ccaa0fe4f08e8f7ce43/operations/556d8211a0fe4f08e8f7ce45
     */
    public function findWarehouseByString(string $warehouseString): ApiResponseInterface
    {
        return $this->getWarehouses([new FindWarehousesPropertiesDto(null, $warehouseString)]);
    }

    /**
     * @param string $settlementRef
     *
     * @return ApiResponseInterface<Warehouse>
     *
     * @see https://devcenter.novaposhta.ua/docs/services/556d7ccaa0fe4f08e8f7ce43/operations/556d8211a0fe4f08e8f7ce45
     */
    public function findWarehousesBySettlement(string $settlementRef): ApiResponseInterface
    {
        return $this->getWarehouses([new FindWarehousesPropertiesDto(null, null, $settlementRef)]);
    }
}
