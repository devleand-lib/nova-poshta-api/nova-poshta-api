<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Dto\GetWarehousesMethod;

use JMS\Serializer\Annotation as Serializer;
use Devleand\NovaPoshta\Api\V2\Contracts\Model\ApiMethodProperties;
use Devleand\NovaPoshta\Api\V2\Support\Model\ConvertToSendMethodPropertiesTrait;
use Devleand\NovaPoshta\Api\V2\Support\Model\FilterMethodPropertiesTrait;

class GetWarehousesFiltersDto implements ApiMethodProperties
{
    use FilterMethodPropertiesTrait;
    use ConvertToSendMethodPropertiesTrait;

    /**
     * @var string|null
     *
     * @Serializer\SkipWhenEmpty()
     * @Serializer\SerializedName("TypeOfWarehouseRef")
     * @Serializer\Type("string")
     */
    protected ?string $warehouseTypeRef;

    /**
     * @var string|null
     *
     * @Serializer\SkipWhenEmpty()
     * @Serializer\SerializedName("CityRef")
     * @Serializer\Type("string")
     */
    protected ?string $cityRef;

    /**
     * @var string|null
     *
     * @Serializer\SkipWhenEmpty()
     * @Serializer\SerializedName("CityName")
     * @Serializer\Type("string")
     */
    protected ?string $cityDescriptionUk;

    /**
     * @var bool|null
     *
     * @Serializer\SkipWhenEmpty()
     * @Serializer\SerializedName("PostFinance")
     * @Serializer\Type("string")
     */
    protected ?bool $isAvailablePostFinance;

    /**
     * @var bool|null
     *
     * @Serializer\SkipWhenEmpty()
     * @Serializer\SerializedName("BicycleParking")
     * @Serializer\Type("string")
     */
    protected ?bool $isAvailableBicycleParking;

    public function __construct(
        ?string $warehouseTypeRef = null,
        ?string $cityRef = null,
        ?string $cityDescriptionUk = null,
        ?bool $isAvailablePostFinance = null,
        ?bool $isAvailableBicycleParking = null
    ) {
        $this->warehouseTypeRef = $warehouseTypeRef;
        $this->cityRef = $cityRef;
        $this->cityDescriptionUk = $cityDescriptionUk;
        $this->isAvailablePostFinance = $isAvailablePostFinance;
        $this->isAvailableBicycleParking = $isAvailableBicycleParking;
    }

    public function getWarehouseTypeRef(): ?string
    {
        return $this->warehouseTypeRef;
    }

    public function getCityRef(): ?string
    {
        return $this->cityRef;
    }

    public function getCityDescriptionUk(): ?string
    {
        return $this->cityDescriptionUk;
    }

    public function isAvailablePostFinance(): ?bool
    {
        return $this->isAvailablePostFinance;
    }

    public function isAvailableBicycleParking(): ?bool
    {
        return $this->isAvailableBicycleParking;
    }
}
