<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Dto\GetWarehousesMethod;

use JMS\Serializer\Annotation as Serializer;
use Devleand\NovaPoshta\Api\V2\Contracts\Model\ApiMethodProperties;

class FindWarehousesPropertiesDto implements ApiMethodProperties
{
    /**
     * @var string|null
     *
     * @Serializer\SkipWhenEmpty()
     * @Serializer\SerializedName("Ref")
     * @Serializer\Type("string")
     */
    protected ?string $ref;

    /**
     * @var string|null
     *
     * @Serializer\SkipWhenEmpty()
     * @Serializer\SerializedName("FindByString")
     * @Serializer\Type("string")
     */
    protected ?string $byString;

    /**
     * @var string|null
     *
     * @Serializer\SkipWhenEmpty()
     * @Serializer\SerializedName("SettlementRef")
     * @Serializer\Type("string")
     */
    protected ?string $settlementRef = null;

    public function __construct(?string $ref = null, ?string $byString = null, ?string $settlementRef = null)
    {
        $this->ref = $ref;
        $this->byString = $byString;
        $this->settlementRef = $settlementRef;
    }

    public function getRef(): ?string
    {
        return $this->ref;
    }

    public function getByString(): ?string
    {
        return $this->byString;
    }

    public function getSettlementRef(): ?string
    {
        return $this->settlementRef;
    }
}
