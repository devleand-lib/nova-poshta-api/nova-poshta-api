<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Dto\GetSettlementsMethod;

use JMS\Serializer\Annotation as Serializer;
use Devleand\NovaPoshta\Api\V2\Contracts\Model\ApiMethodProperties;

class FindSettlementsPropertiesDto implements ApiMethodProperties
{
    /**
     * @var string|null
     *
     * @Serializer\SkipWhenEmpty()
     * @Serializer\SerializedName("Ref")
     * @Serializer\Type("string")
     */
    protected ?string $ref;

    /**
     * @var string|null
     *
     * @Serializer\SkipWhenEmpty()
     * @Serializer\SerializedName("FindByString")
     * @Serializer\Type("string")
     */
    protected ?string $byStringUk;

    public function __construct(?string $ref = null, ?string $byStringUk = null)
    {
        $this->ref = $ref;
        $this->byStringUk = $byStringUk;
    }

    public function getRef(): ?string
    {
        return $this->ref;
    }

    public function getByStringUk(): ?string
    {
        return $this->byStringUk;
    }
}
