<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Dto\GetSettlementsMethod;

use JMS\Serializer\Annotation as Serializer;
use Devleand\NovaPoshta\Api\V2\Contracts\Model\ApiMethodProperties;

class GetSettlementsFiltersDto implements ApiMethodProperties
{
    /**
     * @var string|null
     *
     * @Serializer\SkipWhenEmpty()
     * @Serializer\SerializedName("RegionRef")
     * @Serializer\Type("string")
     */
    protected ?string $regionRef;

    /**
     * @var bool|null
     *
     * @Serializer\SkipWhenEmpty()
     * @Serializer\SerializedName("Warehouse")
     * @Serializer\Type("string")
     */
    protected ?bool $isAvailableWarehouses;

    public function __construct(?string $regionRef = null, ?bool $isAvailableWarehouses = null)
    {
        $this->regionRef = $regionRef;
        $this->isAvailableWarehouses = $isAvailableWarehouses;
    }

    /**
     * @return string|null
     */
    public function getRegionRef(): ?string
    {
        return $this->regionRef;
    }

    /**
     * @return bool|null
     */
    public function isAvailableWarehouses(): ?bool
    {
        return $this->isAvailableWarehouses;
    }
}
