<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\Address\Entity;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

final class Area
{
    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("Ref")
     * @Serializer\Type("string")
     */
    private string $ref;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("AreasCenter")
     * @Serializer\Type("string")
     */
    private string $areaCenterRef;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("DescriptionRu")
     * @Serializer\Type("string")
     */
    private string $descriptionRu;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Serializer\SerializedName("Description")
     * @Serializer\Type("string")
     */
    private string $descriptionUk;

    public function __construct(
        string $ref,
        string $areaCenterRef,
        string $descriptionRu,
        string $descriptionUk
    ) {
        $this->ref = $ref;
        $this->areaCenterRef = $areaCenterRef;
        $this->descriptionRu = $descriptionRu;
        $this->descriptionUk = $descriptionUk;
    }

    public function getRef(): string
    {
        return $this->ref;
    }

    public function getAreaCenterRef(): string
    {
        return $this->areaCenterRef;
    }

    public function getDescriptionRu(): string
    {
        return $this->descriptionRu;
    }

    public function getDescriptionUk(): string
    {
        return $this->descriptionUk;
    }
}
