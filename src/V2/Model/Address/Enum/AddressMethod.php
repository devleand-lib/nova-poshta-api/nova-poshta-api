<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\Address\Enum;

use MyCLabs\Enum\Enum;

/**
 * @extends Enum<string>
 *
 * @method static AddressMethod GET_AREAS()
 */
final class AddressMethod extends Enum
{
    public const GET_AREAS = 'getAreas';
}
