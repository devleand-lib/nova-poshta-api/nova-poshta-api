<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\Address;

use Devleand\NovaPoshta\Api\V2\Contracts\Response\ApiResponseInterface;
use Devleand\NovaPoshta\Api\V2\Model\Address\Entity\Area;
use Devleand\NovaPoshta\Api\V2\Model\Address\Enum\AddressMethod;
use Devleand\NovaPoshta\Api\V2\Model\Enum\ApiModel;
use Devleand\NovaPoshta\Api\V2\Model\Model;

class AddressModel extends Model
{
    public function getApiModel(): ApiModel
    {
        return ApiModel::ADDRESS();
    }

    public function isApiMethodSupported(string $apiMethod): bool
    {
        return AddressMethod::isValid($apiMethod);
    }

    /**
     * @return ApiResponseInterface<Area>
     *
     * @see https://devcenter.novaposhta.ua/docs/services/556d7ccaa0fe4f08e8f7ce43/operations/556d9130a0fe4f08e8f7ce48
     */
    public function getAreas(): ApiResponseInterface
    {
        return $this->sendWithTransform(AddressMethod::GET_AREAS, Area::class);
    }
}
