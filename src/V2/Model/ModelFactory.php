<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model;

use Devleand\NovaPoshta\Api\V2\Contracts\Model\ModelFactoryInterface;
use Devleand\NovaPoshta\Api\V2\Contracts\Model\ModelInterface;
use Devleand\NovaPoshta\Api\V2\Contracts\NovaPoshtaApiClientFactoryInterface;
use Devleand\NovaPoshta\Api\V2\Contracts\Transformer\DataToEntityTransformer;
use Devleand\NovaPoshta\Api\V2\Enum\Language;
use InvalidArgumentException;
use JMS\Serializer\ArrayTransformerInterface;
use Psr\Log\LoggerInterface;

class ModelFactory implements ModelFactoryInterface
{
    /**
     * @var NovaPoshtaApiClientFactoryInterface
     */
    private NovaPoshtaApiClientFactoryInterface $novaPoshtaApiClientFactory;

    /**
     * @var DataToEntityTransformer
     */
    private DataToEntityTransformer $dataToEntityTransformer;

    /**
     * @var ArrayTransformerInterface
     */
    private ArrayTransformerInterface $arrayTransformer;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    public function __construct(
        NovaPoshtaApiClientFactoryInterface $novaPoshtaApiClientFactory,
        DataToEntityTransformer $dataToEntityTransformer,
        ArrayTransformerInterface $arrayTransformer,
        LoggerInterface $logger
    ) {
        $this->novaPoshtaApiClientFactory = $novaPoshtaApiClientFactory;
        $this->dataToEntityTransformer = $dataToEntityTransformer;
        $this->arrayTransformer = $arrayTransformer;
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     */
    public function make(string $modelClass, string $key, ?Language $language = null): ModelInterface
    {
        if (!is_subclass_of($modelClass, ModelInterface::class)) {
            throw new InvalidArgumentException(
                sprintf("Class %s must implement %s.", $modelClass, ModelInterface::class)
            );
        }

        return new $modelClass(
            $this->novaPoshtaApiClientFactory->make($key, $language),
            $this->dataToEntityTransformer,
            $this->arrayTransformer,
            $this->logger
        );
    }
}
