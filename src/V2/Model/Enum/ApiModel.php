<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Model\Enum;

use MyCLabs\Enum\Enum;

/**
 * @extends Enum<string>
 *
 * @method static ApiModel ADDRESS()
 * @method static ApiModel ADDRESS_GENERAL()
 * @method static ApiModel COMMON()
 * @method static ApiModel COMMON_GENERAL()
 * @method static ApiModel CONTACT_PERSON()
 * @method static ApiModel INTERNET_DOCUMENT()
 * @method static ApiModel TRACKING_DOCUMENT()
 */
class ApiModel extends Enum
{
    public const ADDRESS = 'Address';
    public const ADDRESS_GENERAL = 'AddressGeneral';

    public const COMMON = 'Common';
    public const COMMON_GENERAL = 'CommonGeneral';

    public const CONTACT_PERSON = 'ContactPerson';
    public const INTERNET_DOCUMENT = 'InternetDocument';
    public const TRACKING_DOCUMENT = 'TrackingDocument';
}
