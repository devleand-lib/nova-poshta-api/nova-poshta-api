<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Contracts;

use Devleand\NovaPoshta\Api\V2\Contracts\Exception\BadApiResponseException;
use Devleand\NovaPoshta\Api\V2\Contracts\Model\ModelInterface;
use Devleand\NovaPoshta\Api\V2\Contracts\Response\ApiResponseInterface;
use Devleand\NovaPoshta\Api\V2\Enum\Request\HttpMethod;
use Devleand\NovaPoshta\Api\V2\Exception\Request\UnsupportedApiMethodException;
use Http\Client\Exception\RequestException;
use Http\Client\Exception\HttpException;
use Devleand\NovaPoshta\Api\Exception\Request\CreateRequestException;

interface ApiClientInterface
{
    /**
     * @param HttpMethod                $httpMethod
     * @param ModelInterface            $model
     * @param string                    $apiMethod
     * @param array<string, mixed>|null $data
     *
     * @return ApiResponseInterface<array<string, mixed>>
     *
     * @throws UnsupportedApiMethodException
     * @throws CreateRequestException
     * @throws RequestException
     * @throws HttpException
     * @throws BadApiResponseException
     */
    public function send(
        HttpMethod $httpMethod,
        ModelInterface $model,
        string $apiMethod,
        array $data = null
    ): ApiResponseInterface;
}
