<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Contracts;

use Devleand\NovaPoshta\Api\V2\Enum\Language;

interface NovaPoshtaApiClientFactoryInterface
{
    public function make(string $key, ?Language $language = null): ApiClientInterface;
}
