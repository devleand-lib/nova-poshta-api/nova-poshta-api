<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Contracts\Exception;

use RuntimeException;
use Throwable;

/**
 * @template DataRaw
 * @template Entity
 */
class DataToEntityTransformException extends RuntimeException
{
    /**
     * @var DataRaw
     */
    private $data;

    /**
     * @var class-string<Entity>
     */
    private string $entityClass;

    /**
     * @param DataRaw              $data
     * @param class-string<Entity> $entityClass
     * @param string               $message
     * @param int                  $code
     * @param \Throwable|null      $previous
     */
    public function __construct(
        $data,
        string $entityClass,
        $message = "Failed to transform raw data to target entity.",
        $code = 0,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);

        $this->data = $data;
        $this->entityClass = $entityClass;
    }

    /**
     * @return DataRaw
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return class-string<Entity>
     */
    public function getEntityClass(): string
    {
        return $this->entityClass;
    }
}
