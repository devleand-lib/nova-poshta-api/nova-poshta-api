<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Contracts\Exception;

use Devleand\NovaPoshta\Api\V2\Contracts\Response\ApiResponseInterface;
use RuntimeException;
use Throwable;

/**
 * @template TData
 */
class BadApiResponseException extends RuntimeException
{
    /**
     * @var ApiResponseInterface<TData>
     */
    private ApiResponseInterface $apiResponse;

    /**
     * @param ApiResponseInterface<TData> $apiResponse
     * @param string                      $message
     * @param int                         $code
     * @param \Throwable|null             $previous
     */
    public function __construct(
        ApiResponseInterface $apiResponse,
        $message = "",
        $code = 0,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);

        $this->apiResponse = $apiResponse;
    }

    /**
     * @return ApiResponseInterface<TData>
     */
    public function getApiResponse(): ApiResponseInterface
    {
        return $this->apiResponse;
    }
}
