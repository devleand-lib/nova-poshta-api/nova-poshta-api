<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Contracts\Exception;

use Devleand\NovaPoshta\Api\Support\Types;
use Throwable;
use UnexpectedValueException;

/**
 * @template TExpectedEntity
 */
class UnexpectedEntityException extends UnexpectedValueException
{
    /**
     * @var mixed
     */
    private $actualEntity;

    /**
     * @var class-string<TExpectedEntity>
     */
    private string $expectedEntity;

    /**
     * @param mixed                         $actualEntity
     * @param class-string<TExpectedEntity> $expectedEntity
     * @param string|null                   $message
     * @param int                           $code
     * @param \Throwable|null               $previous
     */
    public function __construct(
        $actualEntity,
        string $expectedEntity,
        $message = null,
        $code = 0,
        Throwable $previous = null
    ) {
        $defaultMessage = sprintf(
            "Expected %s entity, received %s",
            $expectedEntity,
            Types::getType($actualEntity)
        );
        parent::__construct(
            $message ?? $defaultMessage,
            $code,
            $previous
        );

        $this->actualEntity = $actualEntity;
        $this->expectedEntity = $expectedEntity;
    }

    /**
     * @return mixed
     */
    public function getActualEntity()
    {
        return $this->actualEntity;
    }

    /**
     * @return class-string<TExpectedEntity>
     */
    public function getExpectedEntity(): string
    {
        return $this->expectedEntity;
    }
}
