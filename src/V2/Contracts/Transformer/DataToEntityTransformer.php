<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Contracts\Transformer;

use Devleand\NovaPoshta\Api\V2\Contracts\Exception\DataToEntityTransformException;
use Devleand\NovaPoshta\Api\V2\Contracts\Exception\UnexpectedEntityException;

interface DataToEntityTransformer
{
    /**
     * @template TEntity
     *
     * @param iterable<array<string, mixed>> $data
     * @param class-string<TEntity>          $entityClass
     *
     * @return iterable<TEntity>
     *
     * @throws DataToEntityTransformException
     * @throws UnexpectedEntityException
     */
    public function transform(iterable $data, string $entityClass): iterable;
}
