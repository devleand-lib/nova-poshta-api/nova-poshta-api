<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Contracts\Response;

/**
 * @template T
 */
interface ApiResponseInterface
{
    public function isSuccess(): bool;

    /**
     * @return iterable<T>
     */
    public function getData(): iterable;

    /**
     * @template TData
     *
     * @param iterable<TData> $data
     *
     * @return static<TData>
     */
    public function withData(iterable $data): self;

    /**
     * @return string[]
     */
    public function getMessageCodes(): array;

    /**
     * @return string[]
     */
    public function getErrorCodes(): array;

    /**
     * @return string[]
     */
    public function getWarningCodes(): array;

    /**
     * @return string[]
     */
    public function getInfoCodes(): array;

    /**
     * @return string[]
     */
    public function getErrors(): array;

    /**
     * @return string[]
     */
    public function getWarnings(): array;

    /**
     * @return array<string, mixed>
     */
    public function getInfo(): array;
}
