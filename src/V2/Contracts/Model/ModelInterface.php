<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Contracts\Model;

use Devleand\NovaPoshta\Api\V2\Model\Enum\ApiModel;

interface ModelInterface
{
    public function getApiModel(): ApiModel;
    public function isApiMethodSupported(string $apiMethod): bool;
}
