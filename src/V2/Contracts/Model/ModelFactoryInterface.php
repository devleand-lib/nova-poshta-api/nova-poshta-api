<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Contracts\Model;

use Devleand\NovaPoshta\Api\V2\Enum\Language;

interface ModelFactoryInterface
{
    /**
     * @param class-string  $modelClass
     * @param string        $key
     * @param Language|null $language
     *
     * @return ModelInterface
     */
    public function make(string $modelClass, string $key, ?Language $language = null): ModelInterface;
}
