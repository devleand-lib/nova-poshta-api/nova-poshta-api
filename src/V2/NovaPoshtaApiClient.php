<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2;

use Devleand\NovaPoshta\Api\Logger\LogValidationErrorsTrait;
use Http\Client\Exception\RequestException;
use JMS\Serializer\SerializerInterface;
use Devleand\NovaPoshta\Api\Exception\Request\CreateRequestException;
use Devleand\NovaPoshta\Api\Exception\Response\InvalidBodyException;
use Devleand\NovaPoshta\Api\Exception\Response\UnexpectedStatusException;
use Devleand\NovaPoshta\Api\V2\Contracts\ApiClientInterface;
use Devleand\NovaPoshta\Api\V2\Contracts\Model\ModelInterface;
use Devleand\NovaPoshta\Api\V2\Contracts\Response\ApiResponseInterface;
use Devleand\NovaPoshta\Api\V2\Dto\Response\ResponseDto;
use Devleand\NovaPoshta\Api\V2\Dto\SettingsDto;
use Devleand\NovaPoshta\Api\V2\Enum\Request\HttpMethod;
use Devleand\NovaPoshta\Api\V2\Exception\NotSuccessApiResponseException;
use Devleand\NovaPoshta\Api\V2\Exception\Request\UnsupportedApiMethodException;
use Devleand\NovaPoshta\Api\Logger\ExceptionLoggerTrait;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Throwable;
use Http\Client\Exception\HttpException;

class NovaPoshtaApiClient implements ApiClientInterface
{
    use ExceptionLoggerTrait;
    use LogValidationErrorsTrait;

    public const SUCCESS_STATUS_CODES = [200];

    /**
     * @var SettingsDto
     */
    protected SettingsDto $settings;

    /**
     * @var ClientInterface
     */
    protected ClientInterface $client;

    /**
     * @var RequestFactoryInterface
     */
    protected RequestFactoryInterface $requestFactory;

    /**
     * @var StreamFactoryInterface
     */
    protected StreamFactoryInterface $streamFactory;

    /**
     * @var SerializerInterface
     */
    protected SerializerInterface $serializer;

    /**
     * @var ValidatorInterface
     */
    protected ValidatorInterface $validator;

    /**
     * @var LoggerInterface
     */
    protected LoggerInterface $logger;

    public function __construct(
        SettingsDto $settings,
        ClientInterface $client,
        RequestFactoryInterface $requestFactory,
        StreamFactoryInterface $streamFactory,
        SerializerInterface $serializer,
        ValidatorInterface $validator,
        LoggerInterface $logger
    ) {
        $this->settings = $settings;
        $this->client = $client;
        $this->requestFactory = $requestFactory;
        $this->streamFactory = $streamFactory;
        $this->serializer = $serializer;
        $this->validator = $validator;
        $this->logger = $logger;
    }

    public function setSettings(SettingsDto $settings): self
    {
        $this->settings = $settings;

        return $this;
    }

    public function getSettings(): SettingsDto
    {
        return $this->settings;
    }

    /**
     * @inheritDoc
     */
    public function send(
        HttpMethod $httpMethod,
        ModelInterface $model,
        string $apiMethod,
        array $data = null
    ): ApiResponseInterface {
        if (! $model->isApiMethodSupported($apiMethod)) {
            throw new UnsupportedApiMethodException($model, $apiMethod);
        }

        $this->logger->info('Creating request for sending to Nova Poshta API.');
        $params = [
            'apiKey' => $this->settings->getKey(),
            'modelName' => $model->getApiModel()->getValue(),
            'calledMethod' => $apiMethod,
            'language' => $this->settings->getLanguage()->getValue(),
            'methodProperties' => $data,
        ];
        $request = $this->createRequest($httpMethod, $params);

        try {
            $this->logger->info('Sending request to Nova Poshta API.');
            $response = $this->client->sendRequest($request);
        } catch (Throwable $exception) {
            $this->logExceptionWithTrace($exception);

            throw new RequestException('Failed to send request to Nova Poshta API.', $request);
        }

        if (! in_array($response->getStatusCode(), self::SUCCESS_STATUS_CODES)) {
            throw new UnexpectedStatusException($request, $response);
        }

        $apiResponse = $this->getApiResponse($response, $request);
        if ($apiResponse->isSuccess() === false) {
            throw new NotSuccessApiResponseException($apiResponse);
        }

        return $apiResponse;
    }

    /**
     * @param ResponseInterface $response
     * @param RequestInterface  $request
     *
     * @return ApiResponseInterface<array<string, mixed>>
     *
     * @throws HttpException
     */
    protected function getApiResponse(ResponseInterface $response, RequestInterface $request): ApiResponseInterface
    {
        $body = $response->getBody();
        if ($body->isSeekable()) {
            $body->seek(0);
        }

        $contents = $body->getContents();
        if ($contents === '') {
            throw new InvalidBodyException($request, $response, 'Nova Poshta sent empty response body.');
        }

        try {
            /** @var ResponseDto<array<string, mixed>> $apiResponseDto */
            $apiResponseDto = $this->serializer->deserialize(
                $contents,
                ResponseDto::class,
                'json'
            );

            $errors = $this->validator->validate($apiResponseDto);
            if (count($errors) > 0) {
                $this->logValidationErrors($errors);

                throw new ValidationFailedException($apiResponseDto, $errors);
            }
        } catch (Throwable $exception) {
            $this->logExceptionWithTrace($exception);

            throw new InvalidBodyException($request, $response);
        }

        return $apiResponseDto;
    }

    /**
     * @param HttpMethod           $httpMethod
     * @param array<string, mixed> $params
     *
     * @return RequestInterface
     *
     * @throws CreateRequestException
     */
    protected function createRequest(HttpMethod $httpMethod, array $params): RequestInterface
    {
        $headers = [
            'Content-Type' => ['application/json']
        ];

        try {
            $body = $this->serializer->serialize($params, 'json');
        } catch (Throwable $exception) {
            $this->logExceptionWithTrace($exception);

            throw new CreateRequestException(
                'Failed to serialize request body.',
                $httpMethod->getValue(),
                $this->settings->getBaseUrl(),
                $params,
                $headers
            );
        }

        $request = $this->requestFactory
            ->createRequest(
                $httpMethod->getValue(),
                $this->settings->getBaseUrl()
            )
            ->withBody(
                $this->streamFactory->createStream($body)
            );

        foreach ($headers as $headerName => $headerValue) {
            $request = $request->withHeader($headerName, $headerValue);
        }

        return $request;
    }
}
