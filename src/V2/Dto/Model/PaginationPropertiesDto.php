<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Dto\Model;

use Devleand\NovaPoshta\Api\V2\Contracts\Model\ApiMethodProperties;

class PaginationPropertiesDto implements ApiMethodProperties
{
    use PaginationPropertiesTrait;

    /**
     * @param int $page
     * @param int $limit
     */
    public function __construct(int $page, int $limit)
    {
        $this->setPage($page);
        $this->setLimit($limit);
    }
}
