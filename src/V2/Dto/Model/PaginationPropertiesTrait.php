<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Dto\Model;

use InvalidArgumentException;
use JMS\Serializer\Annotation as Serializer;

trait PaginationPropertiesTrait
{
    /**
     * @var positive-int
     *
     * @Serializer\SerializedName("Page")
     * @Serializer\Type("string")
     */
    protected int $page;

    /**
     * @var positive-int
     *
     * @Serializer\SerializedName("Limit")
     * @Serializer\Type("string")
     */
    protected int $limit;

    /**
     * @return positive-int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    public function setPage(int $page): self
    {
        if ($page < 1) {
            throw new InvalidArgumentException(
                sprintf('Page must be positive and not equal to zero, received %d.', $page)
            );
        }
        $this->page = $page;

        return $this;
    }

    /**
     * @return positive-int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    public function setLimit(int $limit): self
    {
        if ($limit < 1) {
            throw new InvalidArgumentException(
                sprintf('Limit must be positive and not equal to zero, received %d.', $limit)
            );
        }
        $this->limit = $limit;

        return $this;
    }
}
