<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Dto\Response;

use Devleand\NovaPoshta\Api\Contracts\Arrayable;
use JMS\Serializer\Annotation as Serializer;
use Devleand\NovaPoshta\Api\V2\Contracts\Response\ApiResponseInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @template T
 * @implements ApiResponseInterface<T>
 */
final class ResponseDto implements ApiResponseInterface, Arrayable
{
    /**
     * @Assert\NotBlank(message="The 'success' field of the response must not be empty.")
     * @Assert\Type(type="boolean", message="The 'success' field of the response must be of type 'boolean'.")
     *
     * @Serializer\SerializedName("success")
     * @Serializer\Type("boolean")
     */
    private bool $isSuccess;

    /**
     * @var iterable<T>
     *
     * @Assert\NotBlank(message="The 'data' field of the response must not be empty.")
     * @Assert\Type(type="iterable", message="The 'data' field of the response must be of type 'iterable'.")
     *
     * @Serializer\Type("array")
     */
    private iterable $data;

    /**
     * @var string[]
     *
     * @Assert\NotBlank(message="The 'messageCodes' field of the response must not be empty.")
     * @Assert\Type(type="array", message="The 'messageCodes' field of the response must be of type 'array'.")
     *
     * @Serializer\SerializedName("messageCodes")
     * @Serializer\Type("array<string>")
     */
    private array $messageCodes;

    /**
     * @var string[]
     *
     * @Assert\NotBlank(message="The 'errorCodes' field of the response must not be empty.")
     * @Assert\Type(type="array", message="The 'errorCodes' field of the response must be of type 'array'.")
     *
     * @Serializer\SerializedName("errorCodes")
     * @Serializer\Type("array<string>")
     */
    private array $errorCodes;

    /**
     * @var string[]
     *
     * @Assert\NotBlank(message="The 'warningCodes' field of the response must not be empty.")
     * @Assert\Type(type="array", message="The 'warningCodes' field of the response must be of type 'array'.")
     *
     * @Serializer\SerializedName("warningCodes")
     * @Serializer\Type("array<string>")
     */
    private array $warningCodes;

    /**
     * @var string[]
     *
     * @Assert\NotBlank(message="The 'infoCodes' field of the response must not be empty.")
     * @Assert\Type(type="array", message="The 'infoCodes' field of the response must be of type 'array'.")
     *
     * @Serializer\SerializedName("infoCodes")
     * @Serializer\Type("array<string>")
     */
    private array $infoCodes;

    /**
     * @var string[]
     *
     * @Assert\NotBlank(message="The 'errors' field of the response must not be empty.")
     * @Assert\Type(type="array", message="The 'errors' field of the response must be of type 'array'.")
     *
     * @Serializer\Type("array<string>")
     */
    private array $errors;

    /**
     * @var string[]
     *
     * @Assert\NotBlank(message="The 'warnings' field of the response must not be empty.")
     * @Assert\Type(type="array", message="The 'warnings' field of the response must be of type 'array'.")
     *
     * @Serializer\Type("array<string>")
     */
    private array $warnings;

    /**
     * @var array<string, mixed>
     *
     * @Assert\NotBlank(message="The 'info' field of the response must not be empty.")
     * @Assert\Type(type="array", message="The 'info' field of the response must be of type 'array'.")
     *
     * @Serializer\Type("array")
     */
    private array $info;

    /**
     * @param bool                 $isSuccess
     * @param iterable<T>          $data
     * @param string[]             $messageCodes
     * @param string[]             $errorCodes
     * @param string[]             $warningCodes
     * @param string[]             $infoCodes
     * @param string[]             $errors
     * @param string[]             $warnings
     * @param array<string, mixed> $info
     */
    public function __construct(
        bool $isSuccess,
        iterable $data,
        array $messageCodes,
        array $errorCodes,
        array $warningCodes,
        array $infoCodes,
        array $errors,
        array $warnings,
        array $info
    ) {
        $this->isSuccess = $isSuccess;
        $this->data = $data;
        $this->messageCodes = $messageCodes;
        $this->errorCodes = $errorCodes;
        $this->warningCodes = $warningCodes;
        $this->infoCodes = $infoCodes;
        $this->errors = $errors;
        $this->warnings = $warnings;
        $this->info = $info;
    }

    public function isSuccess(): bool
    {
        return $this->isSuccess;
    }

    public function getData(): iterable
    {
        return $this->data;
    }

    /**
     * @template TData
     *
     * @param iterable<TData> $data
     *
     * @return self<TData>
     */
    public function withData(iterable $data): self
    {
        return new self(
            $this->isSuccess,
            $data,
            $this->messageCodes,
            $this->errorCodes,
            $this->warningCodes,
            $this->infoCodes,
            $this->errors,
            $this->warnings,
            $this->info
        );
    }

    public function getMessageCodes(): array
    {
        return $this->messageCodes;
    }

    public function getErrorCodes(): array
    {
        return $this->errorCodes;
    }

    public function getWarningCodes(): array
    {
        return $this->warningCodes;
    }

    public function getInfoCodes(): array
    {
        return $this->infoCodes;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    public function getWarnings(): array
    {
        return $this->warnings;
    }

    public function getInfo(): array
    {
        return $this->info;
    }

    public function toArray(): array
    {
        return [
            'isSuccess' => $this->isSuccess,
            'data' => is_array($this->data) ? $this->data : iterator_to_array($this->data),
            'messageCodes' => $this->messageCodes,
            'errorCodes' => $this->errorCodes,
            'warningCodes' => $this->warningCodes,
            'infoCodes' => $this->infoCodes,
            'errors' => $this->errors,
            'warnings' => $this->warnings,
            'info' => $this->info
        ];
    }
}
