<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Dto;

use Devleand\NovaPoshta\Api\V2\Enum\Language;

final class SettingsDto
{
    /**
     * @var string
     */
    private string $baseUrl;

    /**
     * Key for API NovaPoshta.
     *
     * @var string
     *
     * @see https://new.novaposhta.ua/dashboard/settings/developers
     */
    private string $key;

    /**
     * Language of response.
     *
     * @var Language
     */
    private Language $language;

    public function __construct(string $baseUrl, string $key, Language $language)
    {
        $this->baseUrl = $baseUrl;
        $this->key = $key;
        $this->language = $language;
    }

    public function getBaseUrl(): string
    {
        return $this->baseUrl;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function getLanguage(): Language
    {
        return $this->language;
    }
}
