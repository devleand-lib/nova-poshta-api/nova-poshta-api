<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Support\Model;

trait FilterMethodPropertiesTrait
{
    /**
     * @param array<string, mixed> $properties
     *
     * @return array<string, mixed>
     */
    protected function filterOnlyNotNull(array $properties): array
    {
        return array_filter(
            $properties,
            fn($value) => !is_null($value)
        );
    }
}
