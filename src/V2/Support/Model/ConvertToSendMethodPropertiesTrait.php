<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Support\Model;

trait ConvertToSendMethodPropertiesTrait
{
    protected function convertBoolToSend(?bool $value): ?string
    {
        if ($value === null) {
            return null;
        }

        return $value ? "1" : "0";
    }
}
