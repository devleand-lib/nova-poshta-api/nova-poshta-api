<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Support;

class TypeCaster
{
    public const TRUE_STRING = '1';
    public const FALSE_STRING = '0';

    public static function boolToString(bool $value): string
    {
        return $value ? self::TRUE_STRING : self::FALSE_STRING;
    }

    public static function stringToBool(string $value): bool
    {
        if ($value !== self::TRUE_STRING && $value !== self::FALSE_STRING && !empty($value)) {
            throw new \InvalidArgumentException('Value must be "1" or "0"');
        }

        return $value === self::TRUE_STRING;
    }
}
