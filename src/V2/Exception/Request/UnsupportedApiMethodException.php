<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Exception\Request;

use Devleand\NovaPoshta\Api\V2\Contracts\Model\ModelInterface;
use LogicException;
use Throwable;

class UnsupportedApiMethodException extends LogicException
{
    /**
     * @var ModelInterface
     */
    protected ModelInterface $model;

    /**
     * @var string
     */
    protected string $apiMethod;

    /**
     * @param ModelInterface $model
     * @param string         $apiMethod
     * @param string|null    $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct(
        ModelInterface $model,
        string $apiMethod,
        $message = null,
        $code = 0,
        Throwable $previous = null
    ) {
        $this->model = $model;
        $this->apiMethod = $apiMethod;

        parent::__construct(
            $message ?? sprintf("%s model does not support %s method", get_class($model), $apiMethod),
            $code,
            $previous
        );
    }

    public function getModel(): ModelInterface
    {
        return $this->model;
    }

    public function getApiMethod(): string
    {
        return $this->apiMethod;
    }
}
