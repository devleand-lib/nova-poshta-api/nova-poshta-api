<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Exception;

use RuntimeException;
use Throwable;

/**
 * @template TEntity
 */
class EntityValidationFailedException extends RuntimeException
{
    /**
     * @var TEntity
     */
    private $entity;

    /**
     * @param TEntity         $entity
     * @param string          $message
     * @param int             $code
     * @param \Throwable|null $previous
     */
    public function __construct(
        $entity,
        $message = 'Entity validation failed.',
        $code = 0,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);

        $this->entity = $entity;
    }

    /**
     * @return TEntity
     */
    public function getEntity()
    {
        return $this->entity;
    }
}
