<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\V2\Exception;

use Devleand\NovaPoshta\Api\V2\Contracts\Exception\BadApiResponseException;
use Devleand\NovaPoshta\Api\V2\Contracts\Response\ApiResponseInterface;
use Throwable;

/**
 * @template TData
 *
 * @extends BadApiResponseException<TData>
 */
class NotSuccessApiResponseException extends BadApiResponseException
{
    public function __construct(
        ApiResponseInterface $apiResponse,
        $message = 'Nova Poshta sent a response with an unsuccessful status (`success`: false).',
        $code = 0,
        Throwable $previous = null
    ) {
        parent::__construct($apiResponse, $message, $code, $previous);
    }
}
