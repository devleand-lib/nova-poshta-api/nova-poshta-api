<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\Validator;

use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\GroupSequence;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class LogValidationDecorator implements ValidatorInterface
{
    /**
     * @var ValidatorInterface
     */
    private ValidatorInterface $validator;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    public function __construct(ValidatorInterface $validator, LoggerInterface $logger)
    {
        $this->validator = $validator;
        $this->logger = $logger;
    }

    public function getMetadataFor($value)
    {
        return $this->validator->getMetadataFor(...func_get_args());
    }

    public function hasMetadataFor($value)
    {
        return $this->validator->hasMetadataFor(...func_get_args());
    }

    /**
     * @param mixed                                                 $value
     * @param Constraint|Constraint[]|null                          $constraints
     * @param string|GroupSequence|array<string|GroupSequence>|null $groups
     *
     * @return ConstraintViolationListInterface<int, ConstraintViolationInterface>
     */
    public function validate($value, $constraints = null, $groups = null)
    {
        $this->logger->info('Validating started.', [
            'value' => $value,
            'constraints' => $constraints,
            'groups' => $groups
        ]);

        $errors = $this->validator->validate(...func_get_args());
        $this->logger->info('Validating completed.', [
            'errors' => iterator_to_array($errors)
        ]);

        return $errors;
    }

    /**
     * @param object                                                $object
     * @param string                                                $propertyName
     * @param string|GroupSequence|array<string|GroupSequence>|null $groups
     *
     * @return ConstraintViolationListInterface<int, ConstraintViolationInterface>
     */
    public function validateProperty(object $object, string $propertyName, $groups = null)
    {
        return $this->validator->validateProperty(...func_get_args());
    }

    /**
     * @param object|string                                         $objectOrClass
     * @param string                                                $propertyName
     * @param mixed                                                 $value
     * @param string|GroupSequence|array<string|GroupSequence>|null $groups
     *
     * @return ConstraintViolationListInterface<int, ConstraintViolationInterface>
     */
    public function validatePropertyValue($objectOrClass, string $propertyName, $value, $groups = null)
    {
        return $this->validator->validatePropertyValue(...func_get_args());
    }

    public function startContext()
    {
        return $this->validator->startContext();
    }

    public function inContext(ExecutionContextInterface $context)
    {
        return $this->validator->inContext(...func_get_args());
    }
}
