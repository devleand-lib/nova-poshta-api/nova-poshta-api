<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\Logger;

use Psr\Http\Message\ResponseInterface;

trait ResponseLoggerTrait
{
    /**
     * @param ResponseInterface   $response
     * @param array<mixed, mixed> $context
     */
    protected function logResponse(ResponseInterface $response, array $context = []): void
    {
        $this->logger->info(sprintf('RESPONSE: %s', $this->convertResponseToString($response)), $context);
    }

    private function convertResponseToString(ResponseInterface $response): string
    {
        $responseAsArray = [
            'statusCode' => $response->getStatusCode(),
            'status' => $response->getReasonPhrase(),
            'headers' => $response->getHeaders(),
            'body' => (string) $response->getBody()
        ];

        return json_encode($responseAsArray) ?: (string) var_export($responseAsArray, true);
    }
}
