<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\Logger;

use Psr\Http\Message\RequestInterface;

trait RequestLoggerTrait
{
    /**
     * @param RequestInterface    $request
     * @param array<mixed, mixed> $context
     */
    protected function logRequest(RequestInterface $request, array $context = []): void
    {
        $this->logger->info(sprintf('REQUEST: %s', $this->convertRequestToString($request)), $context);
    }

    private function convertRequestToString(RequestInterface $request): string
    {
        $requestAsArray = [
            'uri' => $request->getUri(),
            'method' => $request->getMethod(),
            'headers' => $request->getHeaders(),
            'body' => (string) $request->getBody()
        ];

        return json_encode($requestAsArray) ?: (string) var_export($requestAsArray, true);
    }
}
