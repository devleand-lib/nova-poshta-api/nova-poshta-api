<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\Logger;

use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

trait LogValidationErrorsTrait
{
    /**
     * @param ConstraintViolationListInterface<int, ConstraintViolationInterface> $constraintViolationList
     */
    protected function logValidationErrors(ConstraintViolationListInterface $constraintViolationList): void
    {
        /** @var ConstraintViolationInterface $constraintViolation */
        foreach ($constraintViolationList as $constraintViolation) {
            $this->logger->info(
                (string) $constraintViolation->getMessage(),
                $constraintViolation->getParameters()
            );
        }
    }
}
