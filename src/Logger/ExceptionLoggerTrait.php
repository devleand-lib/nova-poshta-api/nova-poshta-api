<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\Logger;

use Throwable;

trait ExceptionLoggerTrait
{
    /**
     * @param Throwable             $e
     * @param string                $initMessage
     * @param array<mixed, mixed>   $initMessageContext
     */
    protected function logExceptionWithTrace(
        Throwable $e,
        string $initMessage = '',
        array $initMessageContext = []
    ): void {
        try {
            $traceId = random_bytes(16);
        } catch (Throwable $exception) {
            $traceId = uniqid('', true);
        }

        if (! empty($initMessage)) {
            $initMessageContext['trace_id'] = $traceId;
            $this->logger->warning($initMessage, $initMessageContext);
        }

        $level = 0;
        do {
            $this->logger->warning(
                $e->getMessage(),
                [
                    'trace_id' => $traceId,
                    'level' => $level++,
                    'class' => get_class($e),
                    'file' => $e->getFile(),
                    'line' => $e->getLine(),
                ]
            );
            $e = $e->getPrevious();
        } while (null !== $e);
    }
}
