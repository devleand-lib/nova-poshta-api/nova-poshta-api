<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\Http;

use Devleand\NovaPoshta\Api\Logger\RequestLoggerTrait;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;

class LogRequestClientDecorator implements ClientInterface
{
    use RequestLoggerTrait;

    /**
     * @var ClientInterface
     */
    private ClientInterface $client;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    public function __construct(ClientInterface $client, LoggerInterface $logger)
    {
        $this->client = $client;
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     */
    public function sendRequest(RequestInterface $request): ResponseInterface
    {
        $this->logRequest($request);

        return $this->client->sendRequest($request);
    }
}
