<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\Support;

use Devleand\NovaPoshta\Api\Contracts\Arrayable;

class Types
{
    /**
     * @param mixed $value
     *
     * @return string
     */
    public static function getType($value): string
    {
        return is_object($value) ? get_class($value) : gettype($value);
    }

    /**
     * Convert value to array. Return null if converting failed.
     *
     * @param mixed $value
     *
     * @return array<mixed, mixed>|null
     */
    public static function toArray($value): ?array
    {
        if (is_array($value)) {
            return $value;
        }

        if (is_iterable($value)) {
            return iterator_to_array($value);
        }

        if ($value instanceof Arrayable) {
            return $value->toArray();
        }

        return null;
    }
}
