<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\Exception\Response;

use Exception;
use Http\Client\Exception\HttpException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class InvalidBodyException extends HttpException
{
    /**
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param string            $message
     * @param Exception|null    $previous
     */
    public function __construct(
        RequestInterface $request,
        ResponseInterface $response,
        $message = 'Nova Poshta sent an invalid response body.',
        Exception $previous = null
    ) {
        parent::__construct($message, $request, $response, $previous);
    }
}
