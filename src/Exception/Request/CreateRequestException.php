<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\Exception\Request;

use RuntimeException;
use Throwable;

class CreateRequestException extends RuntimeException
{
    /**
     * @var string|null
     */
    private ?string $method;

    /**
     * @var string|null
     */
    private ?string $url;

    /**
     * @var array<string, mixed>|null
     */
    private ?array $params;

    /**
     * @var array<string, string[]>|null
     */
    private ?array $headers;

    /**
     * @param string                       $message
     * @param string|null                  $method
     * @param string|null                  $url
     * @param array<string, mixed>|null    $params
     * @param array<string, string[]>|null $headers
     * @param int                          $code
     * @param \Throwable|null              $previous
     */
    public function __construct(
        $message = "",
        ?string $method = null,
        ?string $url = null,
        ?array $params = null,
        ?array $headers = null,
        $code = 0,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);

        $this->method = $method;
        $this->url = $url;
        $this->params = $params;
        $this->headers = $headers;
    }

    public function getMethod(): ?string
    {
        return $this->method;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @return array<string, mixed>|null
     */
    public function getParams(): ?array
    {
        return $this->params;
    }

    /**
     * @return array<string, string[]>|null
     */
    public function getHeaders(): ?array
    {
        return $this->headers;
    }
}
