<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\Serializer\Transformer;

use Devleand\NovaPoshta\Api\Support\Types;
use JMS\Serializer\ArrayTransformerInterface;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\SerializationContext;
use Psr\Log\LoggerInterface;

class LogToArrayTransformingDecorator implements ArrayTransformerInterface
{
    /**
     * @var ArrayTransformerInterface
     */
    private ArrayTransformerInterface $arrayTransformer;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    public function __construct(ArrayTransformerInterface $arrayTransformer, LoggerInterface $logger)
    {
        $this->arrayTransformer = $arrayTransformer;
        $this->logger = $logger;
    }

    /**
     * @param mixed                     $data
     * @param SerializationContext|null $context
     * @param string|null               $type
     *
     * @return array<mixed, mixed>
     */
    public function toArray($data, ?SerializationContext $context = null, ?string $type = null): array
    {
        $this->logger->info('Transforming data to array started.', [
            'dataType' => Types::getType($data),
            'data' => $data
        ]);

        $result = $this->arrayTransformer->toArray(...func_get_args());
        $this->logger->info('Transforming data to array completed.', ['result' => $result]);

        return $result;
    }

    /**
     * @param array<mixed, mixed>         $data
     * @param string                      $type
     * @param DeserializationContext|null $context
     *
     * @return mixed
     */
    public function fromArray(array $data, string $type, ?DeserializationContext $context = null)
    {
        return $this->arrayTransformer->fromArray(...func_get_args());
    }
}
