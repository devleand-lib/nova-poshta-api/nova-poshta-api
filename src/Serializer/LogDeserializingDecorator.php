<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\Serializer;

use Devleand\NovaPoshta\Api\Support\Types;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Psr\Log\LoggerInterface;

class LogDeserializingDecorator implements SerializerInterface
{
    /**
     * @var SerializerInterface
     */
    private SerializerInterface $serializer;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    public function __construct(SerializerInterface $serializer, LoggerInterface $logger)
    {
        $this->serializer = $serializer;
        $this->logger = $logger;
    }

    public function serialize(
        $data,
        string $format,
        ?SerializationContext $context = null,
        ?string $type = null
    ): string {
        return $this->serializer->serialize(...func_get_args());
    }

    public function deserialize(string $data, string $type, string $format, ?DeserializationContext $context = null)
    {
        $this->logger->info("Deserializing data started.", [
            'type' => $type,
            'format' => $format,
            'data' => $data
        ]);

        $result = $this->serializer->deserialize(...func_get_args());
        $this->logger->info('Deserializing data completed.', [
            'result' => $result,
            'resultType' => Types::getType($result)
        ]);

        return $result;
    }
}
