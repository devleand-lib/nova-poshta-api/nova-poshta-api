<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\Serializer;

use Devleand\NovaPoshta\Api\Support\Types;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Psr\Log\LoggerInterface;

class LogSerializingDecorator implements SerializerInterface
{
    /**
     * @var SerializerInterface
     */
    private SerializerInterface $serializer;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    public function __construct(SerializerInterface $serializer, LoggerInterface $logger)
    {
        $this->serializer = $serializer;
        $this->logger = $logger;
    }

    public function serialize(
        $data,
        string $format,
        ?SerializationContext $context = null,
        ?string $type = null
    ): string {
        $dataType = Types::getType($data);
        $this->logger->info('Serializing data started.', [
            'format' => $format,
            'dataType' => $dataType,
            'data' => $data
        ]);

        $result = $this->serializer->serialize(...func_get_args());
        $this->logger->info('Serializing data completed.', ['result' => $result]);

        return $result;
    }

    public function deserialize(string $data, string $type, string $format, ?DeserializationContext $context = null)
    {
        return $this->serializer->deserialize(...func_get_args());
    }
}
