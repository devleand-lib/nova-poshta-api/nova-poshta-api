<?php

declare(strict_types=1);

namespace Devleand\NovaPoshta\Api\Contracts;

interface Arrayable
{
    /**
     * @return array<string, mixed>
     */
    public function toArray(): array;
}
