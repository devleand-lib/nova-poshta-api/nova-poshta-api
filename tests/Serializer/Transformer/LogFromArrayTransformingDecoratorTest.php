<?php

declare(strict_types=1);

namespace Devleand\Tests\NovaPoshta\Api\Serializer\Transformer;

use Devleand\NovaPoshta\Api\Serializer\Transformer\LogFromArrayTransformingDecorator;
use JMS\Serializer\ArrayTransformerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class LogFromArrayTransformingDecoratorTest extends TestCase
{
    public function testFromArray(): void
    {
        $transformer = $this->createMock(ArrayTransformerInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $decorator = new LogFromArrayTransformingDecorator($transformer, $logger);

        $logger
            ->expects($this->atLeast(2))
            ->method('info');
        $transformer
            ->expects($this->once())
            ->method('fromArray')
            ->with(['12345'], 'SomeClass');

        $decorator->fromArray(['12345'], 'SomeClass');
    }

    public function testToArray(): void
    {
        $transformer = $this->createMock(ArrayTransformerInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $decorator = new LogFromArrayTransformingDecorator($transformer, $logger);

        $transformer
            ->expects($this->once())
            ->method('toArray')
            ->with('12345');

        $decorator->toArray('12345');
    }
}
