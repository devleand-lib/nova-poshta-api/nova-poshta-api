<?php

declare(strict_types=1);

namespace Devleand\Tests\NovaPoshta\Api\Serializer;

use Devleand\NovaPoshta\Api\Serializer\LogDeserializingDecorator;
use JMS\Serializer\SerializerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class LogDeserializingDecoratorTest extends TestCase
{
    public function testDeserialize(): void
    {
        $serializer = $this->createMock(SerializerInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $decorator = new LogDeserializingDecorator($serializer, $logger);

        $logger
            ->expects($this->atLeast(2))
            ->method('info');
        $serializer
            ->expects($this->once())
            ->method('deserialize')
            ->with('12345', 'TestClass', 'json');

        $decorator->deserialize('12345', 'TestClass', 'json');
    }

    public function testSerialize(): void
    {
        $serializer = $this->createMock(SerializerInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $decorator = new LogDeserializingDecorator($serializer, $logger);

        $serializer
            ->expects($this->once())
            ->method('serialize')
            ->with('12345', 'json');

        $decorator->serialize('12345', 'json');
    }
}
