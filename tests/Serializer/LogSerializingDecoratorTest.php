<?php

declare(strict_types=1);

namespace Devleand\Tests\NovaPoshta\Api\Serializer;

use Devleand\NovaPoshta\Api\Serializer\LogSerializingDecorator;
use JMS\Serializer\SerializerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class LogSerializingDecoratorTest extends TestCase
{
    public function testSerialize(): void
    {
        $serializer = $this->createMock(SerializerInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $decorator = new LogSerializingDecorator($serializer, $logger);

        $logger
            ->expects($this->atLeast(2))
            ->method('info');
        $serializer
            ->expects($this->once())
            ->method('serialize')
            ->with('12345', 'json');

        $decorator->serialize('12345', 'json');
    }

    public function testDeserialize(): void
    {
        $serializer = $this->createMock(SerializerInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $decorator = new LogSerializingDecorator($serializer, $logger);

        $serializer
            ->expects($this->once())
            ->method('deserialize')
            ->with('12345', 'SomeClass', 'json');

        $decorator->deserialize('12345', 'SomeClass', 'json');
    }
}
