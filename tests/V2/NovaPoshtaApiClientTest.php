<?php

declare(strict_types=1);

namespace Devleand\Tests\NovaPoshta\Api\V2;

use Devleand\NovaPoshta\Api\V2\Enum\Language;
use Devleand\NovaPoshta\Api\V2\Model\Enum\ApiModel;
use JMS\Serializer\SerializerInterface;
use Devleand\NovaPoshta\Api\Exception\Response\InvalidBodyException;
use Devleand\NovaPoshta\Api\Exception\Response\UnexpectedStatusException;
use Devleand\NovaPoshta\Api\V2\Contracts\Model\ModelInterface;
use Devleand\NovaPoshta\Api\V2\Contracts\Response\ApiResponseInterface;
use Devleand\NovaPoshta\Api\V2\Dto\SettingsDto;
use Devleand\NovaPoshta\Api\V2\Enum\Request\HttpMethod;
use Devleand\NovaPoshta\Api\V2\Exception\NotSuccessApiResponseException;
use Devleand\NovaPoshta\Api\V2\Exception\Request\UnsupportedApiMethodException;
use Devleand\NovaPoshta\Api\V2\NovaPoshtaApiClient;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\StreamInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Psr\Http\Client\ClientExceptionInterface;
use PHPUnit\Framework\MockObject\MockObject;

class NovaPoshtaApiClientTest extends TestCase
{
    /**
     * @var SettingsDto
     */
    private SettingsDto $settings;

    /**
     * @var ClientInterface|MockObject
     */
    private ClientInterface $client;

    /**
     * @var RequestFactoryInterface|MockObject
     */
    private RequestFactoryInterface $requestFactory;

    /**
     * @var StreamFactoryInterface|MockObject
     */
    private StreamFactoryInterface $streamFactory;

    /**
     * @var SerializerInterface|MockObject
     */
    private SerializerInterface $serializer;

    /**
     * @var ValidatorInterface|MockObject
     */
    private ValidatorInterface $validator;

    /**
     * @var LoggerInterface|MockObject
     */
    private LoggerInterface $logger;

    /**
     * @var ModelInterface|MockObject
     */
    private ModelInterface $model;

    protected function setUp(): void
    {
        $this->settings = new SettingsDto('', '', Language::RU());
        $this->client = $this->createMock(ClientInterface::class);
        $this->requestFactory = $this->createMock(RequestFactoryInterface::class);
        $this->streamFactory = $this->createMock(StreamFactoryInterface::class);
        $this->streamFactory
            ->method('createStream')
            ->willReturn($this->createMock(StreamInterface::class));
        $this->serializer = $this->createMock(SerializerInterface::class);
        $this->validator = $this->createMock(ValidatorInterface::class);
        $this->logger = $this->createMock(LoggerInterface::class);

        $this->model = $this->createMock(ModelInterface::class);
        $this->model
            ->method('getApiModel')
            ->willReturn(ApiModel::COMMON());
    }

    public function testSendWhenUnsupportedApiMethod(): void
    {
        $this->model
            ->method('isApiMethodSupported')
            ->willReturn(false);
        $apiClient = $this->makeNovaPoshtaApiClient();

        $this->expectExceptionObject(new UnsupportedApiMethodException($this->model, 'apiMethodTest'));

        $apiClient->send(HttpMethod::POST(), $this->model, 'apiMethodTest');
    }

    /**
     * @dataProvider provideSendWhenInvalidResponseStatusData
     *
     * @param int $statusCode
     *
     * @throws ClientExceptionInterface
     */
    public function testSendWhenInvalidResponseStatus(int $statusCode): void
    {
        $this->model
            ->method('isApiMethodSupported')
            ->willReturn(true);
        $request = $this->getRequestMock();
        $this->requestFactory
            ->method('createRequest')
            ->willReturn($request);
        $response = $this->getResponseMock();
        $response
            ->method('getStatusCode')
            ->willReturn($statusCode);
        $this->client
            ->method('sendRequest')
            ->willReturn($response);
        $apiClient = $this->makeNovaPoshtaApiClient();

        $this->expectExceptionObject(new UnexpectedStatusException($request, $response));
        $this->expectExceptionMessage('Nova Poshta sent an unexpected response status.');

        $apiClient->send(HttpMethod::POST(), $this->model, 'apiMethodTest');
    }

    public function testSendWhenEmptyResponseBody(): void
    {
        $this->model
            ->method('isApiMethodSupported')
            ->willReturn(true);
        $request = $this->getRequestMock();
        $this->requestFactory
            ->method('createRequest')
            ->willReturn($request);
        $responseBody = $this->createMock(StreamInterface::class);
        $responseBody
            ->method('getContents')
            ->willReturn('');
        $response = $this->getResponseMock();
        $response
            ->method('getStatusCode')
            ->willReturn(200);
        $response
            ->method('getBody')
            ->willReturn($responseBody);
        $this->client
            ->method('sendRequest')
            ->willReturn($response);
        $apiClient = $this->makeNovaPoshtaApiClient();

        $this->expectExceptionObject(new InvalidBodyException(
            $request,
            $response,
            'Nova Poshta sent empty response body.'
        ));

        $apiClient->send(HttpMethod::POST(), $this->model, 'apiMethodTest');
    }

    public function testSendWhenFailedDeserializeResponseBody(): void
    {
        $this->model
            ->method('isApiMethodSupported')
            ->willReturn(true);
        $request = $this->getRequestMock();
        $this->requestFactory
            ->method('createRequest')
            ->willReturn($request);
        $response = $this->getResponseMock();
        $response
            ->method('getStatusCode')
            ->willReturn(200);
        $response
            ->method('getBody')
            ->willReturn($this->getResponseBodyMock('body'));
        $this->client
            ->method('sendRequest')
            ->willReturn($response);
        $this->serializer
            ->method('deserialize')
            ->willThrowException(new \ErrorException());
        $apiClient = $this->makeNovaPoshtaApiClient();

        $this->expectExceptionObject(new InvalidBodyException($request, $response));
        $this->expectExceptionMessage('Nova Poshta sent an invalid response body.');

        $apiClient->send(HttpMethod::POST(), $this->model, 'apiMethodTest');
    }

    public function testSendWhenFailedValidateResponseBody(): void
    {
        $this->model
            ->method('isApiMethodSupported')
            ->willReturn(true);
        $request = $this->getRequestMock();
        $this->requestFactory
            ->method('createRequest')
            ->willReturn($request);
        $response = $this->getResponseMock();
        $response
            ->method('getStatusCode')
            ->willReturn(200);
        $response
            ->method('getBody')
            ->willReturn($this->getResponseBodyMock('body'));
        $this->client
            ->method('sendRequest')
            ->willReturn($response);
        $validationErrors = $this->createMock(ConstraintViolationListInterface::class);
        $validationErrors
            ->method('count')
            ->willReturn(1);
        $this->validator
            ->method('validate')
            ->willReturn($validationErrors);
        $apiClient = $this->makeNovaPoshtaApiClient();

        $this->expectExceptionObject(new InvalidBodyException($request, $response));
        $this->expectExceptionMessage('Nova Poshta sent an invalid response body.');

        $apiClient->send(HttpMethod::POST(), $this->model, 'apiMethodTest');
    }

    public function testSendWhenNotSuccessResponse(): void
    {
        $this->model
            ->method('isApiMethodSupported')
            ->willReturn(true);
        $request = $this->getRequestMock();
        $this->requestFactory
            ->method('createRequest')
            ->willReturn($request);
        $response = $this->getResponseMock();
        $response
            ->method('getStatusCode')
            ->willReturn(200);
        $response
            ->method('getBody')
            ->willReturn($this->getResponseBodyMock('1234'));
        $this->client
            ->method('sendRequest')
            ->willReturn($response);
        $validationErrors = $this->createMock(ConstraintViolationListInterface::class);
        $validationErrors
            ->method('count')
            ->willReturn(0);
        $this->validator
            ->method('validate')
            ->willReturn($validationErrors);
        $apiResponseMock = $this->getApiResponseMock();
        $apiResponseMock
            ->method('isSuccess')
            ->willReturn(false);
        $this->serializer
            ->method('deserialize')
            ->willReturn($apiResponseMock);
        $apiClient = $this->makeNovaPoshtaApiClient();

        $this->expectExceptionObject(new NotSuccessApiResponseException($apiResponseMock));
        $this->expectExceptionMessage('Nova Poshta sent a response with an unsuccessful status (`success`: false).');

        $apiResponse = $apiClient->send(HttpMethod::POST(), $this->model, 'apiMethodTest');

        $this->assertEquals($apiResponseMock, $apiResponse);
    }

    public function testSend(): void
    {
        $this->model
            ->method('isApiMethodSupported')
            ->willReturn(true);
        $request = $this->getRequestMock();
        $this->requestFactory
            ->method('createRequest')
            ->willReturn($request);
        $response = $this->getResponseMock();
        $response
            ->method('getStatusCode')
            ->willReturn(200);
        $response
            ->method('getBody')
            ->willReturn($this->getResponseBodyMock('21312'));
        $this->client
            ->method('sendRequest')
            ->willReturn($response);
        $validationErrors = $this->createMock(ConstraintViolationListInterface::class);
        $validationErrors
            ->method('count')
            ->willReturn(0);
        $this->validator
            ->method('validate')
            ->willReturn($validationErrors);
        $apiResponseMock = $this->getApiResponseMock();
        $apiResponseMock
            ->method('isSuccess')
            ->willReturn(true);
        $this->serializer
            ->method('deserialize')
            ->willReturn($apiResponseMock);
        $apiClient = $this->makeNovaPoshtaApiClient();

        $apiResponse = $apiClient->send(HttpMethod::POST(), $this->model, 'apiMethodTest');

        $this->assertEquals($apiResponseMock, $apiResponse);
    }

    public function provideSendWhenInvalidResponseStatusData(): array
    {
        return [
            'Response status code is equal 100' => [100],
            'Response status code is equal 404' => [404],
            'Response status code is equal 500' => [500],
            'Response status code is equal 303' => [303],
            'Response status code is equal 400' => [400]
        ];
    }

    private function makeNovaPoshtaApiClient(): NovaPoshtaApiClient
    {
        return new NovaPoshtaApiClient(
            $this->settings,
            $this->client,
            $this->requestFactory,
            $this->streamFactory,
            $this->serializer,
            $this->validator,
            $this->logger
        );
    }

    /**
     * @return RequestInterface|MockObject
     */
    private function getRequestMock(): RequestInterface
    {
        $request = $this->createMock(RequestInterface::class);
        $request
            ->method('withBody')
            ->willReturn($request);
        $request
            ->method('withHeader')
            ->willReturn($request);
        $request
            ->method('getHeaders')
            ->willReturn([]);

        return $request;
    }

    /**
     * @return ResponseInterface|MockObject
     */
    private function getResponseMock(): ResponseInterface
    {
        $response = $this->createMock(ResponseInterface::class);
        $response
            ->method('getHeaders')
            ->willReturn([]);

        return $response;
    }

    /**
     * @return StreamInterface|MockObject
     */
    private function getResponseBodyMock(string $body): StreamInterface
    {
        $responseBody = $this->createMock(StreamInterface::class);
        $responseBody
            ->method('getContents')
            ->willReturn($body);

        return $responseBody;
    }

    /**
     * @return ApiResponseInterface|MockObject
     */
    private function getApiResponseMock(): ApiResponseInterface
    {
        $apiResponseMock = $this->getMockBuilder(ApiResponseInterface::class)
            ->addMethods(['toArray'])
            ->getMockForAbstractClass();
        $apiResponseMock
            ->method('toArray')
            ->willReturn([]);

        return $apiResponseMock;
    }
}
