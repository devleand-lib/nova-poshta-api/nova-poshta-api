<?php

declare(strict_types=1);

namespace Devleand\Tests\NovaPoshta\Api\V2\Transformer;

use Exception;
use JMS\Serializer\ArrayTransformerInterface;
use Devleand\NovaPoshta\Api\V2\Contracts\Exception\DataToEntityTransformException;
use Devleand\NovaPoshta\Api\V2\Contracts\Exception\UnexpectedEntityException;
use Devleand\NovaPoshta\Api\V2\Exception\EntityValidationFailedException;
use Devleand\NovaPoshta\Api\V2\Transformer\DataToEntityTransformer;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class DataToEntityTransformerTest extends TestCase
{
    /**
     * @var ArrayTransformerInterface
     */
    private ArrayTransformerInterface $arrayTransformer;

    /**
     * @var ValidatorInterface
     */
    private ValidatorInterface $validator;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    protected function setUp(): void
    {
        $this->arrayTransformer = $this->createMock(ArrayTransformerInterface::class);
        $this->validator = $this->createMock(ValidatorInterface::class);
        $this->logger = $this->createMock(LoggerInterface::class);
    }

    public function testTransformWhenMakeEntityFailed(): void
    {
        $data = [['test key' => '12345 qwe']];
        $this->arrayTransformer
            ->method('fromArray')
            ->willThrowException(new Exception());
        $transformer = $this->makeTransformer();

        $this->expectExceptionObject(new DataToEntityTransformException($data[0], TestEntity::class));

        $entities = $transformer->transform($data, TestEntity::class);
        iterator_to_array($entities);
    }

    public function testTransformWhenInvalidEntityType(): void
    {
        $data = [['test key' => '12345 qwe']];
        $entity = new \stdClass();
        $this->arrayTransformer
            ->method('fromArray')
            ->willReturn($entity);
        $transformer = $this->makeTransformer();

        $this->expectExceptionObject(new UnexpectedEntityException($entity, TestEntity::class));

        $entities = $transformer->transform($data, TestEntity::class);
        iterator_to_array($entities);
    }

    public function testTransformWhenEntityValidationFailed(): void
    {
        $data = [['test key' => '12345 qwe']];
        $entity = new TestEntity();
        $this->arrayTransformer
            ->method('fromArray')
            ->willReturn($entity);
        $validationErrors = $this->createMock(ConstraintViolationListInterface::class);
        $validationErrors
            ->method('count')
            ->willReturn(1);
        $this->validator
            ->method('validate')
            ->willReturn($validationErrors);
        $transformer = $this->makeTransformer();

        $this->expectExceptionObject(new EntityValidationFailedException($entity));
        $this->expectExceptionMessage('Entity validation failed.');

        $entities = $transformer->transform($data, TestEntity::class);
        iterator_to_array($entities);
    }

    /**
     * @dataProvider provideTransformData
     *
     * @param array<mixed, mixed> $data
     */
    public function testTransform(array $data): void
    {
        $entity = new TestEntity();
        $this->arrayTransformer
            ->method('fromArray')
            ->willReturn($entity);
        $validationErrors = $this->createMock(ConstraintViolationListInterface::class);
        $validationErrors
            ->method('count')
            ->willReturn(0);
        $this->validator
            ->method('validate')
            ->willReturn($validationErrors);
        $transformer = $this->makeTransformer();

        $lazyEntities = $transformer->transform($data, TestEntity::class);
        $entities = iterator_to_array($lazyEntities);

        $this->assertEquals(
            array_fill(0, count($data), $entity),
            $entities
        );
    }

    public function provideTransformData(): array
    {
        return [
            'Transform empty array' => [[]],
            'Transform array with one value' => [[['test key' => 'value']]],
            'Transform array with many values' => [
                [
                    [
                        'test 1 key' => 'test 1',
                        'key' => 'test 2',
                        'second' => 43,
                        'boolean' => true
                    ],
                    [
                        'test 2 key' => 'test 2',
                        'key' => 'test',
                        'second' => 65,
                        'boolean' => true
                    ],
                    [
                        'test 3 key' => 'test 3',
                        'key' => 'test',
                        'second' => 87,
                        'boolean' => false
                    ]
                ]
            ]
        ];
    }

    private function makeTransformer(): DataToEntityTransformer
    {
        return new DataToEntityTransformer($this->arrayTransformer, $this->validator, $this->logger);
    }
}
