<?php

declare(strict_types=1);

namespace Devleand\Tests\NovaPoshta\Api\V2\Model\Common;

use Devleand\NovaPoshta\Api\V2\Contracts\Response\ApiResponseInterface;
use Devleand\NovaPoshta\Api\V2\Enum\Request\HttpMethod;
use Devleand\NovaPoshta\Api\V2\Model\Common\CommonModel;
use Devleand\NovaPoshta\Api\V2\Model\Common\Enum\CommonMethod;
use Devleand\Tests\NovaPoshta\Api\V2\Model\ModelTestCase;

class CommonModelTest extends ModelTestCase
{
    public function testGetCargoTypes(): void
    {
        $apiResponse = $this->createMock(ApiResponseInterface::class);
        $transformedApiResponse = $this->createMock(ApiResponseInterface::class);
        $this->apiClient
            ->method('send')
            ->willReturn($apiResponse);
        $apiResponse
            ->method('withData')
            ->willReturn($transformedApiResponse);
        $model = $this->makeModel();

        $this->apiClient
            ->expects($this->once())
            ->method('send')
            ->with(HttpMethod::POST(), $model, CommonMethod::GET_CARGO_TYPES, null);

        $actualApiResponse = $model->getCargoTypes();

        $this->assertEquals($transformedApiResponse, $actualApiResponse);
    }

    public function testGetBackwardDeliveryCargoTypes(): void
    {
        $apiResponse = $this->createMock(ApiResponseInterface::class);
        $transformedApiResponse = $this->createMock(ApiResponseInterface::class);
        $this->apiClient
            ->method('send')
            ->willReturn($apiResponse);
        $apiResponse
            ->method('withData')
            ->willReturn($transformedApiResponse);
        $model = $this->makeModel();

        $this->apiClient
            ->expects($this->once())
            ->method('send')
            ->with(HttpMethod::POST(), $model, CommonMethod::GET_BACKWARD_DELIVERY_CARGO_TYPES, null);

        $actualApiResponse = $model->getBackwardDeliveryCargoTypes();

        $this->assertEquals($transformedApiResponse, $actualApiResponse);
    }

    public function testGetTypesOfPayers(): void
    {
        $apiResponse = $this->createMock(ApiResponseInterface::class);
        $transformedApiResponse = $this->createMock(ApiResponseInterface::class);
        $this->apiClient
            ->method('send')
            ->willReturn($apiResponse);
        $apiResponse
            ->method('withData')
            ->willReturn($transformedApiResponse);
        $model = $this->makeModel();

        $this->apiClient
            ->expects($this->once())
            ->method('send')
            ->with(HttpMethod::POST(), $model, CommonMethod::GET_TYPES_OF_PAYERS, null);

        $actualApiResponse = $model->getTypesOfPayers();

        $this->assertEquals($transformedApiResponse, $actualApiResponse);
    }

    public function testGetTypesOfPayersForRedelivery(): void
    {
        $apiResponse = $this->createMock(ApiResponseInterface::class);
        $transformedApiResponse = $this->createMock(ApiResponseInterface::class);
        $this->apiClient
            ->method('send')
            ->willReturn($apiResponse);
        $apiResponse
            ->method('withData')
            ->willReturn($transformedApiResponse);
        $model = $this->makeModel();

        $this->apiClient
            ->expects($this->once())
            ->method('send')
            ->with(HttpMethod::POST(), $model, CommonMethod::GET_TYPES_OF_PAYERS_FOR_REDELIVERY, null);

        $actualApiResponse = $model->getTypesOfPayersForRedelivery();

        $this->assertEquals($transformedApiResponse, $actualApiResponse);
    }

    public function testGetCargoDescriptionList(): void
    {
        $apiResponse = $this->createMock(ApiResponseInterface::class);
        $transformedApiResponse = $this->createMock(ApiResponseInterface::class);
        $this->apiClient
            ->method('send')
            ->willReturn($apiResponse);
        $apiResponse
            ->method('withData')
            ->willReturn($transformedApiResponse);
        $model = $this->makeModel();

        $this->apiClient
            ->expects($this->once())
            ->method('send')
            ->with(HttpMethod::POST(), $model, CommonMethod::GET_CARGO_DESCRIPTION_LIST, null);

        $actualApiResponse = $model->getCargoDescriptionList();

        $this->assertEquals($transformedApiResponse, $actualApiResponse);
    }

    public function testGetServiceTypes(): void
    {
        $apiResponse = $this->createMock(ApiResponseInterface::class);
        $transformedApiResponse = $this->createMock(ApiResponseInterface::class);
        $this->apiClient
            ->method('send')
            ->willReturn($apiResponse);
        $apiResponse
            ->method('withData')
            ->willReturn($transformedApiResponse);
        $model = $this->makeModel();

        $this->apiClient
            ->expects($this->once())
            ->method('send')
            ->with(HttpMethod::POST(), $model, CommonMethod::GET_SERVICE_TYPES, null);

        $actualApiResponse = $model->getServiceTypes();

        $this->assertEquals($transformedApiResponse, $actualApiResponse);
    }

    public function testGetTypesOfCounterparties(): void
    {
        $apiResponse = $this->createMock(ApiResponseInterface::class);
        $transformedApiResponse = $this->createMock(ApiResponseInterface::class);
        $this->apiClient
            ->method('send')
            ->willReturn($apiResponse);
        $apiResponse
            ->method('withData')
            ->willReturn($transformedApiResponse);
        $model = $this->makeModel();

        $this->apiClient
            ->expects($this->once())
            ->method('send')
            ->with(HttpMethod::POST(), $model, CommonMethod::GET_TYPES_OF_COUNTERPARTIES, null);

        $actualApiResponse = $model->getTypesOfCounterparties();

        $this->assertEquals($transformedApiResponse, $actualApiResponse);
    }

    public function testGetPaymentForms(): void
    {
        $apiResponse = $this->createMock(ApiResponseInterface::class);
        $transformedApiResponse = $this->createMock(ApiResponseInterface::class);
        $this->apiClient
            ->method('send')
            ->willReturn($apiResponse);
        $apiResponse
            ->method('withData')
            ->willReturn($transformedApiResponse);
        $model = $this->makeModel();

        $this->apiClient
            ->expects($this->once())
            ->method('send')
            ->with(HttpMethod::POST(), $model, CommonMethod::GET_PAYMENT_FORMS, null);

        $actualApiResponse = $model->getPaymentForms();

        $this->assertEquals($transformedApiResponse, $actualApiResponse);
    }

    public function testGetOwnershipFormsList(): void
    {
        $apiResponse = $this->createMock(ApiResponseInterface::class);
        $transformedApiResponse = $this->createMock(ApiResponseInterface::class);
        $this->apiClient
            ->method('send')
            ->willReturn($apiResponse);
        $apiResponse
            ->method('withData')
            ->willReturn($transformedApiResponse);
        $model = $this->makeModel();

        $this->apiClient
            ->expects($this->once())
            ->method('send')
            ->with(HttpMethod::POST(), $model, CommonMethod::GET_OWNERSHIP_FORMS_LIST, null);

        $actualApiResponse = $model->getOwnershipFormsList();

        $this->assertEquals($transformedApiResponse, $actualApiResponse);
    }

    protected function makeModel(): CommonModel
    {
        return new CommonModel(
            $this->apiClient,
            $this->dataToEntityTransformer,
            $this->arrayTransformer,
            $this->logger
        );
    }
}
