<?php

declare(strict_types=1);

namespace Devleand\Tests\NovaPoshta\Api\V2\Model;

use Devleand\NovaPoshta\Api\V2\Contracts\Model\ModelInterface;
use Devleand\NovaPoshta\Api\V2\Model\Enum\ApiModel;

class ModelMock implements ModelInterface
{
    public function getApiModel(): ApiModel
    {
        return ApiModel::COMMON();
    }

    public function isApiMethodSupported(string $apiMethod): bool
    {
        return true;
    }
}
