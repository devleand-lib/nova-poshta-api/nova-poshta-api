<?php

declare(strict_types=1);

namespace Devleand\Tests\NovaPoshta\Api\V2\Model\CommonGeneral;

use Devleand\NovaPoshta\Api\V2\Contracts\Response\ApiResponseInterface;
use Devleand\NovaPoshta\Api\V2\Enum\Request\HttpMethod;
use Devleand\NovaPoshta\Api\V2\Model\CommonGeneral\CommonGeneralModel;
use Devleand\NovaPoshta\Api\V2\Model\CommonGeneral\Enum\CommonGeneralMethod;
use Devleand\Tests\NovaPoshta\Api\V2\Model\ModelTestCase;

class CommonGeneralModelTest extends ModelTestCase
{
    public function testGetMessageCodeText(): void
    {
        $apiResponse = $this->createMock(ApiResponseInterface::class);
        $transformedApiResponse = $this->createMock(ApiResponseInterface::class);
        $this->apiClient
            ->method('send')
            ->willReturn($apiResponse);
        $apiResponse
            ->method('withData')
            ->willReturn($transformedApiResponse);
        $model = $this->makeModel();

        $this->apiClient
            ->expects($this->once())
            ->method('send')
            ->with(HttpMethod::POST(), $model, CommonGeneralMethod::GET_MESSAGE_CODE_TEXT, null);

        $actualApiResponse = $model->getMessageCodeText();

        $this->assertEquals($transformedApiResponse, $actualApiResponse);
    }

    protected function makeModel(): CommonGeneralModel
    {
        return new CommonGeneralModel(
            $this->apiClient,
            $this->dataToEntityTransformer,
            $this->arrayTransformer,
            $this->logger
        );
    }
}
