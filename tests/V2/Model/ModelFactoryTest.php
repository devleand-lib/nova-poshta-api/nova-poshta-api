<?php

declare(strict_types=1);

namespace Devleand\Tests\NovaPoshta\Api\V2\Model;

use Devleand\NovaPoshta\Api\V2\Contracts\Model\ModelInterface;
use Devleand\NovaPoshta\Api\V2\Contracts\NovaPoshtaApiClientFactoryInterface;
use Devleand\NovaPoshta\Api\V2\Contracts\Transformer\DataToEntityTransformer;
use Devleand\NovaPoshta\Api\V2\Enum\Language;
use Devleand\NovaPoshta\Api\V2\Model\ModelFactory;
use Devleand\NovaPoshta\Api\V2\NovaPoshtaApiClient;
use InvalidArgumentException;
use JMS\Serializer\ArrayTransformerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class ModelFactoryTest extends TestCase
{
    public function testMakeWithInvalidModelClass(): void
    {
        $factory = new ModelFactory(
            $this->createMock(NovaPoshtaApiClientFactoryInterface::class),
            $this->createMock(DataToEntityTransformer::class),
            $this->createMock(ArrayTransformerInterface::class),
            $this->createMock(LoggerInterface::class)
        );

        $this->expectExceptionObject(new InvalidArgumentException(
            sprintf("Class %s must implement %s.", self::class, ModelInterface::class)
        ));

        $factory->make(self::class, '12345');
    }

    public function testMakeWithoutLanguage(): void
    {
        $apiClientFactoryMock = $this->createMock(NovaPoshtaApiClientFactoryInterface::class);
        $apiClientFactoryMock
            ->method('make')
            ->willReturn($this->createMock(NovaPoshtaApiClient::class));
        $factory = new ModelFactory(
            $apiClientFactoryMock,
            $this->createMock(DataToEntityTransformer::class),
            $this->createMock(ArrayTransformerInterface::class),
            $this->createMock(LoggerInterface::class)
        );
        $key = '12345abcde';

        $apiClientFactoryMock
            ->expects($this->once())
            ->method('make')
            ->with($key, null);

        $factory->make(ModelMock::class, $key);
    }

    public function testMakeWithLanguage(): void
    {
        $apiClientFactoryMock = $this->createMock(NovaPoshtaApiClientFactoryInterface::class);
        $apiClientFactoryMock
            ->method('make')
            ->willReturn($this->createMock(NovaPoshtaApiClient::class));
        $factory = new ModelFactory(
            $apiClientFactoryMock,
            $this->createMock(DataToEntityTransformer::class),
            $this->createMock(ArrayTransformerInterface::class),
            $this->createMock(LoggerInterface::class)
        );
        $key = '12345abcde';
        $language = Language::RU();

        $apiClientFactoryMock
            ->expects($this->once())
            ->method('make')
            ->with($key, $language);

        $factory->make(ModelMock::class, $key, $language);
    }
}
