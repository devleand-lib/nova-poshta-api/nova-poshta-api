<?php

declare(strict_types=1);

namespace Devleand\Tests\NovaPoshta\Api\V2\Model\AddressGeneral;

use Devleand\NovaPoshta\Api\V2\Contracts\Response\ApiResponseInterface;
use Devleand\NovaPoshta\Api\V2\Enum\Request\HttpMethod;
use Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\AddressGeneralModel;
use Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Dto\GetSettlementsMethod\GetSettlementsFiltersDto;
use Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Dto\GetWarehousesMethod\GetWarehousesFiltersDto;
use Devleand\NovaPoshta\Api\V2\Model\AddressGeneral\Enum\AddressGeneralMethod;
use Devleand\NovaPoshta\Api\V2\Dto\Model\PaginationPropertiesDto;
use Devleand\Tests\NovaPoshta\Api\V2\Model\ModelTestCase;

class AddressGeneralModelTest extends ModelTestCase
{
    public function testGetSettlementsWithoutProperties(): void
    {
        $apiResponse = $this->createMock(ApiResponseInterface::class);
        $transformedApiResponse = $this->createMock(ApiResponseInterface::class);
        $this->apiClient
            ->method('send')
            ->willReturn($apiResponse);
        $apiResponse
            ->method('withData')
            ->willReturn($transformedApiResponse);
        $model = $this->makeModel();

        $this->apiClient
            ->expects($this->once())
            ->method('send')
            ->with(HttpMethod::POST(), $model, AddressGeneralMethod::GET_SETTLEMENTS, null);

        $actualApiResponse = $model->getSettlements();

        $this->assertEquals($transformedApiResponse, $actualApiResponse);
    }

    public function testGetSettlementsWithPaginationProperties(): void
    {
        $apiResponse = $this->createMock(ApiResponseInterface::class);
        $transformedApiResponse = $this->createMock(ApiResponseInterface::class);
        $this->apiClient
            ->method('send')
            ->willReturn($apiResponse);
        $apiResponse
            ->method('withData')
            ->willReturn($transformedApiResponse);
        $this->arrayTransformer
            ->method('toArray')
            ->willReturn(['Page' => '45', 'Limit' => '100']);
        $model = $this->makeModel();

        $this->apiClient
            ->expects($this->once())
            ->method('send')
            ->with(
                HttpMethod::POST(),
                $model,
                AddressGeneralMethod::GET_SETTLEMENTS,
                ['Page' => '45', 'Limit' => '100']
            );

        $actualApiResponse = $model->getSettlements([new PaginationPropertiesDto(45, 100)]);

        $this->assertEquals($transformedApiResponse, $actualApiResponse);
    }

    public function testGetSettlementsWithFilters(): void
    {
        $apiResponse = $this->createMock(ApiResponseInterface::class);
        $transformedApiResponse = $this->createMock(ApiResponseInterface::class);
        $this->apiClient
            ->method('send')
            ->willReturn($apiResponse);
        $apiResponse
            ->method('withData')
            ->willReturn($transformedApiResponse);
        $this->arrayTransformer
            ->method('toArray')
            ->willReturn(['RegionRef' => 'regionRef', 'Warehouse' => '1']);
        $model = $this->makeModel();

        $this->apiClient
            ->expects($this->once())
            ->method('send')
            ->with(
                HttpMethod::POST(),
                $model,
                AddressGeneralMethod::GET_SETTLEMENTS,
                ['RegionRef' => 'regionRef', 'Warehouse' => '1']
            );

        $actualApiResponse = $model->getSettlements([new GetSettlementsFiltersDto('regionRef', true)]);

        $this->assertEquals($transformedApiResponse, $actualApiResponse);
    }

    public function testGetSettlementWithProperties(): void
    {
        $apiResponse = $this->createMock(ApiResponseInterface::class);
        $transformedApiResponse = $this->createMock(ApiResponseInterface::class);
        $this->apiClient
            ->method('send')
            ->willReturn($apiResponse);
        $apiResponse
            ->method('withData')
            ->willReturn($transformedApiResponse);
        $this->arrayTransformer
            ->method('toArray')
            ->willReturn(['RegionRef' => 'regionRef', 'Warehouse' => '0', 'Page' => '67', 'Limit' => '134']);
        $model = $this->makeModel();

        $this->apiClient
            ->expects($this->once())
            ->method('send')
            ->with(
                HttpMethod::POST(),
                $model,
                AddressGeneralMethod::GET_SETTLEMENTS,
                ['RegionRef' => 'regionRef', 'Warehouse' => '0', 'Page' => '67', 'Limit' => '134']
            );

        $actualApiResponse = $model->getSettlements([
            new GetSettlementsFiltersDto('regionRef', false),
            new PaginationPropertiesDto(67, 134)
        ]);

        $this->assertEquals($transformedApiResponse, $actualApiResponse);
    }

    public function testFindSettlementByRef(): void
    {
        $apiResponse = $this->createMock(ApiResponseInterface::class);
        $transformedApiResponse = $this->createMock(ApiResponseInterface::class);
        $this->apiClient
            ->method('send')
            ->willReturn($apiResponse);
        $apiResponse
            ->method('withData')
            ->willReturn($transformedApiResponse);
        $this->arrayTransformer
            ->method('toArray')
            ->willReturn(['Ref' => 'settlementTestRef']);
        $model = $this->makeModel();

        $this->apiClient
            ->expects($this->once())
            ->method('send')
            ->with(
                HttpMethod::POST(),
                $model,
                AddressGeneralMethod::GET_SETTLEMENTS,
                ['Ref' => 'settlementTestRef']
            );

        $actualApiResponse = $model->findSettlementByRef('settlementTestRef');

        $this->assertEquals($transformedApiResponse, $actualApiResponse);
    }

    public function testFindSettlementByString(): void
    {
        $apiResponse = $this->createMock(ApiResponseInterface::class);
        $transformedApiResponse = $this->createMock(ApiResponseInterface::class);
        $this->apiClient
            ->method('send')
            ->willReturn($apiResponse);
        $apiResponse
            ->method('withData')
            ->willReturn($transformedApiResponse);
        $this->arrayTransformer
            ->method('toArray')
            ->willReturn(['FindByString' => 'settlement test string']);
        $model = $this->makeModel();

        $this->apiClient
            ->expects($this->once())
            ->method('send')
            ->with(
                HttpMethod::POST(),
                $model,
                AddressGeneralMethod::GET_SETTLEMENTS,
                ['FindByString' => 'settlement test string']
            );

        $actualApiResponse = $model->findSettlementByString('settlement test string');

        $this->assertEquals($transformedApiResponse, $actualApiResponse);
    }

    public function testGetWarehouseTypes(): void
    {
        $apiResponse = $this->createMock(ApiResponseInterface::class);
        $transformedApiResponse = $this->createMock(ApiResponseInterface::class);
        $this->apiClient
            ->method('send')
            ->willReturn($apiResponse);
        $apiResponse
            ->method('withData')
            ->willReturn($transformedApiResponse);
        $model = $this->makeModel();

        $this->apiClient
            ->expects($this->once())
            ->method('send')
            ->with(HttpMethod::POST(), $model, AddressGeneralMethod::GET_WAREHOUSE_TYPES, null);

        $actualApiResponse = $model->getWarehouseTypes();

        $this->assertEquals($transformedApiResponse, $actualApiResponse);
    }

    public function testGetWarehousesWithoutProperties(): void
    {
        $apiResponse = $this->createMock(ApiResponseInterface::class);
        $transformedApiResponse = $this->createMock(ApiResponseInterface::class);
        $this->apiClient
            ->method('send')
            ->willReturn($apiResponse);
        $apiResponse
            ->method('withData')
            ->willReturn($transformedApiResponse);
        $model = $this->makeModel();

        $this->apiClient
            ->expects($this->once())
            ->method('send')
            ->with(HttpMethod::POST(), $model, AddressGeneralMethod::GET_WAREHOUSES, null);

        $actualApiResponse = $model->getWarehouses();

        $this->assertEquals($transformedApiResponse, $actualApiResponse);
    }

    public function testGetWarehousesWithPaginationProperties(): void
    {
        $apiResponse = $this->createMock(ApiResponseInterface::class);
        $transformedApiResponse = $this->createMock(ApiResponseInterface::class);
        $this->apiClient
            ->method('send')
            ->willReturn($apiResponse);
        $apiResponse
            ->method('withData')
            ->willReturn($transformedApiResponse);
        $this->arrayTransformer
            ->method('toArray')
            ->willReturn(['Page' => '45', 'Limit' => '100']);
        $model = $this->makeModel();

        $this->apiClient
            ->expects($this->once())
            ->method('send')
            ->with(
                HttpMethod::POST(),
                $model,
                AddressGeneralMethod::GET_WAREHOUSES,
                ['Page' => '45', 'Limit' => '100']
            );

        $actualApiResponse = $model->getWarehouses([new PaginationPropertiesDto(45, 100)]);

        $this->assertEquals($transformedApiResponse, $actualApiResponse);
    }

    public function testGetWarehousesWithFilters(): void
    {
        $apiResponse = $this->createMock(ApiResponseInterface::class);
        $transformedApiResponse = $this->createMock(ApiResponseInterface::class);
        $this->apiClient
            ->method('send')
            ->willReturn($apiResponse);
        $apiResponse
            ->method('withData')
            ->willReturn($transformedApiResponse);
        $this->arrayTransformer
            ->method('toArray')
            ->willReturn([
                'TypeOfWarehouseRef' => 'warehouseTypeRef',
                'CityRef' => 'cityRef',
                'CityName' => 'cityDescriptionUk',
                'PostFinance' => '1',
                'BicycleParking' => '0'
            ]);
        $model = $this->makeModel();

        $this->apiClient
            ->expects($this->once())
            ->method('send')
            ->with(
                HttpMethod::POST(),
                $model,
                AddressGeneralMethod::GET_WAREHOUSES,
                [
                    'TypeOfWarehouseRef' => 'warehouseTypeRef',
                    'CityRef' => 'cityRef',
                    'CityName' => 'cityDescriptionUk',
                    'PostFinance' => '1',
                    'BicycleParking' => '0'
                ]
            );

        $actualApiResponse = $model->getWarehouses([
            new GetWarehousesFiltersDto(
                'warehouseTypeRef',
                'cityRef',
                'cityDescriptionUk',
                true,
                false
            )
        ]);

        $this->assertEquals($transformedApiResponse, $actualApiResponse);
    }

    public function testGetWarehousesWithProperties(): void
    {
        $apiResponse = $this->createMock(ApiResponseInterface::class);
        $transformedApiResponse = $this->createMock(ApiResponseInterface::class);
        $this->apiClient
            ->method('send')
            ->willReturn($apiResponse);
        $apiResponse
            ->method('withData')
            ->willReturn($transformedApiResponse);
        $this->arrayTransformer
            ->method('toArray')
            ->willReturn([
                'TypeOfWarehouseRef' => 'warehouseTypeRef',
                'CityRef' => 'cityRef',
                'CityName' => 'cityDescriptionUk',
                'PostFinance' => '0',
                'BicycleParking' => '1',
                'Page' => '67',
                'Limit' => '134'
            ]);
        $model = $this->makeModel();

        $this->apiClient
            ->expects($this->once())
            ->method('send')
            ->with(
                HttpMethod::POST(),
                $model,
                AddressGeneralMethod::GET_WAREHOUSES,
                [
                    'TypeOfWarehouseRef' => 'warehouseTypeRef',
                    'CityRef' => 'cityRef',
                    'CityName' => 'cityDescriptionUk',
                    'PostFinance' => '0',
                    'BicycleParking' => '1',
                    'Page' => '67',
                    'Limit' => '134'
                ]
            );

        $actualApiResponse = $model->getWarehouses([
            new GetWarehousesFiltersDto(
                'warehouseTypeRef',
                'cityRef',
                'cityDescriptionUk',
                false,
                true
            ),
            new PaginationPropertiesDto(67, 134)
        ]);

        $this->assertEquals($transformedApiResponse, $actualApiResponse);
    }

    public function testFindWarehouseByRef(): void
    {
        $apiResponse = $this->createMock(ApiResponseInterface::class);
        $transformedApiResponse = $this->createMock(ApiResponseInterface::class);
        $this->apiClient
            ->method('send')
            ->willReturn($apiResponse);
        $apiResponse
            ->method('withData')
            ->willReturn($transformedApiResponse);
        $this->arrayTransformer
            ->method('toArray')
            ->willReturn(['Ref' => 'warehouseRef']);
        $model = $this->makeModel();

        $this->apiClient
            ->expects($this->once())
            ->method('send')
            ->with(
                HttpMethod::POST(),
                $model,
                AddressGeneralMethod::GET_WAREHOUSES,
                ['Ref' => 'warehouseRef']
            );

        $actualApiResponse = $model->findWarehouseByRef('warehouseRef');

        $this->assertEquals($transformedApiResponse, $actualApiResponse);
    }

    public function testFindWarehousesByString(): void
    {
        $apiResponse = $this->createMock(ApiResponseInterface::class);
        $transformedApiResponse = $this->createMock(ApiResponseInterface::class);
        $this->apiClient
            ->method('send')
            ->willReturn($apiResponse);
        $apiResponse
            ->method('withData')
            ->willReturn($transformedApiResponse);
        $this->arrayTransformer
            ->method('toArray')
            ->willReturn(['FindByString' => 'warehouse test string']);
        $model = $this->makeModel();

        $this->apiClient
            ->expects($this->once())
            ->method('send')
            ->with(
                HttpMethod::POST(),
                $model,
                AddressGeneralMethod::GET_WAREHOUSES,
                ['FindByString' => 'warehouse test string']
            );

        $actualApiResponse = $model->findWarehouseByString('warehouse test string');

        $this->assertEquals($transformedApiResponse, $actualApiResponse);
    }

    public function testFindWarehousesBySettlement(): void
    {
        $apiResponse = $this->createMock(ApiResponseInterface::class);
        $transformedApiResponse = $this->createMock(ApiResponseInterface::class);
        $this->apiClient
            ->method('send')
            ->willReturn($apiResponse);
        $apiResponse
            ->method('withData')
            ->willReturn($transformedApiResponse);
        $this->arrayTransformer
            ->method('toArray')
            ->willReturn(['SettlementRef' => 'settlementRef']);
        $model = $this->makeModel();

        $this->apiClient
            ->expects($this->once())
            ->method('send')
            ->with(
                HttpMethod::POST(),
                $model,
                AddressGeneralMethod::GET_WAREHOUSES,
                ['SettlementRef' => 'settlementRef']
            );

        $actualApiResponse = $model->findWarehousesBySettlement('settlementRef');

        $this->assertEquals($transformedApiResponse, $actualApiResponse);
    }

    protected function makeModel(): AddressGeneralModel
    {
        return new AddressGeneralModel(
            $this->apiClient,
            $this->dataToEntityTransformer,
            $this->arrayTransformer,
            $this->logger
        );
    }
}
