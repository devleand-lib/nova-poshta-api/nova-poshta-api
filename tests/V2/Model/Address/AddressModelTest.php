<?php

declare(strict_types=1);

namespace Devleand\Tests\NovaPoshta\Api\V2\Model\Address;

use Devleand\NovaPoshta\Api\V2\Contracts\Response\ApiResponseInterface;
use Devleand\NovaPoshta\Api\V2\Enum\Request\HttpMethod;
use Devleand\NovaPoshta\Api\V2\Model\Address\AddressModel;
use Devleand\NovaPoshta\Api\V2\Model\Address\Enum\AddressMethod;
use Devleand\Tests\NovaPoshta\Api\V2\Model\ModelTestCase;

class AddressModelTest extends ModelTestCase
{
    public function testGetAreas(): void
    {
        $apiResponse = $this->createMock(ApiResponseInterface::class);
        $transformedApiResponse = $this->createMock(ApiResponseInterface::class);
        $this->apiClient
            ->method('send')
            ->willReturn($apiResponse);
        $apiResponse
            ->method('withData')
            ->willReturn($transformedApiResponse);
        $model = $this->makeModel();

        $this->apiClient
            ->expects($this->once())
            ->method('send')
            ->with(HttpMethod::POST(), $model, AddressMethod::GET_AREAS, null);

        $actualApiResponse = $model->getAreas();

        $this->assertEquals($transformedApiResponse, $actualApiResponse);
    }

    protected function makeModel(): AddressModel
    {
        return new AddressModel(
            $this->apiClient,
            $this->dataToEntityTransformer,
            $this->arrayTransformer,
            $this->logger
        );
    }
}
