<?php

declare(strict_types=1);

namespace Devleand\Tests\NovaPoshta\Api\V2\Model\ContactPerson;

use Devleand\NovaPoshta\Api\V2\Contracts\Response\ApiResponseInterface;
use Devleand\NovaPoshta\Api\V2\Enum\Request\HttpMethod;
use Devleand\NovaPoshta\Api\V2\Model\ContactPerson\ContactPersonModel;
use Devleand\NovaPoshta\Api\V2\Model\ContactPerson\Dto\ContactPersonDto;
use Devleand\NovaPoshta\Api\V2\Model\ContactPerson\Enum\ContactPersonMethod;
use Devleand\Tests\NovaPoshta\Api\V2\Model\ModelTestCase;

class ContactPersonModelTest extends ModelTestCase
{
    public function testSave(): void
    {
        $apiResponse = $this->createMock(ApiResponseInterface::class);
        $transformedApiResponse = $this->createMock(ApiResponseInterface::class);
        $this->apiClient
            ->method('send')
            ->willReturn($apiResponse);
        $apiResponse
            ->method('withData')
            ->willReturn($transformedApiResponse);
        $model = $this->makeModel();
        $contactPersonDto = $this->createMock(ContactPersonDto::class);
        $this->arrayTransformer
            ->method('toArray')
            ->willReturn([
                'CounterpartyRef' => 'counterparty-ref',
                'FirstName' => 'First name',
                'LastName' => 'Last name',
                'MiddleName' => 'Middle name',
                'Phone' => '+380969999999'
            ]);

        $this->apiClient
            ->expects($this->once())
            ->method('send')
            ->with(
                HttpMethod::POST(),
                $model,
                ContactPersonMethod::SAVE,
                [
                    'CounterpartyRef' => 'counterparty-ref',
                    'FirstName' => 'First name',
                    'LastName' => 'Last name',
                    'MiddleName' => 'Middle name',
                    'Phone' => '+380969999999'
                ]
            );

        $actualApiResponse = $model->save($contactPersonDto);

        $this->assertEquals($transformedApiResponse, $actualApiResponse);
    }

    public function testUpdate(): void
    {
        $apiResponse = $this->createMock(ApiResponseInterface::class);
        $transformedApiResponse = $this->createMock(ApiResponseInterface::class);
        $this->apiClient
            ->method('send')
            ->willReturn($apiResponse);
        $apiResponse
            ->method('withData')
            ->willReturn($transformedApiResponse);
        $model = $this->makeModel();
        $contactPersonDto = $this->createMock(ContactPersonDto::class);
        $this->arrayTransformer
            ->method('toArray')
            ->willReturn([
                'Ref' => 'contact-person-ref',
                'CounterpartyRef' => 'counterparty-ref',
                'FirstName' => 'First name',
                'LastName' => 'Last name',
                'MiddleName' => 'Middle name',
                'Phone' => '+380969999999'
            ]);

        $this->apiClient
            ->expects($this->once())
            ->method('send')
            ->with(
                HttpMethod::POST(),
                $model,
                ContactPersonMethod::UPDATE,
                [
                    'Ref' => 'contact-person-ref',
                    'CounterpartyRef' => 'counterparty-ref',
                    'FirstName' => 'First name',
                    'LastName' => 'Last name',
                    'MiddleName' => 'Middle name',
                    'Phone' => '+380969999999'
                ]
            );

        $actualApiResponse = $model->update('contact-person-ref', $contactPersonDto);

        $this->assertEquals($transformedApiResponse, $actualApiResponse);
    }

    public function testDelete(): void
    {
        $apiResponse = $this->createMock(ApiResponseInterface::class);
        $transformedApiResponse = $this->createMock(ApiResponseInterface::class);
        $this->apiClient
            ->method('send')
            ->willReturn($apiResponse);
        $apiResponse
            ->method('withData')
            ->willReturn($transformedApiResponse);
        $model = $this->makeModel();
        $this->arrayTransformer
            ->method('toArray')
            ->willReturn([
                'Ref' => 'contact-person-ref'
            ]);

        $this->apiClient
            ->expects($this->once())
            ->method('send')
            ->with(
                HttpMethod::POST(),
                $model,
                ContactPersonMethod::DELETE,
                [
                    'Ref' => 'contact-person-ref'
                ]
            );

        $actualApiResponse = $model->delete('contact-person-ref');

        $this->assertEquals($transformedApiResponse, $actualApiResponse);
    }

    protected function makeModel(): ContactPersonModel
    {
        return new ContactPersonModel(
            $this->apiClient,
            $this->dataToEntityTransformer,
            $this->arrayTransformer,
            $this->logger
        );
    }
}
