<?php

declare(strict_types=1);

namespace Devleand\Tests\NovaPoshta\Api\V2\Model;

use JMS\Serializer\ArrayTransformerInterface;
use Devleand\NovaPoshta\Api\V2\Contracts\ApiClientInterface;
use Devleand\NovaPoshta\Api\V2\Contracts\Transformer\DataToEntityTransformer;
use Devleand\NovaPoshta\Api\V2\Model\Model;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

abstract class ModelTestCase extends TestCase
{
    /**
     * @var ApiClientInterface
     */
    protected ApiClientInterface $apiClient;

    /**
     * @var DataToEntityTransformer
     */
    protected DataToEntityTransformer $dataToEntityTransformer;

    /**
     * @var ArrayTransformerInterface
     */
    protected ArrayTransformerInterface $arrayTransformer;

    /**
     * @var LoggerInterface
     */
    protected LoggerInterface $logger;

    protected function setUp(): void
    {
        $this->apiClient = $this->createMock(ApiClientInterface::class);
        $this->dataToEntityTransformer = $this->createMock(DataToEntityTransformer::class);
        $this->arrayTransformer = $this->createMock(ArrayTransformerInterface::class);
        $this->logger = $this->createMock(LoggerInterface::class);
    }

    abstract protected function makeModel(): Model;
}
