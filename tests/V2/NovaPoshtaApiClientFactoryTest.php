<?php

declare(strict_types=1);

namespace Devleand\Tests\NovaPoshta\Api\V2;

use Devleand\NovaPoshta\Api\V2\Enum\Language;
use Devleand\NovaPoshta\Api\V2\NovaPoshtaApiClientFactory;
use JMS\Serializer\SerializerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class NovaPoshtaApiClientFactoryTest extends TestCase
{
    public function testMakeWithoutLanguage(): void
    {
        $baseUrl = 'http://test';
        $key = '12345abcde';
        $defaultLanguage = Language::RU();
        $factory = new NovaPoshtaApiClientFactory(
            $this->createMock(ClientInterface::class),
            $this->createMock(RequestFactoryInterface::class),
            $this->createMock(StreamFactoryInterface::class),
            $this->createMock(SerializerInterface::class),
            $this->createMock(ValidatorInterface::class),
            $this->createMock(LoggerInterface::class),
            $baseUrl,
            $defaultLanguage
        );

        $client = $factory->make($key);

        $this->assertEquals($baseUrl, $client->getSettings()->getBaseUrl());
        $this->assertEquals($key, $client->getSettings()->getKey());
        $this->assertEquals($defaultLanguage, $client->getSettings()->getLanguage());
    }

    public function testMakeWithLanguage(): void
    {
        $baseUrl = 'http://test';
        $key = '12345abcde';
        $language = Language::UK();
        $factory = new NovaPoshtaApiClientFactory(
            $this->createMock(ClientInterface::class),
            $this->createMock(RequestFactoryInterface::class),
            $this->createMock(StreamFactoryInterface::class),
            $this->createMock(SerializerInterface::class),
            $this->createMock(ValidatorInterface::class),
            $this->createMock(LoggerInterface::class),
            $baseUrl,
            Language::RU()
        );

        $client = $factory->make($key, $language);

        $this->assertEquals($baseUrl, $client->getSettings()->getBaseUrl());
        $this->assertEquals($key, $client->getSettings()->getKey());
        $this->assertEquals($language, $client->getSettings()->getLanguage());
    }
}
