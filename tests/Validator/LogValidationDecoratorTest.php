<?php

declare(strict_types=1);

namespace Devleand\Tests\NovaPoshta\Api\Validator;

use Devleand\NovaPoshta\Api\Validator\LogValidationDecorator;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class LogValidationDecoratorTest extends TestCase
{
    public function testValidate(): void
    {
        $validator = $this->createMock(ValidatorInterface::class);
        $validator
            ->method('validate')
            ->willReturn(new \ArrayIterator());
        $logger = $this->createMock(LoggerInterface::class);
        $decorator = new LogValidationDecorator($validator, $logger);

        $validator
            ->expects($this->once())
            ->method('validate')
            ->with('12345', null, null);
        $logger
            ->expects($this->atLeast(2))
            ->method('info');

        $decorator->validate('12345');
    }

    public function testGetMetadataFor(): void
    {
        $validator = $this->createMock(ValidatorInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $decorator = new LogValidationDecorator($validator, $logger);

        $validator
            ->expects($this->once())
            ->method('getMetadataFor')
            ->with('12345');

        $decorator->getMetadataFor('12345');
    }

    public function testHasMetadataFor(): void
    {
        $validator = $this->createMock(ValidatorInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $decorator = new LogValidationDecorator($validator, $logger);

        $validator
            ->expects($this->once())
            ->method('hasMetadataFor')
            ->with('12345');

        $decorator->hasMetadataFor('12345');
    }

    public function testValidateProperty(): void
    {
        $validator = $this->createMock(ValidatorInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $decorator = new LogValidationDecorator($validator, $logger);
        $object = new \stdClass();

        $validator
            ->expects($this->once())
            ->method('validateProperty')
            ->with($object, 'test', null);

        $decorator->validateProperty($object, 'test');
    }

    public function testValidatePropertyValue(): void
    {
        $validator = $this->createMock(ValidatorInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $decorator = new LogValidationDecorator($validator, $logger);
        $object = new \stdClass();

        $validator
            ->expects($this->once())
            ->method('validatePropertyValue')
            ->with($object, 'test', '12345', null);

        $decorator->validatePropertyValue($object, 'test', '12345');
    }

    public function testStartContext(): void
    {
        $validator = $this->createMock(ValidatorInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $decorator = new LogValidationDecorator($validator, $logger);

        $validator
            ->expects($this->once())
            ->method('startContext');

        $decorator->startContext();
    }

    public function testInContext(): void
    {
        $validator = $this->createMock(ValidatorInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $decorator = new LogValidationDecorator($validator, $logger);
        $context = $this->createMock(ExecutionContextInterface::class);

        $validator
            ->expects($this->once())
            ->method('inContext')
            ->with($context);

        $decorator->inContext($context);
    }
}
