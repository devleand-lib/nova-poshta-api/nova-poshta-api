<?php

declare(strict_types=1);

namespace Devleand\Tests\NovaPoshta\Api\Http;

use Devleand\NovaPoshta\Api\Http\LogRequestClientDecorator;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Log\LoggerInterface;

class LogRequestClientDecoratorTest extends TestCase
{
    public function testSendRequest(): void
    {
        $client = $this->createMock(ClientInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $decorator = new LogRequestClientDecorator($client, $logger);
        $request = $this->createMock(RequestInterface::class);

        $logger
            ->expects($this->atLeastOnce())
            ->method('info');
        $client
            ->expects($this->once())
            ->method('sendRequest')
            ->with($request);

        $decorator->sendRequest($request);
    }
}
