<?php

declare(strict_types=1);

namespace Devleand\Tests\NovaPoshta\Api\Http;

use Devleand\NovaPoshta\Api\Http\LogResponseClientDecorator;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;

class LogResponseClientDecoratorTest extends TestCase
{
    public function testSendRequest(): void
    {
        $response = $this->createMock(ResponseInterface::class);
        $client = $this->createMock(ClientInterface::class);
        $client
            ->method('sendRequest')
            ->willReturn($response);
        $logger = $this->createMock(LoggerInterface::class);
        $decorator = new LogResponseClientDecorator($client, $logger);
        $request = $this->createMock(RequestInterface::class);

        $logger
            ->expects($this->atLeastOnce())
            ->method('info');
        $client
            ->expects($this->once())
            ->method('sendRequest')
            ->with($request);

        $decorator->sendRequest($request);
    }
}
